"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var audio_service_1 = require("../../services/audio/audio.service");
var router_1 = require("@angular/router");
require("rxjs/add/operator/filter");
var AdministrationComponent = /** @class */ (function () {
    function AdministrationComponent(svc, router) {
        this.svc = svc;
        this.router = router;
        this._welcomeFlag = false;
    }
    Object.defineProperty(AdministrationComponent.prototype, "title", {
        get: function () {
            return this._title;
        },
        set: function (value) {
            this._title = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdministrationComponent.prototype, "welcomeFlag", {
        get: function () {
            return this._welcomeFlag;
        },
        enumerable: true,
        configurable: true
    });
    AdministrationComponent.prototype.modifyTitle = function (newTitle) {
        this._title = newTitle;
        this._welcomeFlag = true;
    };
    AdministrationComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof router_1.RoutesRecognized; }).subscribe(function (event) {
            if (event.url == "/customer") {
                _this.title = "118712 - Parcours client";
            }
            else if (event.url == "/programming") {
                _this.title = "118712 - Programmation";
            }
            else if (event.url == "/dialogSimulator") {
                _this.title = "118712 - Simulateur de dialogue";
            }
            else if (event.url == "/smsSimulator") {
                _this.title = "118712 - Simulateur de sms";
            }
            else if (event.url == "/writtenMessageModel") {
                _this.title = "118712 - Liste des modèles de messages écrits";
            }
            else if (event.url == "/writtenMessageText") {
                _this.title = "118712 - Liste des pieds de messages écrits";
            }
            else if (event.url == "/masks") {
                _this.title = "118712 - Liste des numéros noirs";
            }
            else {
                _this.title = 'Interface d\'administration 118712';
            }
        });
    };
    AdministrationComponent = __decorate([
        core_1.Component({
            selector: 'administration',
            templateUrl: 'views/administration/administration.html',
            styleUrls: ['views/administration/administration.css'],
            providers: [audio_service_1.AudioInfosService]
        }),
        __metadata("design:paramtypes", [audio_service_1.AudioInfosService, router_1.Router])
    ], AdministrationComponent);
    return AdministrationComponent;
}());
exports.AdministrationComponent = AdministrationComponent;
//# sourceMappingURL=administration.component.js.map