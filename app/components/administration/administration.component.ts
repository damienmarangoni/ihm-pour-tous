﻿import { Component } from '@angular/core';
import { AudioInfosService } from '../../services/audio/audio.service';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'administration',
    templateUrl: 'views/administration/administration.html',
    styleUrls: ['views/administration/administration.css'],
    providers: [AudioInfosService]
})

export class AdministrationComponent {

    private _title: String;
    private _welcomeFlag: boolean = false;

    public get title() {
        return this._title;
    }

    public set title(value: String) {
        this._title = value;
    }

    public get welcomeFlag() {
        return this._welcomeFlag;
    }

    public modifyTitle(newTitle: String) {
        this._title = newTitle;
        this._welcomeFlag = true;
    }

    ngAfterViewInit() {
        this.router.events.filter(event => event instanceof RoutesRecognized).subscribe((event: RoutesRecognized) => {

            if (event.url == "/customer") {
                this.title = "118712 - Parcours client";
            }
            else if (event.url == "/programming") {
                this.title = "118712 - Programmation";
            }
            else if (event.url == "/dialogSimulator") {
                this.title = "118712 - Simulateur de dialogue";

            }
            else if (event.url == "/smsSimulator") {
                this.title = "118712 - Simulateur de sms";
            }
            else if (event.url == "/writtenMessageModel") {
                this.title = "118712 - Liste des modèles de messages écrits";
            }
            else if (event.url == "/writtenMessageText") {
                this.title = "118712 - Liste des pieds de messages écrits";
            }
            else if(event.url=="/masks") {
                this.title = "118712 - Liste des numéros noirs";
            }
            else {
                this.title = 'Interface d\'administration 118712';
            }
        });
    }

    public constructor(private svc: AudioInfosService, private router: Router) {


    }
}