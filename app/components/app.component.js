"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
/**************************************************************/
/* AppComponent                                               */
/* Description : Gestion des menus et de leur affichage       */
/*                                                            */
/**************************************************************/
var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "<administration></administration>",
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
var ProgrammingComponent = /** @class */ (function () {
    function ProgrammingComponent() {
    }
    ProgrammingComponent = __decorate([
        core_1.Component({ selector: 'programming',
            template: "<h1>Ma Programmation</h1>"
        })
    ], ProgrammingComponent);
    return ProgrammingComponent;
}());
exports.ProgrammingComponent = ProgrammingComponent;
var HomeComponent = /** @class */ (function () {
    /*    constructor(private chs: ChartsService) {
            chs.getMoreUsedRoutes(new Date(Date.now() - 1000*60*60*24)).subscribe(data => {
            this.pie_ChartData  = data.json()
            });
        
            chs.getNbSentSMS(new Date(Date.now() - 1000*60*60*24)).subscribe(data => {
            this.area_ChartData  = data.json()
            });
        }*/
    function HomeComponent() {
        this.pie_ChartOptions = {
            title: 'Parcours client les plus utilisés',
            width: 800,
            height: 400
        };
        this.area_ChartOptions = {
            title: 'Nombre de SMS envoyés',
            width: 630,
            height: 300,
            /*hAxis: { title: 'Jour', titleTextStyle: { color: '#333' } },*/
            vAxis: { minValue: 0 }
        };
    }
    HomeComponent = __decorate([
        core_1.Component({ selector: 'home',
            /*    templateUrl: 'views/charts/googleCharts.html',
                styleUrls: ['views/charts/googleCharts.css'],
                providers: [ChartsService]*/
            template: "<div height=\"10em\" style=\"border: 1px solid #FFA500;margin-top: 3em;border-radius: 0.4em;padding: 0.5em;\">\n                <h6>Bienvenue sur le 118 712</h6>\n                <h6>Veuillez s\u00E9lectionner l'entr\u00E9e de votre choix dans le menu</h6>\n               </div>"
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=app.component.js.map