import { Component } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';

import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Ng2PopupComponent, Ng2MessagePopupComponent } from 'ng2-popup';
import { Ng2Overlay } from 'ng2-overlay';

import { CustomerRouteService } from '../services/customerRoute/customerRoute.service';
/*import { ChartsService } from '../services/charts/charts.service';*/
import { modifyRouteForm, deleteRouteForm, addRouteForm, consultRouteForm } from '../forms/customerRoute/customerRoute.form';
/*import { DeleteWrittMessTextForm } from '../forms/writtenMessage/writtenMessageText.form'*/
import { consultRouteProgForm } from '../forms/customerRoute/customerProg.form';


/**************************************************************/
/* AppComponent                                               */
/* Description : Gestion des menus et de leur affichage       */
/*                                                            */
/**************************************************************/
@Component({
  selector: 'my-app',
  template: `<administration></administration>`,
})
export class AppComponent  {
}

@Component({ selector: 'programming', 
template: `<h1>Ma Programmation</h1>` 
})
export class ProgrammingComponent { }

@Component({ selector: 'home', 
/*    templateUrl: 'views/charts/googleCharts.html',
    styleUrls: ['views/charts/googleCharts.css'],
    providers: [ChartsService]*/
    template: `<div height="10em" style="border: 1px solid #FFA500;margin-top: 3em;border-radius: 0.4em;padding: 0.5em;">
                <h6>Bienvenue sur le 118 712</h6>
                <h6>Veuillez sélectionner l'entrée de votre choix dans le menu</h6>
               </div>`
})
export class HomeComponent { 

    public pie_ChartData: Array<string[]>;
        
    public pie_ChartOptions = {
        title: 'Parcours client les plus utilisés',
        width: 800,
        height: 400
    };

    public area_ChartData: Array<string[]>;

    public area_ChartOptions = {
        title: 'Nombre de SMS envoyés',
        width: 630,
        height: 300,
        /*hAxis: { title: 'Jour', titleTextStyle: { color: '#333' } },*/
        vAxis: { minValue: 0 }
    };

/*    constructor(private chs: ChartsService) {
        chs.getMoreUsedRoutes(new Date(Date.now() - 1000*60*60*24)).subscribe(data => {
        this.pie_ChartData  = data.json()
        });
    
        chs.getNbSentSMS(new Date(Date.now() - 1000*60*60*24)).subscribe(data => {
        this.area_ChartData  = data.json()
        });
    }*/
    constructor() {
        
    }
}