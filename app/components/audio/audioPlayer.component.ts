import { Component, Input, Directive, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AudioInfosService } from '../../services/audio/audio.service';

@Component({
    selector: 'audioPlayer',
    templateUrl: 'views/audioPlayer/audioPlayer.html'
})
export class AudioPlayerComponent {

    @Input() audioFile:string;

    private _src: String;
    private _isClosingInhibited: boolean;

    constructor(private svc: AudioInfosService) {
        
    }
}