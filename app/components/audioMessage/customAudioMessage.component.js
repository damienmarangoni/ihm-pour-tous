"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var audioMessage_service_1 = require("../../services/audioMessage/audioMessage.service");
var audio_service_1 = require("../../services/audio/audio.service");
var ref_service_1 = require("../../services/ref/ref.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var CustomAudioMessageComponent = /** @class */ (function () {
    function CustomAudioMessageComponent(svas, svam, svre) {
        var _this = this;
        this.svas = svas;
        this.svam = svam;
        this.svre = svre;
        /*public idContent:string;*/
        this.messageName = "";
        this.phaseName = "";
        this.trackName = "";
        this.isAdding = false;
        this._audioTrackListFinal = new Array();
        this._phaseListFinal = new Array();
        this.isDeleting = false;
        this.phaseName = "Accueil";
        this.svre.getRefPhase().subscribe(function (data) {
            _this._phaseList = data.json();
            _this._phaseList.forEach(function (el) {
                if (el.codePhase != "ENVOI") {
                    _this._phaseListFinal.push(el);
                }
            });
            _this.svam.getMessageModelList(_this._phaseList[0].codePhase).subscribe(function (data) {
                _this._audioTrackList = data.json();
                _this._audioTrackList.forEach(function (el) {
                    if (el.templateContent != null) {
                        _this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(function (data) {
                            var audioTrackFinal = new MyTemplate();
                            audioTrackFinal.tmp = el;
                            audioTrackFinal.isPresent = data.json();
                            _this._audioTrackListFinal.push(audioTrackFinal);
                        });
                    }
                });
            });
        });
    }
    CustomAudioMessageComponent.prototype.consultMessage = function (audioTrack) {
        var _this = this;
        this.isAdding = false;
        this.isDeleting = false;
        this.phaseName = this.phaseName.split(' ')[0];
        this.idMessage = audioTrack.idTemplate;
        /*this.idContent = audioTrack.templateContent.templateContentId;*/
        this.messageName = audioTrack.templateName;
        this.trackName = audioTrack.templateContent.templateContentTxt;
        this.modal.onDismiss.subscribe(function (_) {
            //alert("Attention: Vos changements seront perdus");        
            //Modification du phaseName sur annulation
            //pour rechargement des données serveurs
            _this.phaseName = _this.phaseName + " ";
        });
        this.modal.open();
    };
    CustomAudioMessageComponent.prototype.listenMessage = function (audioTrack) {
        this.svas.getAudioFile(audioTrack.templateContent.templateContentTxt).subscribe(function (data) { window.open(data.url); });
    };
    CustomAudioMessageComponent.prototype.deleteMessage = function (audioTrack) {
        this.isAdding = false;
        this.isDeleting = true;
        this.idMessage = audioTrack.idTemplate;
        this.messageName = audioTrack.templateName;
        this.modal.open();
    };
    CustomAudioMessageComponent.prototype.setSelected = function (value) {
        var _this = this;
        this.phaseName = value;
        this.svam.getMessageModelList(this._phaseListFinal.find(function (el) { return el.libPhase == value; }).codePhase).subscribe(function (data) {
            _this._audioTrackList = data.json();
            _this._audioTrackListFinal = new Array();
            _this._audioTrackList.forEach(function (el) {
                if (el.templateContent != null) {
                    _this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(function (data) {
                        var audioTrackFinal = new MyTemplate();
                        audioTrackFinal.tmp = el;
                        audioTrackFinal.isPresent = data.json();
                        _this._audioTrackListFinal.push(audioTrackFinal);
                    });
                }
            });
        });
    };
    CustomAudioMessageComponent.prototype.addCustomAudioMessage = function () {
        this.isAdding = true;
        this.isDeleting = false;
        this.modal.open();
    };
    CustomAudioMessageComponent.prototype.messageCreateOrSave = function (e) {
        var _this = this;
        this.isAdding = false;
        this.modal.close();
        this.svam.getMessageModelList(this._phaseListFinal.find(function (el) { return el.libPhase == _this.phaseName.split(' ')[0]; }).codePhase).subscribe(function (data) {
            _this._audioTrackList = data.json();
            _this._audioTrackListFinal = new Array();
            _this._audioTrackList.forEach(function (el) {
                if (el.templateContent != null) {
                    _this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(function (data) {
                        var audioTrackFinal = new MyTemplate();
                        audioTrackFinal.tmp = el;
                        audioTrackFinal.isPresent = data.json();
                        _this._audioTrackListFinal.push(audioTrackFinal);
                    });
                }
            });
        });
    };
    //Appelée suite à l'event onDelete du template delete form
    CustomAudioMessageComponent.prototype.onDelete = function (i) {
        var _this = this;
        this.svam.deleteAudioMessageModel(i.toString()).subscribe(function (response) {
            _this.modal.close();
            _this.svam.getMessageModelList(_this._phaseListFinal.find(function (el) { return el.libPhase == _this.phaseName; }).codePhase).subscribe(function (data) {
                _this._audioTrackList = new Array();
                _this._audioTrackListFinal = new Array();
                _this._audioTrackList = data.json();
                _this._audioTrackList.forEach(function (el) {
                    if (el.templateContent != null) {
                        _this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(function (data) {
                            var audioTrackFinal = new MyTemplate();
                            audioTrackFinal.tmp = el;
                            audioTrackFinal.isPresent = data.json();
                            _this._audioTrackListFinal.push(audioTrackFinal);
                        });
                    }
                });
            });
        }, function (error) {
            alert("ERROR: La suppression du parcours client a echouée");
        }, function () { });
    };
    //Appelée suite à l'event onClose du template delete form
    CustomAudioMessageComponent.prototype.onClose = function (i) {
        this.modal.close();
    };
    __decorate([
        core_1.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CustomAudioMessageComponent.prototype, "modal", void 0);
    CustomAudioMessageComponent = __decorate([
        core_1.Component({
            selector: 'customAudioMessage',
            templateUrl: 'views/audioMessage/customAudioMessage.html',
            styleUrls: ['views/audioMessage/customAudioMessage.css',
                'views/customerRoute/customerRoute.css'],
            providers: [audioMessage_service_1.AudioMessageService, audio_service_1.AudioInfosService, ref_service_1.RefService]
        }),
        __metadata("design:paramtypes", [audio_service_1.AudioInfosService, audioMessage_service_1.AudioMessageService, ref_service_1.RefService])
    ], CustomAudioMessageComponent);
    return CustomAudioMessageComponent;
}());
exports.CustomAudioMessageComponent = CustomAudioMessageComponent;
var RefAudioTrack = /** @class */ (function () {
    function RefAudioTrack() {
    }
    return RefAudioTrack;
}());
var RefPhase = /** @class */ (function () {
    function RefPhase() {
    }
    return RefPhase;
}());
var MyTemplate = /** @class */ (function () {
    function MyTemplate() {
    }
    return MyTemplate;
}());
//# sourceMappingURL=customAudioMessage.component.js.map