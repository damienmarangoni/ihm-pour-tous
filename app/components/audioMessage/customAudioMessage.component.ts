import {Component, ViewChild} from '@angular/core';

import {AudioMessageService} from '../../services/audioMessage/audioMessage.service';
import {AudioInfosService} from '../../services/audio/audio.service';
import {RefService} from '../../services/ref/ref.service';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';
import {Template} from '../../mappers/writtenMessage.mapper';

@Component({
    selector: 'customAudioMessage',
    templateUrl: 'views/audioMessage/customAudioMessage.html',
    styleUrls: ['views/audioMessage/customAudioMessage.css',
      'views/customerRoute/customerRoute.css'],
    providers: [AudioMessageService, AudioInfosService, RefService]
})

export class CustomAudioMessageComponent {

    @ViewChild(ModalComponent) modal: ModalComponent;

    public idMessage:string;
    /*public idContent:string;*/
    public messageName:string = "";
    public phaseName:string = "";
    public trackName:string = "";
    public isAdding:boolean = false;

    /*private _audioTrackList: Array<RefAudioTrack>;*/
    private _audioTrackList: Array<Template>; 
    private _audioTrackListFinal: Array<MyTemplate> = new Array<MyTemplate>(); 
    private _phaseList: Array<RefPhase>;
    private _phaseListFinal: Array<RefPhase> = new Array<RefPhase>();

    public isDeleting:boolean = false;

    public consultMessage(audioTrack:Template) {

        this.isAdding = false;
        this.isDeleting = false;

        this.phaseName = this.phaseName.split(' ')[0];

        this.idMessage = audioTrack.idTemplate;
        /*this.idContent = audioTrack.templateContent.templateContentId;*/
        this.messageName = audioTrack.templateName;
        this.trackName = audioTrack.templateContent.templateContentTxt;

        this.modal.onDismiss.subscribe(_ => {
        
            //alert("Attention: Vos changements seront perdus");        
            //Modification du phaseName sur annulation
            //pour rechargement des données serveurs
            this.phaseName = this.phaseName + " "; 
       
        });

        this.modal.open();
    }

    public listenMessage(audioTrack:Template) {

        this.svas.getAudioFile(audioTrack.templateContent.templateContentTxt).subscribe(
            data => { window.open(data.url);}
        ); 
    }  

    public deleteMessage(audioTrack:Template) {

        this.isAdding = false;
        this.isDeleting = true;

        this.idMessage = audioTrack.idTemplate;
        this.messageName = audioTrack.templateName;

        this.modal.open();
    }

    public setSelected(value:string) {      

        this.phaseName = value;

        this.svam.getMessageModelList(this._phaseListFinal.find(el => el.libPhase==value).codePhase).subscribe(data => {
                this._audioTrackList = data.json();

                this._audioTrackListFinal = new Array<MyTemplate>();      

                this._audioTrackList.forEach(el=>{
                    if(el.templateContent!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(data=>{
                            let audioTrackFinal: MyTemplate = new MyTemplate();
                            audioTrackFinal.tmp = el;
                            audioTrackFinal.isPresent = data.json();

                            this._audioTrackListFinal.push(audioTrackFinal); 
                        })
                    }
                });
            });
    }

    public addCustomAudioMessage() {
        
        this.isAdding = true;        
        this.isDeleting = false;
        this.modal.open();
    }


    private messageCreateOrSave(e:any) {

        this.isAdding = false;
        this.modal.close();

        this.svam.getMessageModelList(this._phaseListFinal.find(el => el.libPhase==this.phaseName.split(' ')[0]).codePhase).subscribe(data => {
                this._audioTrackList = data.json();               

                this._audioTrackListFinal = new Array<MyTemplate>();      

                this._audioTrackList.forEach(el=>{
                    if(el.templateContent!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(data=>{
                            let audioTrackFinal: MyTemplate = new MyTemplate();
                            audioTrackFinal.tmp = el;
                            audioTrackFinal.isPresent = data.json();

                            this._audioTrackListFinal.push(audioTrackFinal); 
                        })
                    }
                });
            });
    }

    //Appelée suite à l'event onDelete du template delete form
    public onDelete(i: number) {
        
       this.svam.deleteAudioMessageModel(i.toString()).subscribe(
                response => {
                    this.modal.close();   
                    this.svam.getMessageModelList(this._phaseListFinal.find(el => el.libPhase==this.phaseName).codePhase).subscribe(data => {
                    this._audioTrackList = new Array<Template>();
                    this._audioTrackListFinal = new Array<MyTemplate>();
                    this._audioTrackList = data.json();

                    this._audioTrackList.forEach(el=>{
                        if(el.templateContent!=null) {
                            
                            this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(data=>{
                                
                                let audioTrackFinal: MyTemplate = new MyTemplate();
                                audioTrackFinal.tmp = el;
                                audioTrackFinal.isPresent = data.json();

                                this._audioTrackListFinal.push(audioTrackFinal); 
                            })
                        }
                    });
                });                         
                },
                error => {
                    alert("ERROR: La suppression du parcours client a echouée");
                },
                () =>{}   
            );
    }

    //Appelée suite à l'event onClose du template delete form
    public onClose(i: number) {
        
        this.modal.close();  
    }



    public constructor(private svas: AudioInfosService, private svam: AudioMessageService, private svre: RefService) {        

        this.phaseName = "Accueil";

        this.svre.getRefPhase().subscribe(data => {
            this._phaseList = data.json();           

            this._phaseList.forEach(el => {
                if(el.codePhase!="ENVOI"){
                    this._phaseListFinal.push(el);
                }
            });  

            this.svam.getMessageModelList(this._phaseList[0].codePhase).subscribe(data => {
                this._audioTrackList = data.json();

                this._audioTrackList.forEach(el=>{
                    if(el.templateContent!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.templateContent.templateContentTxt).subscribe(data=>{
                            let audioTrackFinal: MyTemplate = new MyTemplate();
                            audioTrackFinal.tmp = el;
                            audioTrackFinal.isPresent = data.json();

                            this._audioTrackListFinal.push(audioTrackFinal); 
                        })
                    }
                });
            });
        });
    }
}

class RefAudioTrack {
    filename:string;
    libelle:string;
}

class RefPhase {
    codePhase: string;
    libPhase: string;
    maxTrack: number;
}

class MyTemplate {
    tmp:Template;
    isPresent:boolean;
}