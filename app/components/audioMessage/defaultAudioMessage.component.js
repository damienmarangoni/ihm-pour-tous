"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var audioMessage_service_1 = require("../../services/audioMessage/audioMessage.service");
var audio_service_1 = require("../../services/audio/audio.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var DefaultAudioMessageComponent = /** @class */ (function () {
    function DefaultAudioMessageComponent(svas, svam) {
        var _this = this;
        this.svas = svas;
        this.svam = svam;
        this.messageName = "";
        this.phaseName = "";
        this.isDeleting = false;
        this.svam.getRefDefAudioTrackList().subscribe(function (data) {
            _this._audioTrackList = data.json();
            _this._audioTrackList.forEach(function (el) {
                if (el.filename != null) {
                    _this.svam.getIsAudioFilePresent(el.filename).subscribe(function (data) {
                        el.isPresent = data.json();
                    });
                }
            });
        });
    }
    DefaultAudioMessageComponent.prototype.consultMessage = function (audioTrack) {
        var _this = this;
        this.isDeleting = false;
        this.idMessage = audioTrack.filename;
        this.phaseName = this.phaseName.split(' ')[0];
        this.messageName = audioTrack.libelle;
        this.modal.onDismiss.subscribe(function (_) {
            //alert("Attention: Vos changements seront perdus");        
            //Modification du phaseName sur annulation
            //pour rechargement des données serveurs
            _this.phaseName = _this.phaseName + " ";
        });
        this.modal.open();
    };
    DefaultAudioMessageComponent.prototype.listenMessage = function (file) {
        this.svas.getDefAudioFile(file).subscribe(function (data) { window.open(data.url); });
    };
    DefaultAudioMessageComponent.prototype.messageCreateOrSave = function (e) {
        var _this = this;
        this._audioTrackList = Array();
        this.svam.getRefDefAudioTrackList().subscribe(function (data) {
            _this._audioTrackList = data.json();
            _this._audioTrackList.forEach(function (el) {
                if (el.filename != null) {
                    _this.svam.getIsAudioFilePresent(el.filename).subscribe(function (data) {
                        el.isPresent = data.json();
                    });
                }
            });
        });
        this.modal.close();
    };
    __decorate([
        core_1.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], DefaultAudioMessageComponent.prototype, "modal", void 0);
    DefaultAudioMessageComponent = __decorate([
        core_1.Component({
            selector: 'defaultAudioMessage',
            templateUrl: 'views/audioMessage/defaultAudioMessage.html',
            styleUrls: ['views/audioMessage/defaultAudioMessage.css'],
            providers: [audioMessage_service_1.AudioMessageService]
        }),
        __metadata("design:paramtypes", [audio_service_1.AudioInfosService, audioMessage_service_1.AudioMessageService])
    ], DefaultAudioMessageComponent);
    return DefaultAudioMessageComponent;
}());
exports.DefaultAudioMessageComponent = DefaultAudioMessageComponent;
var RefDefAudioTrack = /** @class */ (function () {
    function RefDefAudioTrack() {
    }
    return RefDefAudioTrack;
}());
exports.RefDefAudioTrack = RefDefAudioTrack;
//# sourceMappingURL=defaultAudioMessage.component.js.map