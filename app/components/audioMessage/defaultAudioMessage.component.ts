import {Component, ViewChild} from '@angular/core';

import {AudioMessageService} from '../../services/audioMessage/audioMessage.service';
import {AudioInfosService} from '../../services/audio/audio.service';
import {Template} from '../../mappers/writtenMessage.mapper';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'defaultAudioMessage',
    templateUrl: 'views/audioMessage/defaultAudioMessage.html',
    styleUrls: ['views/audioMessage/defaultAudioMessage.css'],
    providers: [AudioMessageService]
})

export class DefaultAudioMessageComponent {

    @ViewChild(ModalComponent) modal: ModalComponent;


    public idMessage:string;
    public messageName:string = "";
    public phaseName:string = "";

    public isDeleting:boolean = false;

    private _audioTrackList: Array<RefDefAudioTrack>;

    public consultMessage(audioTrack:RefDefAudioTrack) {

        this.isDeleting = false;

        this.idMessage = audioTrack.filename;
        this.phaseName = this.phaseName.split(' ')[0];
        this.messageName = audioTrack.libelle;

        this.modal.onDismiss.subscribe(_ => {
        
            //alert("Attention: Vos changements seront perdus");        
            //Modification du phaseName sur annulation
            //pour rechargement des données serveurs
            this.phaseName = this.phaseName + " "; 
       
        });

        this.modal.open();
    }

    public listenMessage(file:string) {

        this.svas.getDefAudioFile(file).subscribe(
            data => { window.open(data.url);}
        ); 
    }

    private messageCreateOrSave(e:any) {


        this._audioTrackList = Array<RefDefAudioTrack>();
        this.svam.getRefDefAudioTrackList().subscribe(data => {
            this._audioTrackList = data.json();
            this._audioTrackList.forEach(el=>{
                    if(el.filename!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.filename).subscribe(data=>{
                            el.isPresent = data.json();
                        })
                    }
                });
        });

        this.modal.close();
    }


    public constructor(private svas: AudioInfosService, private svam: AudioMessageService) {

        this.svam.getRefDefAudioTrackList().subscribe(data => {
            this._audioTrackList = data.json();
            this._audioTrackList.forEach(el=>{
                    if(el.filename!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.filename).subscribe(data=>{
                            el.isPresent = data.json();
                        })
                    }
                });
        });
    }
}

export class RefDefAudioTrack {
    filename:string;
    libelle:string;
    serviceName:string;
    phaseLib:string;
    isPresent:boolean;
}