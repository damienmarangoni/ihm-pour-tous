"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var common_1 = require("@angular/common");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var customerRoute_service_1 = require("../../services/customerRoute/customerRoute.service");
var customerRoute_mapper_1 = require("../../mappers/customerRoute.mapper");
/**************************************************************/
/* CustomerRouteComponent                                     */
/* Description : Gestion des Parcours client et de leurs      */
/*               Programmation                                */
/**************************************************************/
var CustomerRouteComponent = /** @class */ (function () {
    function CustomerRouteComponent(svca) {
        var _this = this;
        this.svca = svca;
        this.onTitle = new core_1.EventEmitter();
        this.isAdding = false;
        this.isConsulting = false;
        this.isConsultingProg = false;
        this.isModifying = false;
        this.isDeleting = false;
        this.isNewProg = false;
        this._mapper = new customerRoute_mapper_1.CustomerRouteMapper();
        //Initialisation des attributs des datepickers
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '10em'
        };
        this.myStartDate = { date: { year: 2018, month: 10, day: 9 } };
        this.myEndDate = { date: { year: 2018, month: 10, day: 9 } };
        this._isProgAssociated = true;
        //TODO: Récupérer les dates de départ au plus tôt et de fin au plus tard dans la base
        /*this.startDateVal = new Date(Date.now() - 1000*60*60*24);
        this.endDateVal = new Date(Date.now());*/
        this.startDateVal = new Date("2000-01-01");
        this.myStartDate = { date: { year: this.startDateVal.getFullYear(),
                month: this.startDateVal.getMonth() + 1,
                day: this.startDateVal.getDate() } };
        this.endDateVal = new Date("2050-12-31");
        this.myEndDate = { date: { year: this.endDateVal.getFullYear(),
                month: this.endDateVal.getMonth() + 1,
                day: this.endDateVal.getDate() } };
        /*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
                this._mapper.customerRouteListDetails  = data.json();
                if(this._mapper.customerRouteListDetails!=undefined) {
                  this._mapper.customerRouteListToDisplayMapper();
                }
              });   */
        this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(function (data) {
            _this._mapper.customerRouteListDetails = data.json();
            if (_this._mapper.customerRouteListDetails != undefined) {
                _this._mapper.customerRouteListToDisplayMapper();
            }
        });
    }
    Object.defineProperty(CustomerRouteComponent.prototype, "customerRouteList", {
        get: function () {
            return this._mapper.customerRouteListToDisplay;
        },
        enumerable: true,
        configurable: true
    });
    CustomerRouteComponent.prototype.modifyRoute = function (route) {
        this.isAdding = false;
        this.isConsulting = false;
        this.isConsultingProg = false;
        this.isModifying = true;
        this.isDeleting = false;
        this.routeName = route[0];
        // Appel du service getCustomerRouteDetails()
        this.modal.open();
    };
    CustomerRouteComponent.prototype.deleteRoute = function (route) {
        this.isAdding = false;
        this.isConsulting = false;
        this.isConsultingProg = false;
        this.isModifying = false;
        this.isDeleting = true;
        this.routeName = route[0];
        this.idRoute = route[1];
        if (route[2] == "Par défaut" &&
            route[3] == "Par défaut" &&
            route[4] == "Par défaut" &&
            route[5] == "Par défaut" &&
            route[6] == "Par défaut") {
            this._isProgAssociated = false;
        }
        else {
            this._isProgAssociated = true;
        }
        this.modal.open();
    };
    CustomerRouteComponent.prototype.addRoute = function () {
        var _this = this;
        this.isAdding = true;
        this.isConsulting = false;
        this.isConsultingProg = false;
        this.isModifying = false;
        this.isDeleting = false;
        this.routeName = null;
        this.modal.open();
        this.modal.onDismiss.subscribe(function (_) {
            //if(this.isModifying) {
            //alert("Attention: Vos changements seront perdus");        
            //Modification du routeName sur annulation
            //pour rechargement des données serveurs
            _this.routeName = _this.routeName + " ";
            //}
        });
    };
    CustomerRouteComponent.prototype.consultRoute = function (route) {
        var _this = this;
        this.isAdding = false;
        this.isConsulting = true;
        this.isConsultingProg = false;
        this.isModifying = false;
        this.isDeleting = false;
        this.routeName = route[0];
        this.idRoute = route[1];
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du routeName sur annulation
                //pour rechargement des données serveurs
                _this.routeName = _this.routeName + " ";
            }
        });
        this.modal.open();
    };
    CustomerRouteComponent.prototype.consultRouteProg = function (route, step) {
        var _this = this;
        this.isAdding = false;
        this.isConsulting = false;
        this.isConsultingProg = true;
        this.isModifying = false;
        this.isDeleting = false;
        var phaseTitle = "";
        // Identification de la phase
        if (step == 1) {
            this.phaseName = "Accueil";
            phaseTitle = route[2];
        }
        else if (step == 2) {
            this.phaseName = "Tarif";
            phaseTitle = route[3];
        }
        else if (step == 3) {
            this.phaseName = "Attente";
            phaseTitle = route[4];
        }
        else if (step == 4) {
            this.phaseName = "Promo";
            phaseTitle = route[5];
        }
        else if (step == 5) {
            this.phaseName = "Sms";
            phaseTitle = route[6];
        }
        this.routeName = route[0];
        this.idRoute = route[1];
        this.step = step;
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du phaseName sur annulation
                //pour rechargement des données serveurs
                _this.phaseName = _this.phaseName + " ";
            }
        });
        if (phaseTitle != "Par défaut") {
            this.isNewProg = false;
        }
        else {
            //Si la phase est Par défaut, il n'y a aucune prog correspondante
            //il faut en ajouter une   
            this.isNewProg = true;
        }
        this.modal.open();
    };
    CustomerRouteComponent.prototype.modifyDate = function (i, e) {
        var _this = this;
        var datePipe = new common_1.DatePipe("en-EN");
        if (i == 1) {
            this._previousStartDate = this.startDateVal;
            this.startDateVal.setFullYear(e.date.year);
            this.startDateVal.setMonth(e.date.month);
            this.startDateVal.setDate(e.date.day);
        }
        else if (i == 2) {
            this.endDateVal.setFullYear(e.date.year);
            this.endDateVal.setMonth(e.date.month);
            this.endDateVal.setDate(e.date.day);
        }
        if (this.startDateVal.getTime() > this.endDateVal.getTime()) {
            alert('La date de début de validité est supérieure à la date de fin de validité');
            this.startDateVal = new Date(this._previousStartDate.getTime());
        }
        //Filtrage de la liste des parcours puis affichage
        /*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
                this._mapper.customerRouteListDetails  = data.json();
                if(this._mapper.customerRouteListDetails!=undefined) {
                  this._mapper.customerRouteListToDisplayMapper();
                }
              });   */
        this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(function (data) {
            _this._mapper.customerRouteListDetails = data.json();
            if (_this._mapper.customerRouteListDetails != undefined) {
                _this._mapper.customerRouteListToDisplayMapper();
            }
        });
    };
    //Appelée suite à l'event onCancel du customer route form
    CustomerRouteComponent.prototype.routeModifyCancel = function (i) {
        //Modification du routeName sur annulation
        //pour rechargement des données serveurs
        this.routeName = this.routeName + " ";
        this.isModifying = false;
    };
    //Appelée suite à l'event onModify du customer route form
    CustomerRouteComponent.prototype.routeModify = function (i) {
        this.isModifying = true;
    };
    //Appelée suite à l'event onSave du customer route form
    CustomerRouteComponent.prototype.routeSave = function (i) {
        this.isModifying = false;
    };
    //Appelée suite à l'event onClose du customer route delete form
    CustomerRouteComponent.prototype.modalClose = function (i) {
        var _this = this;
        this.modal.close();
        this.startDateVal = new Date("2000-01-01");
        this.myStartDate = { date: { year: this.startDateVal.getFullYear(),
                month: this.startDateVal.getMonth() + 1,
                day: this.startDateVal.getDate() } };
        this.endDateVal = new Date("2050-12-31");
        this.myEndDate = { date: { year: this.endDateVal.getFullYear(),
                month: this.endDateVal.getMonth() + 1,
                day: this.endDateVal.getDate() } };
        /*    this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
              this._mapper.customerRouteListDetails  = data.json();
              if(this._mapper.customerRouteListDetails!=undefined) {
                this._mapper.customerRouteListToDisplayMapper();
              }
            });  */
        this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(function (data) {
            _this._mapper.customerRouteListDetails = data.json();
            if (_this._mapper.customerRouteListDetails != undefined) {
                _this._mapper.customerRouteListToDisplayMapper();
            }
        });
    };
    //Appelée suite à l'event onDelete du customer route delete form
    CustomerRouteComponent.prototype.onDelete = function (i) {
        var _this = this;
        this._mapper.customerRouteDetailsToSend = new customerRoute_mapper_1.CustomerRouteDetails();
        this._mapper.customerRouteDetailsToSend.customerQualificationId = i;
        this._mapper.customerRouteDetailsToSend.customerQualificationName = this.routeName;
        if (!this._isProgAssociated) {
            this.svca.deleteCustomerRouteDetails(i).subscribe(function (response) {
                _this.modal.close();
                _this.startDateVal = new Date("2000-01-01");
                _this.myStartDate = { date: { year: _this.startDateVal.getFullYear(),
                        month: _this.startDateVal.getMonth() + 1,
                        day: _this.startDateVal.getDate() } };
                _this.endDateVal = new Date("2050-12-31");
                _this.myEndDate = { date: { year: _this.endDateVal.getFullYear(),
                        month: _this.endDateVal.getMonth() + 1,
                        day: _this.endDateVal.getDate() } };
                /*this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
                  this._mapper.customerRouteListDetails  = data.json();
                  if(this._mapper.customerRouteListDetails!=undefined) {
                    this._mapper.customerRouteListToDisplayMapper();
                  }
                });   */
                _this.svca.getCustomerRouteListOnDates(_this.startDateVal, _this.endDateVal).subscribe(function (data) {
                    _this._mapper.customerRouteListDetails = data.json();
                    if (_this._mapper.customerRouteListDetails != undefined) {
                        _this._mapper.customerRouteListToDisplayMapper();
                    }
                });
            }, function (error) {
                alert("ERROR: La suppression du parcours client a echouée");
            }, function () { });
        }
        else {
            alert("WARNING: Suppression impossible car des programmations sont associées à ce parcours");
            this.modal.close();
        }
    };
    //Appelée suite à l'event onCreate du customer route form
    CustomerRouteComponent.prototype.routeCreate = function (i) {
        var _this = this;
        this.isModifying = false;
        this.modal.close();
        this.startDateVal = new Date("2000-01-01");
        this.myStartDate = { date: { year: this.startDateVal.getFullYear(),
                month: this.startDateVal.getMonth() + 1,
                day: this.startDateVal.getDate() } };
        this.endDateVal = new Date("2050-12-31");
        this.myEndDate = { date: { year: this.endDateVal.getFullYear(),
                month: this.endDateVal.getMonth() + 1,
                day: this.endDateVal.getDate() } };
        /*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
                this._mapper.customerRouteListDetails  = data.json();
                if(this._mapper.customerRouteListDetails!=undefined) {
                  this._mapper.customerRouteListToDisplayMapper();
                }
              });   */
        this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(function (data) {
            _this._mapper.customerRouteListDetails = data.json();
            if (_this._mapper.customerRouteListDetails != undefined) {
                _this._mapper.customerRouteListToDisplayMapper();
            }
        });
    };
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CustomerRouteComponent.prototype, "modal", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], CustomerRouteComponent.prototype, "onTitle", void 0);
    CustomerRouteComponent = __decorate([
        core_1.Component({ selector: 'customer',
            templateUrl: 'views/customerRoute/customerRoute.html',
            styleUrls: ['views/customerRoute/customerRoute.css'],
            providers: [customerRoute_service_1.CustomerRouteService]
        }),
        __metadata("design:paramtypes", [customerRoute_service_1.CustomerRouteService])
    ], CustomerRouteComponent);
    return CustomerRouteComponent;
}());
exports.CustomerRouteComponent = CustomerRouteComponent;
//# sourceMappingURL=customerRoute.component.js.map