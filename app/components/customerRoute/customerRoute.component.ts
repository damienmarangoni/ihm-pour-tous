import { Component,Output, EventEmitter } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {IMyDpOptions} from 'mydatepicker';

import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';
import { modifyRouteForm, deleteRouteForm, addRouteForm, consultRouteForm } from '../../forms/customerRoute/customerRoute.form';
import {CustomerRouteMapper, CustomerRouteDetails} from '../../mappers/customerRoute.mapper';


/**************************************************************/
/* CustomerRouteComponent                                     */
/* Description : Gestion des Parcours client et de leurs      */
/*               Programmation                                */
/**************************************************************/
@Component({ selector: 'customer',
    templateUrl: 'views/customerRoute/customerRoute.html', 
    styleUrls: ['views/customerRoute/customerRoute.css'],
    providers: [CustomerRouteService]
})
export class CustomerRouteComponent { 

  @ViewChild(ModalComponent) modal: ModalComponent;  
  @Output() onTitle: EventEmitter<String> = new EventEmitter<String>();
 
  private isAdding: boolean = false;
  private isConsulting: boolean = false;
  private isConsultingProg: boolean = false;
  private isModifying: boolean = false;
  private isDeleting: boolean = false;
  private isNewProg: boolean = false;

  private _mapper: CustomerRouteMapper = new CustomerRouteMapper();

  public startDateVal : Date;
  private _previousStartDate : Date;
  public endDateVal : Date;
  
  private idRoute: string;
  private routeName:string;
  private phaseName:string;
  private step:number;

  //Initialisation des attributs des datepickers
  private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        inline: false,
        width: '10em'
    };
  private myStartDate: Object = { date: { year: 2018, month: 10, day: 9 } };
  private myEndDate: Object = { date: { year: 2018, month: 10, day: 9 } };


  public get customerRouteList(): Array<string[]> {
    return this._mapper.customerRouteListToDisplay;
  }

  public modifyRoute(route:string[]) {
    this.isAdding = false;       
    this.isConsulting = false;      
    this.isConsultingProg = false;     
    this.isModifying = true;        
    this.isDeleting = false; 

    this.routeName = route[0]; 

    // Appel du service getCustomerRouteDetails()
    
    this.modal.open();
  }

  public deleteRoute(route:string[]) {
    this.isAdding = false;       
    this.isConsulting = false;    
    this.isConsultingProg = false;      
    this.isModifying = false;        
    this.isDeleting = true; 

    this.routeName = route[0];   
    this.idRoute = route[1]; 

    if(route[2]=="Par défaut"&&
       route[3]=="Par défaut"&&
       route[4]=="Par défaut"&&
       route[5]=="Par défaut"&&
       route[6]=="Par défaut") {
         this._isProgAssociated = false;
    }
    else {
      this._isProgAssociated = true;
    }

    this.modal.open();
  }

  public addRoute() {    
    this.isAdding = true;       
    this.isConsulting = false;   
    this.isConsultingProg = false;       
    this.isModifying = false;        
    this.isDeleting = false;     
    
    this.routeName = null; 

    this.modal.open();
    
    this.modal.onDismiss.subscribe(_ => {
      //if(this.isModifying) {
        //alert("Attention: Vos changements seront perdus");        
        //Modification du routeName sur annulation
        //pour rechargement des données serveurs
        this.routeName = this.routeName + " "; 
      //}
    });
  }

  public consultRoute(route:string[]) {    
    this.isAdding = false;       
    this.isConsulting = true;  
    this.isConsultingProg = false;        
    this.isModifying = false;        
    this.isDeleting = false; 
    
    this.routeName = route[0];     
    this.idRoute = route[1]; 
    
    this.modal.onDismiss.subscribe(_ => {
      if(this.isModifying) {
        //alert("Attention: Vos changements seront perdus");        
        //Modification du routeName sur annulation
        //pour rechargement des données serveurs
        this.routeName = this.routeName + " "; 
      }
  });
    this.modal.open();
  }

  public consultRouteProg(route:string[], step:number) {    
    this.isAdding = false;  
    this.isConsulting = false;   
    this.isConsultingProg = true;       
    this.isModifying = false;        
    this.isDeleting = false; 

    let phaseTitle:string = ""

    // Identification de la phase
    if (step==1) {
      this.phaseName = "Accueil";
      phaseTitle = route[2]; 
    }
    else if (step==2) {
      this.phaseName = "Tarif";
      phaseTitle = route[3]; 
    }
    else if (step==3) {
      this.phaseName = "Attente";
      phaseTitle = route[4]; 
    }
    else if (step==4) {
      this.phaseName = "Promo";
      phaseTitle = route[5]; 
    }  
    else if (step==5) {
      this.phaseName = "Sms";
      phaseTitle = route[6]; 
    }   
    
    this.routeName = route[0]; 
    this.idRoute = route[1]; 
    this.step = step;
    this.modal.onDismiss.subscribe(_ => {
      if(this.isModifying) {
        //alert("Attention: Vos changements seront perdus");        
        //Modification du phaseName sur annulation
        //pour rechargement des données serveurs
        this.phaseName = this.phaseName + " ";
      }
    });

    if(phaseTitle!="Par défaut") {
      this.isNewProg = false;
    }
    else {   
      //Si la phase est Par défaut, il n'y a aucune prog correspondante
      //il faut en ajouter une   
      this.isNewProg = true;
    }
    this.modal.open();
  }
  
  public modifyDate(i:number ,e: any) {    
    let datePipe: DatePipe = new DatePipe("en-EN");
    if (i==1) {  
      this._previousStartDate = this.startDateVal;
      this.startDateVal.setFullYear(e.date.year);
      this.startDateVal.setMonth(e.date.month);
      this.startDateVal.setDate(e.date.day);
    } else if (i==2) {    
      this.endDateVal.setFullYear(e.date.year);
      this.endDateVal.setMonth(e.date.month);
      this.endDateVal.setDate(e.date.day);
    }

    if (this.startDateVal.getTime() > this.endDateVal.getTime()) {
      alert('La date de début de validité est supérieure à la date de fin de validité'); 
      this.startDateVal = new Date(this._previousStartDate.getTime());
    }

    //Filtrage de la liste des parcours puis affichage
/*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
        this._mapper.customerRouteListDetails  = data.json();  
        if(this._mapper.customerRouteListDetails!=undefined) {    
          this._mapper.customerRouteListToDisplayMapper();  
        }                 
      });   */

      this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(data => {
          this._mapper.customerRouteListDetails  = data.json();  
          if(this._mapper.customerRouteListDetails!=undefined) {    
            this._mapper.customerRouteListToDisplayMapper();  
          }    
      }); 
  }

  //Appelée suite à l'event onCancel du customer route form
  public routeModifyCancel(i: number) {
    //Modification du routeName sur annulation
    //pour rechargement des données serveurs
    this.routeName = this.routeName + " ";     
    this.isModifying = false;
  }

  //Appelée suite à l'event onModify du customer route form
  public routeModify(i: number) {
    this.isModifying = true;
  }

  //Appelée suite à l'event onSave du customer route form
  public routeSave(i: number) {
    this.isModifying = false;
  }

  //Appelée suite à l'event onClose du customer route delete form
  public modalClose(i: number) {
    this.modal.close(); 

    this.startDateVal = new Date("2000-01-01");
    this.myStartDate = { date: { year: this.startDateVal.getFullYear(), 
      month: this.startDateVal.getMonth()+1,
        day: this.startDateVal.getDate() } };

    this.endDateVal = new Date("2050-12-31");
    this.myEndDate = { date: { year: this.endDateVal.getFullYear(), 
      month: this.endDateVal.getMonth()+1,
        day: this.endDateVal.getDate() } };

/*    this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
      this._mapper.customerRouteListDetails  = data.json();  
      if(this._mapper.customerRouteListDetails!=undefined) {    
        this._mapper.customerRouteListToDisplayMapper();  
      }    
    });  */  

    this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(data => {
        this._mapper.customerRouteListDetails  = data.json();  
        if(this._mapper.customerRouteListDetails!=undefined) {    
          this._mapper.customerRouteListToDisplayMapper();  
        }    
    });   
  }

  private _isProgAssociated: boolean = true;
  //Appelée suite à l'event onDelete du customer route delete form
  public onDelete(i: number) {
    
    this._mapper.customerRouteDetailsToSend = new CustomerRouteDetails();
    this._mapper.customerRouteDetailsToSend.customerQualificationId = i;
    this._mapper.customerRouteDetailsToSend.customerQualificationName = this.routeName;

    if(!this._isProgAssociated) {

      this.svca.deleteCustomerRouteDetails(i).subscribe(
              response => {
                  this.modal.close();                     

                  this.startDateVal = new Date("2000-01-01");
                  this.myStartDate = { date: { year: this.startDateVal.getFullYear(), 
                    month: this.startDateVal.getMonth()+1,
                      day: this.startDateVal.getDate() } };

                  this.endDateVal = new Date("2050-12-31");
                  this.myEndDate = { date: { year: this.endDateVal.getFullYear(), 
                    month: this.endDateVal.getMonth()+1,
                      day: this.endDateVal.getDate() } };

                  /*this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
                    this._mapper.customerRouteListDetails  = data.json();  
                    if(this._mapper.customerRouteListDetails!=undefined) {    
                      this._mapper.customerRouteListToDisplayMapper();  
                    }                 
                  });   */   

                  this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(data => {
                      this._mapper.customerRouteListDetails  = data.json();  
                      if(this._mapper.customerRouteListDetails!=undefined) {    
                        this._mapper.customerRouteListToDisplayMapper();  
                      }    
                  });       
              },
              error => {
                  alert("ERROR: La suppression du parcours client a echouée");
              },
              () =>{}   
      );
    }
    else {
      alert("WARNING: Suppression impossible car des programmations sont associées à ce parcours");
      this.modal.close(); 
    }
  }

  //Appelée suite à l'event onCreate du customer route form
  public routeCreate(i: number) {
      this.isModifying = false;

      this.modal.close();     

      this.startDateVal = new Date("2000-01-01");
      this.myStartDate = { date: { year: this.startDateVal.getFullYear(), 
        month: this.startDateVal.getMonth()+1,
          day: this.startDateVal.getDate() } };

      this.endDateVal = new Date("2050-12-31");
      this.myEndDate = { date: { year: this.endDateVal.getFullYear(), 
        month: this.endDateVal.getMonth()+1,
          day: this.endDateVal.getDate() } };

/*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
        this._mapper.customerRouteListDetails  = data.json();  
        if(this._mapper.customerRouteListDetails!=undefined) {    
          this._mapper.customerRouteListToDisplayMapper();  
        }                 
      });   */

      this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(data => {
          this._mapper.customerRouteListDetails  = data.json();  
          if(this._mapper.customerRouteListDetails!=undefined) {    
            this._mapper.customerRouteListToDisplayMapper();  
          }    
      });  
  }


  constructor(private svca: CustomerRouteService) {
      //TODO: Récupérer les dates de départ au plus tôt et de fin au plus tard dans la base
      /*this.startDateVal = new Date(Date.now() - 1000*60*60*24);
      this.endDateVal = new Date(Date.now());*/
      this.startDateVal = new Date("2000-01-01");
      this.myStartDate = { date: { year: this.startDateVal.getFullYear(), 
        month: this.startDateVal.getMonth()+1,
         day: this.startDateVal.getDate() } };

      this.endDateVal = new Date("2050-12-31");
      this.myEndDate = { date: { year: this.endDateVal.getFullYear(), 
        month: this.endDateVal.getMonth()+1,
         day: this.endDateVal.getDate() } };

/*      this.svca.getCustomerRouteList(this.startDateVal, this.endDateVal).subscribe(data => {
        this._mapper.customerRouteListDetails  = data.json();  
        if(this._mapper.customerRouteListDetails!=undefined) {    
          this._mapper.customerRouteListToDisplayMapper();  
        }                 
      });   */

      this.svca.getCustomerRouteListOnDates(this.startDateVal, this.endDateVal).subscribe(data => {
          this._mapper.customerRouteListDetails  = data.json();  
          if(this._mapper.customerRouteListDetails!=undefined) {    
            this._mapper.customerRouteListToDisplayMapper();  
          }    
      }); 
  }
}