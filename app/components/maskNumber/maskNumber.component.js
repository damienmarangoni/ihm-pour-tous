"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var maskNumber_service_1 = require("../../services/maskNumber/maskNumber.service");
var maskNumber_mapper_1 = require("../../mappers/maskNumber.mapper");
/**************************************************************/
/* maskNumberComponent                                        */
/* Description : Gestion des numéros noirs                    */
/*                                                            */
/**************************************************************/
var MaskNumberComponent = /** @class */ (function () {
    function MaskNumberComponent(svmn) {
        var _this = this;
        this.svmn = svmn;
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = false;
        this._mapper = new maskNumber_mapper_1.MaskNumberMapper();
        this.svmn.getMaskNumberList().subscribe(function (data) {
            _this._mapper.maskNumberListDetails = data.json();
            if (_this._mapper.maskNumberListDetails != undefined) {
                _this._mapper.maskNumberListToDisplayMapper();
            }
        });
    }
    Object.defineProperty(MaskNumberComponent.prototype, "maskNumberList", {
        get: function () {
            return this._mapper.maskNumberListToDisplay;
        },
        enumerable: true,
        configurable: true
    });
    MaskNumberComponent.prototype._reafficheList = function () {
        var _this = this;
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isUpdating = false;
        this.svmn.getMaskNumberList().subscribe(function (data) {
            _this._mapper.maskNumberListDetails = data.json();
            if (_this._mapper.maskNumberListDetails != undefined) {
                _this._mapper.maskNumberListToDisplayMapper();
            }
        });
    };
    //Consultation d'un numéro noir
    MaskNumberComponent.prototype.consultMask = function (maskData) {
        var _this = this;
        this.isAdding = false;
        this.isConsulting = true;
        this.isDeleting = false;
        this.isUpdating = false;
        //alert("Consultation de" + maskData[1]);
        this.numeroGIS = maskData[1];
        this.descMask = maskData[0];
        this.numeroPublic = maskData[2];
        this.SMSMask = maskData[3];
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isUpdating) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du numeroGIS sur annulation
                //pour rechargement des données serveurs
                _this.numeroGIS = _this.numeroGIS + " ";
            }
        });
        this.modal.open();
    };
    //Ajout d'un numéro noir
    MaskNumberComponent.prototype.addMask = function (maskData) {
        var _this = this;
        this.isAdding = true;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isUpdating = false;
        //this._title = "118712 - Création numéro noir";
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isUpdating) {
                alert("Attention: Vos changements seront perdus");
                //Modification du numeroGIS sur annulation
                //pour rechargement des données serveurs
                _this.numeroGIS = _this.numeroGIS + " ";
            }
        });
        this.numeroGIS = "118712";
        this.numeroPublic = "";
        this.descMask = "";
        this.SMSMask = "";
        this.modal.open();
    };
    // Appelé suite à l'event on onSave du customer mask form 
    MaskNumberComponent.prototype.maskSave = function (numeroNoir) {
        var _this = this;
        this.svmn.updateMaskNumber(numeroNoir, numeroNoir.maskMaskedNb).subscribe(function (response) {
            _this.isUpdating = false;
            _this.modal.close();
            _this._reafficheList();
            _this.title = "118712 - Consultation numéros noirs";
        }, function (error) { alert("ERROR: La modification du numéro noir à échoué"); }, function () { });
    };
    // Appelé suite à l'event on onTestNum du customer mask form pour controler si le numéro GIS existe déjà
    MaskNumberComponent.prototype.restNumGIS = function (numeroNoir) {
        var _this = this;
        //alert("on vérifie" + numGIS);
        if (this._mapper.isNumeroGISExist(numeroNoir.maskMaskedNb)) {
            alert("Le numéro GIS " + numeroNoir.maskMaskedNb + " existe déjà");
        }
        else {
            this.svmn.createMaskNumber(numeroNoir).subscribe(function (response) {
                _this.isAdding = false;
                _this.modal.close();
                _this._reafficheList();
                //this.title = "118712 - Consultation numéros noirs"; 
            }, function (error) { alert("ERROR: La création du numéro noir à échoué"); }, function () { });
        }
    };
    //Suppression d'un numéro noir
    MaskNumberComponent.prototype.deleteMaskNumber = function (numeroGIS) {
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = true;
        this.isUpdating = false;
        this.numeroGIS = numeroGIS;
        //alert(numeroGIS);
        this.modal.open();
    };
    // Appel suite a l'appui de oui de la modal de supression
    MaskNumberComponent.prototype.onDelete = function (numGIS) {
        var _this = this;
        this.svmn.deleteMaskNumber(numGIS).subscribe(function (response) {
            _this.isDeleting = false;
            _this.modal.close();
            _this._reafficheList();
        }, function (error) { alert("ERROR: La suppression du numéro noir à échoué"); }, function () { });
    };
    //Appelée suite à l'event onModify du customer route form
    MaskNumberComponent.prototype.maskModify = function (i) {
        this.isUpdating = true;
    };
    // Appel sur fermeture de la fenêtre modal (delete ou detail)
    MaskNumberComponent.prototype.modalClose = function (i) {
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isUpdating = false;
        this.modal.close();
        /*this.svmn.getMaskNumberList().subscribe(data => {
              this._mapper.maskNumberListDetails  = data.json();
              if(this._mapper.maskNumberListDetails!=undefined) {
                this._mapper.maskNumberListToDisplayMapper();
              }
            });*/
    };
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], MaskNumberComponent.prototype, "modal", void 0);
    MaskNumberComponent = __decorate([
        core_1.Component({
            selector: 'masks',
            templateUrl: 'views/maskNumber/maskNumber.html',
            styleUrls: ['views/maskNumber/maskNumberDetails.css'],
            providers: [maskNumber_service_1.MaskNumberService]
        }),
        __metadata("design:paramtypes", [maskNumber_service_1.MaskNumberService])
    ], MaskNumberComponent);
    return MaskNumberComponent;
}());
exports.MaskNumberComponent = MaskNumberComponent;
//# sourceMappingURL=maskNumber.component.js.map