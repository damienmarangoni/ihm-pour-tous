import { Component,Output, EventEmitter } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { MaskNumberService } from '../../services/maskNumber/maskNumber.service';
import { MaskNumberForm, DeleteMaskNumberForm } from '../../forms/maskNumber/maskNumber.form';

import { MaskNumberMapper, MaskNumberToSend } from '../../mappers/maskNumber.mapper';

/**************************************************************/
/* maskNumberComponent                                        */
/* Description : Gestion des numéros noirs                    */
/*                                                            */
/**************************************************************/
@Component({
    selector: 'masks',
    templateUrl: 'views/maskNumber/maskNumber.html',
    styleUrls: ['views/maskNumber/maskNumberDetails.css'],
    providers: [MaskNumberService]
})

export class MaskNumberComponent {

    @ViewChild(ModalComponent) modal: ModalComponent;

    public numeroGIS: string;
    public descMask: string;
    public numeroPublic: string;
    public SMSMask: string;
    public title: string;
   
    private isDeleting: boolean = false;
    private isConsulting: boolean = false;
    private isAdding: boolean = false;
    private isUpdating: boolean = false;

    private _mapper: MaskNumberMapper = new MaskNumberMapper();
    private _title : string;
    
    
    public get maskNumberList(): Array<string[]> {
      return this._mapper.maskNumberListToDisplay;
    }

    private _reafficheList(){

        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isUpdating = false;
        this.svmn.getMaskNumberList().subscribe(data => {
                this._mapper.maskNumberListDetails  = data.json();  
                if(this._mapper.maskNumberListDetails!=undefined) {    
                    this._mapper.maskNumberListToDisplayMapper();  
                } 
        }); 
    }

    //Consultation d'un numéro noir
    public consultMask(maskData:string[]) {

        this.isAdding = false;
        this.isConsulting = true;
        this.isDeleting = false;
        this.isUpdating = false;
        
        //alert("Consultation de" + maskData[1]);
        this.numeroGIS = maskData[1];
        this.descMask = maskData[0];
        this.numeroPublic = maskData[2];
        this.SMSMask = maskData[3];
        
        this.modal.onDismiss.subscribe(_ => {
            if(this.isUpdating) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du numeroGIS sur annulation
                //pour rechargement des données serveurs
                this.numeroGIS = this.numeroGIS + " "; 
            }
        }); 
         this.modal.open();
    }

    //Ajout d'un numéro noir
    public addMask(maskData:string[]) {

        this.isAdding = true;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isUpdating = false;
        //this._title = "118712 - Création numéro noir";
        this.modal.onDismiss.subscribe(_ => {
            if(this.isUpdating) {
                alert("Attention: Vos changements seront perdus");        
                //Modification du numeroGIS sur annulation
                //pour rechargement des données serveurs
                this.numeroGIS = this.numeroGIS + " "; 
            }
        }); 
        this.numeroGIS = "118712";
        this.numeroPublic ="";
        this.descMask ="";
        this.SMSMask ="";
        this.modal.open();
    }

    // Appelé suite à l'event on onSave du customer mask form 
    public maskSave(numeroNoir : MaskNumberToSend ) {
        this.svmn.updateMaskNumber(numeroNoir, numeroNoir.maskMaskedNb).subscribe(
                        response => {
                            this.isUpdating = false;     
                            this.modal.close();   
                            this._reafficheList();                        
                            this.title = "118712 - Consultation numéros noirs"; 
                        },
                        error => {alert("ERROR: La modification du numéro noir à échoué");},
                        () =>{}
                    );        
    }

    // Appelé suite à l'event on onTestNum du customer mask form pour controler si le numéro GIS existe déjà
    public restNumGIS(numeroNoir : MaskNumberToSend ) {
        //alert("on vérifie" + numGIS);
        if (this._mapper.isNumeroGISExist(numeroNoir.maskMaskedNb)){
            alert("Le numéro GIS " + numeroNoir.maskMaskedNb + " existe déjà");
        }
        else{
            this.svmn.createMaskNumber(numeroNoir).subscribe(
                        response => {
                            this.isAdding = false;     
                            this.modal.close();   
                            this._reafficheList();                        
                            //this.title = "118712 - Consultation numéros noirs"; 
                        },
                        error => {alert("ERROR: La création du numéro noir à échoué");},
                        () =>{}
                    );        
            
        }

    }

  //Suppression d'un numéro noir
  public deleteMaskNumber(numeroGIS: string) {

        
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = true;
        this.isUpdating = false;

        this.numeroGIS = numeroGIS;

        //alert(numeroGIS);
        
        this.modal.open();
    }


  // Appel suite a l'appui de oui de la modal de supression
  public onDelete( numGIS : string){
         this.svmn.deleteMaskNumber(numGIS).subscribe(
            response => {
                    this.isDeleting = false;
                    this.modal.close(); 
                    this._reafficheList();
            },
            error => {alert("ERROR: La suppression du numéro noir à échoué");},
            () =>{}
        );        
  }

    //Appelée suite à l'event onModify du customer route form
  public maskModify(i: number) {
    this.isUpdating = true;
  }

  // Appel sur fermeture de la fenêtre modal (delete ou detail)
  public modalClose(i: number) {
    this.isAdding = false;
    this.isConsulting = false;
    this.isDeleting = false;
    this.isUpdating = false;
    this.modal.close(); 
    /*this.svmn.getMaskNumberList().subscribe(data => {
          this._mapper.maskNumberListDetails  = data.json();  
          if(this._mapper.maskNumberListDetails!=undefined) {    
            this._mapper.maskNumberListToDisplayMapper();  
          } 
        });*/
  }

    
 

    public constructor(private svmn: MaskNumberService) {

       this.svmn.getMaskNumberList().subscribe(data => {
          this._mapper.maskNumberListDetails  = data.json();  
          if(this._mapper.maskNumberListDetails!=undefined) {    
            this._mapper.maskNumberListToDisplayMapper();  
          } 
        });
    }
}