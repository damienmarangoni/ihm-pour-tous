"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var ng2_popup_1 = require("ng2-popup");
var mediaSimulator_service_1 = require("../../services/mediaSimulators/mediaSimulator.service");
var audio_service_1 = require("../../services/audio/audio.service");
var DialogSimulatorComponent = /** @class */ (function () {
    function DialogSimulatorComponent(svcs, svas) {
        var _this = this;
        this.svcs = svcs;
        this.svas = svas;
        this._routeName = "";
        //Pour chaque phase on aura un résultat avec:
        //    - CodePhase
        //    - templateName (=> nom de la programmation)
        //    - templateContentTxt (=> nom du fichier audio à écouter)
        this._results = new Array();
        //Initialisation des attributs des datepickers
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '12.5em'
        };
        this.myDu = "";
        this.myCommunity = "";
        this.mySegment = "";
        this.mySimulDate = "";
        this.audioFile = "";
        this.isValidating = false;
        //Récupération des données de référence
        svcs.getDUList().subscribe(function (data) {
            _this._duList = data.json();
            _this.myDu = 'DU ' + _this._duList[0].serviceDu + ' ' + _this._duList[0].serviceDescription;
        });
        svcs.getCommunitiesList().subscribe(function (data) {
            _this._communitiesList = data.json();
            _this.myCommunity = _this._communitiesList[0].libCommunity;
        });
        svcs.getSegmentsList().subscribe(function (data) {
            _this._segmentsList = data.json();
            _this.mySegment = _this._segmentsList[_this._segmentsList.length - 1].libCategory;
        });
        svcs.getPhasesList().subscribe(function (data) {
            _this._phasesList = data.json();
        });
    }
    Object.defineProperty(DialogSimulatorComponent.prototype, "routeName", {
        //Getters and setters
        get: function () {
            return this._routeName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DialogSimulatorComponent.prototype, "duList", {
        get: function () {
            return this._duList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DialogSimulatorComponent.prototype, "communitiesList", {
        get: function () {
            return this._communitiesList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DialogSimulatorComponent.prototype, "segmentsList", {
        get: function () {
            return this._segmentsList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DialogSimulatorComponent.prototype, "phasesList", {
        get: function () {
            return this._phasesList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DialogSimulatorComponent.prototype, "results", {
        get: function () {
            return this._results;
        },
        enumerable: true,
        configurable: true
    });
    //Methods
    DialogSimulatorComponent.prototype.getCustomerRouteSimulation = function () {
        var _this = this;
        if (this.mySimulDate != "") {
            this.isValidating = true;
            var message = [];
            message.push(this._duList.find(function (du) { return "DU " + du.serviceDu + " " + du.serviceDescription == _this.myDu; }).serviceDu.toString());
            message.push(this.mySegment);
            message.push(this._communitiesList.find(function (comm) { return comm.libCommunity == _this.myCommunity; }).codeCommunity.toString());
            this._results = new Array();
            var _loop_1 = function (i) {
                var customerRouteReceived = new Array();
                this_1.svcs.getProgramParentList(message, this_1.phasesList[i].codePhase, this_1.mySimulDate).subscribe(function (data) {
                    //Récupération des données depuis le serveur
                    customerRouteReceived = data.json();
                    //Détermination du parcours à utiliser (le parcours avec les critères les plus précis)
                    var index = 0;
                    if (customerRouteReceived.length > 1) {
                        for (var i_1 = 0; i_1 < customerRouteReceived.length; i_1++) {
                            if (customerRouteReceived[i_1].qualificationExpressionsDTOs[0] != undefined && (i_1 >= 1)) {
                                if ((customerRouteReceived[i_1].qualificationExpressionsDTOs.length <
                                    customerRouteReceived[i_1 - 1].qualificationExpressionsDTOs.length)) {
                                    index = i_1;
                                }
                            }
                        }
                    }
                    //Récupération des programmations associées
                    var result = new Result();
                    if (customerRouteReceived[index] != undefined && customerRouteReceived[index].programParentDTOs[0] != undefined) {
                        if (customerRouteReceived[index].programParentDTOs[0].template.codePhase != "ENVOI" &&
                            customerRouteReceived[index].programParentDTOs[0].template.codePhase == _this.phasesList[i].codePhase) {
                            _this._routeName = customerRouteReceived[index].customerQualificationName;
                            result.codePhase = _this.phasesList[i].codePhase;
                            result.templateName = customerRouteReceived[index].programParentDTOs[0].template.templateName;
                            if (customerRouteReceived[index].programParentDTOs[0].templateContent != undefined) {
                                result.templateContentName = customerRouteReceived[index].programParentDTOs[0].templateContent.templateContentTxt;
                            }
                        }
                        else {
                            result.codePhase = _this.phasesList[i].codePhase;
                            result.templateName = "Template par défaut";
                            result.templateContentName = "";
                        }
                    }
                    else {
                        result.codePhase = _this.phasesList[i].codePhase;
                        result.templateName = "Template par défaut";
                        result.templateContentName = "";
                    }
                    if (_this.phasesList[i].codePhase != "ENVOI") {
                        _this._results.push(result);
                    }
                });
            };
            var this_1 = this;
            for (var i = 0; i < this.phasesList.length; i++) {
                _loop_1(i);
            }
        }
        else {
            alert("WARNING: Vous devez renseigner une date de simulation");
        }
    };
    DialogSimulatorComponent.prototype.modifyDate = function (e) {
        var day;
        var month;
        if (e.date.day.toString().length == 2) {
            day = e.date.day.toString();
        }
        else {
            day = "0" + e.date.day.toString();
        }
        if (e.date.month.toString().length == 2) {
            month = e.date.month.toString();
        }
        else {
            month = "0" + e.date.month.toString();
        }
        this.mySimulDate = e.date.year + "-" + month + "-" + day;
    };
    DialogSimulatorComponent.prototype.openPopup = function (file) {
        if (file != "" && file != null) {
            this.svas.getAudioFile(file).subscribe(function (data) { window.open(data.url); });
        }
        else {
            alert("TODO: Cas des Template par défaut à traiter");
        }
    };
    __decorate([
        core_2.ViewChild(ng2_popup_1.Ng2PopupComponent),
        __metadata("design:type", ng2_popup_1.Ng2PopupComponent)
    ], DialogSimulatorComponent.prototype, "popup", void 0);
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], DialogSimulatorComponent.prototype, "modal", void 0);
    DialogSimulatorComponent = __decorate([
        core_1.Component({
            selector: 'dialogSimulator',
            templateUrl: 'views/mediaSimulators/dialogSimulator.html',
            styleUrls: ['views/customerRoute/customerRoute.css', 'views/mediaSimulators/mediaSimulators.css', 'views/common/geometry.css'],
            providers: [mediaSimulator_service_1.SimulatorDataService]
        }),
        __metadata("design:paramtypes", [mediaSimulator_service_1.SimulatorDataService, audio_service_1.AudioInfosService])
    ], DialogSimulatorComponent);
    return DialogSimulatorComponent;
}());
exports.DialogSimulatorComponent = DialogSimulatorComponent;
var RefPhase = /** @class */ (function () {
    function RefPhase() {
    }
    return RefPhase;
}());
var Result = /** @class */ (function () {
    function Result() {
    }
    return Result;
}());
//# sourceMappingURL=dialogSimulator.component.js.map