﻿import { Component, Input, Directive } from '@angular/core';
import { ViewChild, ViewEncapsulation, ContentChild } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { DatePipe } from '@angular/common';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Ng2PopupComponent, Ng2MessagePopupComponent } from 'ng2-popup';
import { Ng2Overlay } from 'ng2-overlay';
import { AudioPlayerComponent } from '../audio/audioPlayer.component';
import { CustomerRouteDetails, ProgramParent, Service, RefCategory, RefCommunity } from '../../mappers/customerRoute.mapper';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { AudioInfosService } from '../../services/audio/audio.service';


@Component({
    selector: 'dialogSimulator',
    templateUrl: 'views/mediaSimulators/dialogSimulator.html',
    styleUrls: ['views/customerRoute/customerRoute.css','views/mediaSimulators/mediaSimulators.css', 'views/common/geometry.css'],
    providers: [SimulatorDataService]
})

export class DialogSimulatorComponent {

    @ViewChild(Ng2PopupComponent) popup: Ng2PopupComponent;

    @ViewChild(ModalComponent) modal: ModalComponent;  

    private _routeName: string = "";

    private _duList: Service[];
    private _communitiesList: RefCommunity[];
    private _segmentsList: RefCategory[];
    private _phasesList: RefPhase[];    
    private _isRouteWithProg:boolean;

    //Pour chaque phase on aura un résultat avec:
    //    - CodePhase
    //    - templateName (=> nom de la programmation)
    //    - templateContentTxt (=> nom du fichier audio à écouter)
    private _results: Array<Result> = new Array<Result>();

    public isValidating: boolean;

    //Initialisation des attributs des datepickers
    private myDatePickerOptions: IMyDpOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '12.5em'
        };

    private myDate: Object;

    public myDu:string = "";
    public myCommunity:string = "";
    public mySegment:string = "";
    public mySimulDate:string = "";

    public audioFile:string = "";

    //Getters and setters
    public get routeName(): string {
        return this._routeName; 
    } 

    public get duList(): Service[] {
        return this._duList; 
    } 

    public get communitiesList(): RefCommunity[] {
        return this._communitiesList; 
    } 

    public get segmentsList(): RefCategory[] {
        return this._segmentsList; 
    } 

    public get phasesList(): RefPhase[] {
        return this._phasesList; 
    }     

    public get results(): Array<Result> {
        return this._results; 
    } 

    //Methods
    public getCustomerRouteSimulation() {

        if(this.mySimulDate!="") {
            this.isValidating=true

            let message:string[] = [];
            message.push(this._duList.find(du => "DU " + du.serviceDu + " " + du.serviceDescription==this.myDu).serviceDu.toString());
            message.push(this.mySegment);
            message.push(this._communitiesList.find(comm => comm.libCommunity==this.myCommunity).codeCommunity.toString());

            this._results = new Array<Result>();
            for(let i=0;i<this.phasesList.length;i++) {

                let customerRouteReceived: Array<CustomerRouteDetails> = new Array<CustomerRouteDetails>();  
                this.svcs.getProgramParentList(message, this.phasesList[i].codePhase, this.mySimulDate).subscribe(data => {

                    //Récupération des données depuis le serveur
                    customerRouteReceived = data.json();     

                    //Détermination du parcours à utiliser (le parcours avec les critères les plus précis)
                    let index:number=0;
                    if(customerRouteReceived.length>1) {
                        for(let i=0;i<customerRouteReceived.length;i++) {
                            if(customerRouteReceived[i].qualificationExpressionsDTOs[0]!=undefined&&(i>=1)) {
                                if((customerRouteReceived[i].qualificationExpressionsDTOs.length<
                                    customerRouteReceived[i-1].qualificationExpressionsDTOs.length)) {
                                        index = i;
                                }
                            }
                        }
                    }

                    //Récupération des programmations associées
                    let result: Result = new Result();
                    if(customerRouteReceived[index]!=undefined&&customerRouteReceived[index].programParentDTOs[0]!=undefined) {
      
                        if(customerRouteReceived[index].programParentDTOs[0].template.codePhase!="ENVOI"&&
                            customerRouteReceived[index].programParentDTOs[0].template.codePhase==this.phasesList[i].codePhase){

                            this._routeName = customerRouteReceived[index].customerQualificationName; 
                            
                            result.codePhase = this.phasesList[i].codePhase;
                            result.templateName = customerRouteReceived[index].programParentDTOs[0].template.templateName;
                            if(customerRouteReceived[index].programParentDTOs[0].templateContent!=undefined) {

                                result.templateContentName = customerRouteReceived[index].programParentDTOs[0].templateContent.templateContentTxt;
                            }
                        }
                        else {                            

                            result.codePhase = this.phasesList[i].codePhase;
                            result.templateName = "Template par défaut";
                            result.templateContentName = "";
                        }
                    }
                    else {           

                        result.codePhase = this.phasesList[i].codePhase;
                        result.templateName = "Template par défaut";
                        result.templateContentName = "";
                    }    
            
                    if(this.phasesList[i].codePhase!="ENVOI") {

                        this._results.push(result);
                    }     
                });
            }
            
        }
        else {
            alert("WARNING: Vous devez renseigner une date de simulation")
        }
    }

    public modifyDate(e: any) {

        let day:string;
        let month:string;
        if(e.date.day.toString().length==2) {
            day = e.date.day.toString();
        }
        else {            
            day = "0" + e.date.day.toString();
        }
        if(e.date.month.toString().length==2) {
            month = e.date.month.toString();
        }
        else {            
            month = "0" + e.date.month.toString();
        }

        this.mySimulDate = e.date.year + "-" + month + "-" + day;
    }

    public openPopup(file:string) {

        if(file!=""&&file!=null) {

            this.svas.getAudioFile(file).subscribe(
                data => { window.open(data.url);}
            );  
        } 
        else {
            alert("TODO: Cas des Template par défaut à traiter")
        }
    }


    constructor(private svcs: SimulatorDataService, private svas: AudioInfosService) {

        this.isValidating = false;

        //Récupération des données de référence
        svcs.getDUList().subscribe(data => {
            this._duList = data.json();
            this.myDu = 'DU '+ this._duList[0].serviceDu + ' ' + this._duList[0].serviceDescription
        });
        
        svcs.getCommunitiesList().subscribe(data => {
            this._communitiesList = data.json();            
            this.myCommunity = this._communitiesList[0].libCommunity;
        });
        
        svcs.getSegmentsList().subscribe(data => {
            this._segmentsList = data.json();
            this.mySegment = this._segmentsList[this._segmentsList.length-1].libCategory;
        });
        
        svcs.getPhasesList().subscribe(data => {
            this._phasesList = data.json()
        });
    }
}

class RefPhase {
    codePhase: string;
    libPhase: string;
    maxTrack: number;
}

class Result {
    codePhase: string;
    templateName: string;
    templateContentName: string;
}