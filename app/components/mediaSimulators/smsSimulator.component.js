"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var ng2_popup_1 = require("ng2-popup");
var mediaSimulator_service_1 = require("../../services/mediaSimulators/mediaSimulator.service");
var audio_service_1 = require("../../services/audio/audio.service");
var SmsSimulatorComponent = /** @class */ (function () {
    function SmsSimulatorComponent(svcs, svas) {
        var _this = this;
        this.svcs = svcs;
        this.svas = svas;
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '12.5em'
        };
        this.myDu = "";
        this.myCommunity = "";
        this.mySegment = "";
        this.mySimulDate = "";
        this.isValidating = false;
        //Récupération des données de référence
        svcs.getDUList().subscribe(function (data) {
            _this._duList = data.json();
            _this.myDu = 'DU ' + _this._duList[0].serviceDu + ' ' + _this._duList[0].serviceDescription;
        });
        svcs.getCommunitiesList().subscribe(function (data) {
            _this._communitiesList = data.json();
            _this.myCommunity = _this._communitiesList[0].libCommunity;
        });
        svcs.getSegmentsList().subscribe(function (data) {
            _this._segmentsList = data.json();
            _this.mySegment = _this._segmentsList[_this._segmentsList.length - 1].libCategory;
        });
    }
    Object.defineProperty(SmsSimulatorComponent.prototype, "duList", {
        get: function () {
            return this._duList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SmsSimulatorComponent.prototype, "communitiesList", {
        get: function () {
            return this._communitiesList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SmsSimulatorComponent.prototype, "segmentsList", {
        get: function () {
            return this._segmentsList;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_2.ViewChild(ng2_popup_1.Ng2PopupComponent),
        __metadata("design:type", ng2_popup_1.Ng2PopupComponent)
    ], SmsSimulatorComponent.prototype, "popup", void 0);
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], SmsSimulatorComponent.prototype, "modal", void 0);
    SmsSimulatorComponent = __decorate([
        core_1.Component({
            selector: 'smsSimulator',
            templateUrl: 'views/mediaSimulators/smsSimulator.html',
            styleUrls: ['views/common/118712IHM.css', 'views/mediaSimulators/mediaSimulators.css', 'views/common/geometry.css'],
            providers: [mediaSimulator_service_1.SimulatorDataService]
        }),
        __metadata("design:paramtypes", [mediaSimulator_service_1.SimulatorDataService, audio_service_1.AudioInfosService])
    ], SmsSimulatorComponent);
    return SmsSimulatorComponent;
}());
exports.SmsSimulatorComponent = SmsSimulatorComponent;
//# sourceMappingURL=smsSimulator.component.js.map