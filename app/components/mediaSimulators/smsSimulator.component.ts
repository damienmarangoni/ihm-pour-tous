import { Component, Input, Directive } from '@angular/core';
import { ViewChild, ViewEncapsulation, ContentChild } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
import { DatePipe } from '@angular/common';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Ng2PopupComponent, Ng2MessagePopupComponent } from 'ng2-popup';
import { Ng2Overlay } from 'ng2-overlay';
import { AudioPlayerComponent } from '../audio/audioPlayer.component';
import { CustomerRouteDetails, ProgramParent, Service, RefCategory, RefCommunity } from '../../mappers/customerRoute.mapper';

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { AudioInfosService } from '../../services/audio/audio.service';
import { Environment } from '../../conf/environment'


@Component({
    selector: 'smsSimulator',
    templateUrl: 'views/mediaSimulators/smsSimulator.html',
    styleUrls: ['views/common/118712IHM.css', 'views/mediaSimulators/mediaSimulators.css', 'views/common/geometry.css'],
    providers: [SimulatorDataService]
})

export class SmsSimulatorComponent {

    @ViewChild(Ng2PopupComponent) popup: Ng2PopupComponent;

    @ViewChild(ModalComponent) modal: ModalComponent;

    private _duList: Service[];
    private _communitiesList: RefCommunity[];
    private _segmentsList: RefCategory[];

    private myDate: Object;

    private myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        inline: false,
        width: '12.5em'
    };

    public isValidating: boolean;
    public myDu: string = "";
    public myCommunity: string = "";
    public mySegment: string = "";
    public mySimulDate: string = "";

    public get duList(): Service[] {
        return this._duList;
    }

    public get communitiesList(): RefCommunity[] {
        return this._communitiesList;
    }

    public get segmentsList(): RefCategory[] {
        return this._segmentsList;
    }

        constructor(private svcs: SimulatorDataService, private svas: AudioInfosService) {

        this.isValidating = false;

        //Récupération des données de référence
        svcs.getDUList().subscribe(data => {
            this._duList = data.json();
            this.myDu = 'DU '+ this._duList[0].serviceDu + ' ' + this._duList[0].serviceDescription
            
        });
        
        svcs.getCommunitiesList().subscribe(data => {
            this._communitiesList = data.json();            
            this.myCommunity = this._communitiesList[0].libCommunity;
        });
        
        svcs.getSegmentsList().subscribe(data => {
            this._segmentsList = data.json();
            this.mySegment = this._segmentsList[this._segmentsList.length-1].libCategory;
        });
    }


}