"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var writtenMessage_service_1 = require("../../services/writtenMessage/writtenMessage.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var WrittenMessageModelComponent = /** @class */ (function () {
    function WrittenMessageModelComponent(svcr) {
        var _this = this;
        this.svcr = svcr;
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isModifying = false;
        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(function (data) {
            _this._templates = data.json();
        });
    }
    WrittenMessageModelComponent.prototype.consultModel = function (templateName, idTemplate) {
        var _this = this;
        this.isAdding = false;
        this.isConsulting = true;
        this.isDeleting = false;
        this.isModifying = false;
        this.templateName = templateName;
        this.idTemplate = idTemplate;
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du routeName sur annulation
                //pour rechargement des données serveurs
                _this.templateName = _this.templateName + " ";
            }
        });
        this.modal.open();
    };
    WrittenMessageModelComponent.prototype.addModel = function () {
        var _this = this;
        this.isAdding = true;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isModifying = false;
        this.modal.onDismiss.subscribe(function (_) {
            if (_this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du routeName sur annulation
                //pour rechargement des données serveurs
                _this.templateName = _this.templateName + " ";
            }
        });
        this.modal.open();
    };
    WrittenMessageModelComponent.prototype.deleteModel = function (templateName, idTemplate) {
        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = true;
        this.isModifying = false;
        this.templateName = templateName;
        this.idTemplate = idTemplate;
        this.modal.open();
    };
    WrittenMessageModelComponent.prototype.tempSave = function (e) {
        var _this = this;
        this.isModifying = false;
        this.templateName = this.templateName + " ";
        this.modal.close();
        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(function (data) {
            _this._templates = data.json();
        });
    };
    WrittenMessageModelComponent.prototype.tempModify = function (e) {
        this.isModifying = true;
    };
    WrittenMessageModelComponent.prototype.tempCancel = function (e) {
        this.isModifying = false;
    };
    //Appelée suite à l'event onCreate du template form
    WrittenMessageModelComponent.prototype.tempCreate = function (e) {
        var _this = this;
        this.isModifying = false;
        this.templateName = this.templateName + " ";
        this.modal.close();
        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(function (data) {
            _this._templates = data.json();
        });
    };
    //Appelée suite à l'event onDelete du template delete form
    WrittenMessageModelComponent.prototype.onDelete = function (i) {
        var _this = this;
        this.svcr.deleteWrittenMessageModel(i.toString()).subscribe(function (response) {
            _this.modal.close();
            _this.svcr.getWrittenMessageModelList("ENVOI").subscribe(function (data) {
                _this._templates = data.json();
            });
        }, function (error) {
            alert("ERROR: La suppression du parcours client a echouée");
        }, function () { });
    };
    //Appelée suite à l'event onClose du template delete form
    WrittenMessageModelComponent.prototype.onClose = function (i) {
        this.modal.close();
    };
    __decorate([
        core_1.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], WrittenMessageModelComponent.prototype, "modal", void 0);
    WrittenMessageModelComponent = __decorate([
        core_1.Component({
            selector: 'writtenMessageModel',
            templateUrl: 'views/writtenMessage/writtenMessageModel.html',
            styleUrls: ['views/writtenMessage/writtenMessageModel.css'],
            providers: [writtenMessage_service_1.WrittenMessageService]
        }),
        __metadata("design:paramtypes", [writtenMessage_service_1.WrittenMessageService])
    ], WrittenMessageModelComponent);
    return WrittenMessageModelComponent;
}());
exports.WrittenMessageModelComponent = WrittenMessageModelComponent;
//# sourceMappingURL=writtenMessageModel.component.js.map