import {Component, ViewChild} from '@angular/core';

import {WrittenMessageService} from '../../services/writtenMessage/writtenMessage.service';
import {Template} from '../../mappers/writtenMessage.mapper';
import {ModalComponent} from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'writtenMessageModel',
    templateUrl: 'views/writtenMessage/writtenMessageModel.html',
    styleUrls: ['views/writtenMessage/writtenMessageModel.css'],
    providers: [WrittenMessageService]
})

export class WrittenMessageModelComponent {

    @ViewChild(ModalComponent) modal: ModalComponent; 

    private _templates: Array<Template>; 

    public templateName: string;
    public idTemplate: string;

    private isAdding: boolean = false;
    private isConsulting: boolean = false;
    private isDeleting: boolean = false;
    private isModifying:boolean = false;

    public consultModel(templateName: string, idTemplate: string) {

        this.isAdding = false;
        this.isConsulting = true;
        this.isDeleting = false;
        this.isModifying = false;

        this.templateName = templateName;
        this.idTemplate = idTemplate;

        this.modal.onDismiss.subscribe(_ => {
            if(this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du routeName sur annulation
                //pour rechargement des données serveurs
                this.templateName = this.templateName + " "; 
            }
        });
        this.modal.open();
    }

    public addModel() {

        this.isAdding = true;
        this.isConsulting = false;
        this.isDeleting = false;
        this.isModifying = false;

        this.modal.onDismiss.subscribe(_ => {
            if(this.isModifying) {
                //alert("Attention: Vos changements seront perdus");        
                //Modification du routeName sur annulation
                //pour rechargement des données serveurs
                this.templateName = this.templateName + " "; 
            }
        });
        this.modal.open();
    }

    public deleteModel(templateName: string, idTemplate: string) {

        this.isAdding = false;
        this.isConsulting = false;
        this.isDeleting = true;
        this.isModifying = false;

        this.templateName = templateName;
        this.idTemplate = idTemplate;

        this.modal.open();
    }

    public tempSave(e:any) {
        
        this.isModifying = false;
        this.templateName = this.templateName + " "; 
        
        this.modal.close();  
        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(data => {
            this._templates  = data.json(); 
        });
    }

    public tempModify(e:any) {

        this.isModifying = true;
    }

    public tempCancel(e:any) {
        
        this.isModifying = false;        
    }

    //Appelée suite à l'event onCreate du template form
    public tempCreate(e:any) {
        
        this.isModifying = false;
        this.templateName = this.templateName + " "; 
        
        this.modal.close();  
        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(data => {
            this._templates  = data.json(); 
        });
    }
  
    //Appelée suite à l'event onDelete du template delete form
    public onDelete(i: number) {
        
        this.svcr.deleteWrittenMessageModel(i.toString()).subscribe(
                response => {
                    this.modal.close();   
                    this.svcr.getWrittenMessageModelList("ENVOI").subscribe(data => {
                        this._templates  = data.json(); 
                    });                         
                },
                error => {
                    alert("ERROR: La suppression du parcours client a echouée");
                },
                () =>{}   
            );
    }

    //Appelée suite à l'event onClose du template delete form
    public onClose(i: number) {
        
        this.modal.close();  
    }


    public constructor(private svcr: WrittenMessageService) {

        this.svcr.getWrittenMessageModelList("ENVOI").subscribe(data => {
            this._templates  = data.json(); 
        });
    }
}