"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var writtenMessage_service_1 = require("../../services/writtenMessage/writtenMessage.service");
var writtenMessage_mapper_1 = require("../../mappers/writtenMessage.mapper");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var WrittenMessageTextComponent = /** @class */ (function () {
    function WrittenMessageTextComponent(svcr) {
        var _this = this;
        this.svcr = svcr;
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = false;
        this.svcr.getWrittenMessageTextList().subscribe(function (data) {
            _this._templatesContents = data.json();
        });
    }
    WrittenMessageTextComponent.prototype.consultText = function (temp) {
        this.templateContentName = temp.templateContentName;
        this.contractCustomerLabel = temp.contractCustomerLabel;
        this.templateContentTxt = temp.templateContentTxt;
        this.templateContentShortTxt = temp.templateContentShortTxt;
        this.templateContent = temp;
        this.isDeleting = false;
        this.isConsulting = true;
        this.isAdding = false;
        this.isUpdating = false;
        this.modal.open();
    };
    WrittenMessageTextComponent.prototype.updateWrittMessText = function () {
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = true;
        this.modal.open();
    };
    WrittenMessageTextComponent.prototype.createWrittMessText = function () {
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = true;
        this.isUpdating = false;
        this.templateContent = new writtenMessage_mapper_1.TemplateContent();
        this.modal.open();
    };
    WrittenMessageTextComponent.prototype.deleteWrittMessText = function (templateContentId, templateContentName) {
        this.isDeleting = true;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = false;
        this.templateContentId = templateContentId;
        this.templateContentName = templateContentName;
        this.modal.open();
    };
    WrittenMessageTextComponent.prototype.wtmSave = function (e) {
        var _this = this;
        this.isUpdating = false;
        this.modal.close();
        this.svcr.getWrittenMessageTextList().subscribe(function (data) {
            _this._templatesContents = data.json();
        });
    };
    //méthodes appelées sur les évènements onSave, onUpdate dans le composant formulaire
    WrittenMessageTextComponent.prototype.wmtUpdate = function (e) {
        this.isUpdating = true;
    };
    WrittenMessageTextComponent.prototype.wmtCancel = function (e) {
        this.isUpdating = false;
    };
    WrittenMessageTextComponent.prototype.wmtCreate = function (e) {
        var _this = this;
        this.modal.close();
        this.isUpdating = false;
        this.svcr.getWrittenMessageTextList().subscribe(function (data) {
            _this._templatesContents = data.json();
        });
    };
    //Appelée suite à l'event onClose 
    WrittenMessageTextComponent.prototype.modalClose = function (i) {
        this.modal.close();
    };
    //Appelée suite à l'event onDelete du written message text delete form
    WrittenMessageTextComponent.prototype.onDelete = function (templateContentId) {
        var _this = this;
        this.svcr.deleteWrittenMessageText(templateContentId).subscribe(function (response) {
            _this.modal.close();
            _this.svcr.getWrittenMessageTextList().subscribe(function (data) {
                _this._templatesContents = data.json();
            });
        }, function (error) {
            alert("ERROR: La suppression du message écrit a echouée");
        }, function () { });
    };
    __decorate([
        core_1.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], WrittenMessageTextComponent.prototype, "modal", void 0);
    WrittenMessageTextComponent = __decorate([
        core_1.Component({
            selector: 'writtenMessageText',
            templateUrl: 'views/writtenMessage/writtenMessageText.html',
            styleUrls: ['views/writtenMessage/writtenMessageText.css'],
            providers: [writtenMessage_service_1.WrittenMessageService]
        }),
        __metadata("design:paramtypes", [writtenMessage_service_1.WrittenMessageService])
    ], WrittenMessageTextComponent);
    return WrittenMessageTextComponent;
}());
exports.WrittenMessageTextComponent = WrittenMessageTextComponent;
//# sourceMappingURL=writtenMessageText.component.js.map