import { Component, ViewChild } from '@angular/core';

import { WrittenMessageService } from '../../services/writtenMessage/writtenMessage.service';
import { WrittenMessageTextForm } from '../../forms/writtenMessage/writtenMessageText.form';
import { TemplateContent } from '../../mappers/writtenMessage.mapper';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

@Component({
    selector: 'writtenMessageText',
    templateUrl: 'views/writtenMessage/writtenMessageText.html',
    styleUrls: ['views/writtenMessage/writtenMessageText.css'],
    providers: [WrittenMessageService]
})

export class WrittenMessageTextComponent {

    @ViewChild(ModalComponent) modal: ModalComponent;

    private _templatesContents: Array<TemplateContent>;

    private isDeleting: boolean = false;
    private isConsulting: boolean = false;
    private isAdding: boolean = false;
    private isUpdating: boolean = false;

    private templateContent: TemplateContent;

    private templateContentName: string;
    private templateContentId: string;
    private contractCustomerLabel: string;
    private templateContentTxt: string;
    private templateContentShortTxt: string;

    public consultText(temp: TemplateContent) {

        this.templateContentName = temp.templateContentName;
        this.contractCustomerLabel = temp.contractCustomerLabel;
        this.templateContentTxt = temp.templateContentTxt;
        this.templateContentShortTxt = temp.templateContentShortTxt;
        this.templateContent = temp;

        this.isDeleting = false;
        this.isConsulting = true;
        this.isAdding = false;
        this.isUpdating = false;

        this.modal.open();
    }

    public updateWrittMessText() {
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = true;
        this.modal.open();

    }

    public createWrittMessText() {
        this.isDeleting = false;
        this.isConsulting = false;
        this.isAdding = true;
        this.isUpdating = false;

        this.templateContent = new TemplateContent();
        
        this.modal.open();
    }

    public deleteWrittMessText(templateContentId: string, templateContentName: string) {
        this.isDeleting = true;
        this.isConsulting = false;
        this.isAdding = false;
        this.isUpdating = false;

        this.templateContentId = templateContentId;
        this.templateContentName = templateContentName;

        this.modal.open();


    }

    public wtmSave(e:any) {
        
        this.isUpdating = false;
        
        this.modal.close();  
         this.svcr.getWrittenMessageTextList().subscribe(data => {
            this._templatesContents = data.json();
        });
    }

//méthodes appelées sur les évènements onSave, onUpdate dans le composant formulaire
    public wmtUpdate(e:any) {
        this.isUpdating = true;
    }

    public wmtCancel(e:any) {
        this.isUpdating = false;        
    }

    public wmtCreate(e:any) {
        
        this.modal.close();
        
        this.isUpdating = false;
        this.svcr.getWrittenMessageTextList().subscribe(data => {
            this._templatesContents = data.json();
        });
              
    }

    //Appelée suite à l'event onClose 
    public modalClose(i: number) {
        this.modal.close();
    }


    //Appelée suite à l'event onDelete du written message text delete form
    public onDelete(templateContentId: string) {
        this.svcr.deleteWrittenMessageText(templateContentId).subscribe(
            response => {
                this.modal.close();
                this.svcr.getWrittenMessageTextList().subscribe(data => {
                    this._templatesContents = data.json();
                });
            },
            error => {
                alert("ERROR: La suppression du message écrit a echouée");
            },
            () => { }
        );
    }

    
    public constructor(private svcr: WrittenMessageService) {

        this.svcr.getWrittenMessageTextList().subscribe(data => {
            this._templatesContents = data.json();
        });
    }
}