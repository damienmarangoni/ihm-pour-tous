"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var Environment = /** @class */ (function () {
    function Environment(_proxy) {
        var _this = this;
        this._proxy = _proxy;
        //private _baseUrl = "http://localhost:8080/";
        this._env = new Env();
        var url = "../conf/environment.json";
        this._proxy.get(url).subscribe(function (data) {
            _this._env = data.json();
        });
    }
    Object.defineProperty(Environment.prototype, "baseUrl", {
        get: function () {
            return "http://" + this._env.host + ":" + this._env.port + "/";
        },
        enumerable: true,
        configurable: true
    });
    Environment = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject(http_1.Http)),
        __metadata("design:paramtypes", [http_1.Http])
    ], Environment);
    return Environment;
}());
exports.Environment = Environment;
var Env = /** @class */ (function () {
    function Env() {
    }
    return Env;
}());
//# sourceMappingURL=environment.js.map