import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject, Injectable } from '@angular/core'

@Injectable()
export class Environment {
    //private _baseUrl = "http://localhost:8080/";
    public _env:Env = new Env();

    public get baseUrl() {

        return "http://" + this._env.host + ":" + this._env.port + "/";
    }


    public constructor(@Inject(Http) private _proxy: Http) {

        let url:string = "../conf/environment.json";
        this._proxy.get(url).subscribe(data => {
            this._env = data.json()            
        });
    }
}

class Env {
    host:string;
    port:string;
}