"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var ref_service_1 = require("../../services/ref/ref.service");
var writtenMessage_mapper_1 = require("../../mappers/writtenMessage.mapper");
var audio_service_1 = require("../../services/audio/audio.service");
var audioMessage_service_1 = require("../../services/audioMessage/audioMessage.service");
var DeleteCustomAudioMessageForm = /** @class */ (function () {
    function DeleteCustomAudioMessageForm() {
        this.onClose = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    DeleteCustomAudioMessageForm.prototype.deleteMessage = function () {
        this.onDelete.emit(this.idMessage);
    };
    DeleteCustomAudioMessageForm.prototype.closeWindow = function () {
        this.onClose.emit();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DeleteCustomAudioMessageForm.prototype, "messageName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], DeleteCustomAudioMessageForm.prototype, "idMessage", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteCustomAudioMessageForm.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteCustomAudioMessageForm.prototype, "onDelete", void 0);
    DeleteCustomAudioMessageForm = __decorate([
        core_1.Component({ selector: 'deleteCustomAudioMessageForm',
            templateUrl: 'views/audioMessage/deleteCustomAudioMessage.html',
            styleUrls: ['views/audioMessage/customAudioMessage.css',
                'views/customerRoute/customerRoute.css'],
        })
    ], DeleteCustomAudioMessageForm);
    return DeleteCustomAudioMessageForm;
}());
exports.DeleteCustomAudioMessageForm = DeleteCustomAudioMessageForm;
var CustomAudioMessageForm = /** @class */ (function () {
    //Constructor
    function CustomAudioMessageForm(svas, svam, svre) {
        var _this = this;
        this.svas = svas;
        this.svam = svam;
        this.svre = svre;
        this.onCancel = new core_1.EventEmitter();
        this.onModify = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this._audioTrackList = new Array();
        this._phaseList = new Array();
        this._title = "118712 - Consultation d'un message vocal";
        this._isModifying = false;
        this.isAdding = false;
        this.svre.getRefPhase().subscribe(function (data) {
            _this._phaseList = data.json();
        });
    }
    ;
    CustomAudioMessageForm.prototype.panelHelp = function () {
        this.modal.open();
    };
    CustomAudioMessageForm.prototype.listenMessage = function () {
        this.svas.getAudioFile(this.trackName).subscribe(function (data) { window.open(data.url); });
    };
    CustomAudioMessageForm.prototype.modifyMessage = function () {
        this._isModifying = true;
    };
    CustomAudioMessageForm.prototype.saveMessage = function () {
        var _this = this;
        this._isModifying = false;
        this._modifiedTemplate.templateName = this.messageName;
        if (this._fileName != "") {
            this._modifiedTemplate.templateContent.templateContentTxt = this._fileName;
            this._modifiedTemplate.templateContent.templateContentName = this._fileName.split('.')[0];
        }
        this.svam.updateAudioMessageModel(this._modifiedTemplate, this._modifiedTemplate.idTemplate).subscribe(function (response) {
            if (response.status == 200) {
                _this.onSave.emit();
            }
        }, function (error) {
            alert(error);
        });
    };
    CustomAudioMessageForm.prototype.createMessage = function () {
        var _this = this;
        this._isModifying = false;
        if (this._fileName != "") {
            //Constitution de l'objet à modifier
            this._modifiedTemplate = new writtenMessage_mapper_1.Template();
            this._modifiedTemplate.access = 1;
            this._modifiedTemplate.templateContent = new writtenMessage_mapper_1.TemplateContent();
            this._modifiedTemplate.templateName = this.messageName;
            this._modifiedTemplate.templateTemplate = "";
            this._modifiedTemplate.templateSize = 0;
            this._modifiedTemplate.templateType = new writtenMessage_mapper_1.TemplateType();
            this._modifiedTemplate.templateType.templateTypeId = 0;
            this._modifiedTemplate.templateType.templateTypeDescription = "Par défaut - Configuration par l'IHM";
            this._modifiedTemplate.display = 1;
            this._modifiedTemplate.codePhase = this._phaseList.find(function (el) { return el.libPhase == _this.phaseName; }).codePhase;
            this._modifiedTemplate.format = new writtenMessage_mapper_1.Format();
            this._modifiedTemplate.format.formatDescription = "Messages vocaux";
            this._modifiedTemplate.format.formatId = "vocal";
            this._modifiedTemplate.templateContent.templateContentTxt = this._fileName;
            this._modifiedTemplate.templateContent.templateContentId = this._fileName.split('.')[0];
            this._modifiedTemplate.templateContent.templateContentName = this._fileName.split('.')[0];
            this.svam.createAudioMessageModel(this._modifiedTemplate).subscribe(function (response) {
                if (response.status == 200) {
                    _this.onCreate.emit();
                }
            }, function (error) {
                alert(error);
            });
        }
        else {
            alert("Veuillez uploader un fichier audio pour ce modèle");
        }
    };
    CustomAudioMessageForm.prototype.cancel = function () {
        this._isModifying = false;
        //On réinitialise le nom du fichier audio
        this.trackName = this._modifiedTemplate.templateContent.templateContentTxt;
    };
    CustomAudioMessageForm.prototype.uploadMessage = function (e) {
        var _this = this;
        var formData = new FormData();
        formData.append('file', e.target.files[0]);
        this.svas.isAudioFileA8k(formData).subscribe(function (res) {
            if (res.json() == true) {
                //Upload du fichier
                _this.svas.updateAudioFile(e.target.files[0].name, formData).subscribe(function (response) {
                    if (response.status == 200) {
                        alert("L'upload du fichier a été réalisé avec succés");
                        _this._fileName = e.target.files[0].name;
                        _this.trackName = _this._fileName;
                    }
                }, function (error) { alert("L'upload du fichier a échoué: " + error); }, function () { });
            }
            else {
                alert("Le fichier audio envoyé n'a pas pu être converti. Assurez-vous que celui-ci est bien au format WAV encodé en mono sur 8 bits avec le codec ALAW.");
            }
        });
        /*//Upload du fichier
        this.svas.updateAudioFile(e.target.files[0].name, formData).subscribe(
            response => {
                if(response.status == 200) {

                    alert("L'upload du fichier a été réalisé avec succés")
                    this._fileName = e.target.files[0].name;
                }
            },
            error => {alert("L'upload du fichier a échoué")},
            () => {}
        );*/
    };
    CustomAudioMessageForm.prototype.ngOnChanges = function () {
        var _this = this;
        if (!this.isAdding) {
            this._title = "118712 - Consultation d'un message vocal";
            this._fileName = "";
            this._isModifying = false;
            //Récupération du détail du Template
            if (this.idMessage != null) {
                this.svam.getAudioMessageModel(this.idMessage).subscribe(function (data) {
                    _this._modifiedTemplate = data.json();
                    _this.trackName = _this._modifiedTemplate.templateContent.templateContentTxt;
                });
                if (this._phaseList.find(function (el) { return el.libPhase == _this.phaseName; }) != undefined) {
                    this._audioTrackList = new Array();
                    this.svam.getMessageModelList(this._phaseList.find(function (el) { return el.libPhase == _this.phaseName; }).codePhase).subscribe(function (data) {
                        _this._audioTrackList = data.json();
                        if (_this._audioTrackList.find(function (el) { return el.templateName == _this.messageName; }) != undefined) {
                            _this.svam.getIsAudioFilePresent(_this._audioTrackList.find(function (el) { return el.templateName == _this.messageName; }).templateContent.templateContentTxt).subscribe(function (data) {
                                _this._isPresent = data.json();
                            });
                        }
                    });
                }
            }
        }
        else {
            this._title = "118712 - Création d'un message vocal";
            this._fileName = "";
            this._isModifying = false;
            this.messageName = "";
        }
    };
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], CustomAudioMessageForm.prototype, "modal", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], CustomAudioMessageForm.prototype, "messageName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], CustomAudioMessageForm.prototype, "phaseName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], CustomAudioMessageForm.prototype, "trackName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], CustomAudioMessageForm.prototype, "idMessage", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], CustomAudioMessageForm.prototype, "isAdding", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], CustomAudioMessageForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], CustomAudioMessageForm.prototype, "onModify", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], CustomAudioMessageForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], CustomAudioMessageForm.prototype, "onCreate", void 0);
    CustomAudioMessageForm = __decorate([
        core_1.Component({ selector: 'customAudioMessageForm',
            templateUrl: 'views/audioMessage/customAudioMessageDetails.html',
            styleUrls: ['views/audioMessage/customAudioMessage.css',
                'views/customerRoute/customerRoute.css'],
            providers: [audio_service_1.AudioInfosService, audioMessage_service_1.AudioMessageService],
        }),
        __metadata("design:paramtypes", [audio_service_1.AudioInfosService, audioMessage_service_1.AudioMessageService, ref_service_1.RefService])
    ], CustomAudioMessageForm);
    return CustomAudioMessageForm;
}());
exports.CustomAudioMessageForm = CustomAudioMessageForm;
var RefPhase = /** @class */ (function () {
    function RefPhase() {
    }
    return RefPhase;
}());
//# sourceMappingURL=customAudioMessage.form.js.map