import { Component, Input, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';

import { RefService } from '../../services/ref/ref.service';
import { WrittenMessageService } from '../../services/writtenMessage/WrittenMessage.service';
import {Template, TemplateContent, Format, TemplateType} from '../../mappers/writtenMessage.mapper';
import {AudioInfosService} from '../../services/audio/audio.service';
import {AudioMessageService} from '../../services/audioMessage/audioMessage.service';

@Component({ selector: 'deleteCustomAudioMessageForm',
    templateUrl: 'views/audioMessage/deleteCustomAudioMessage.html',
      styleUrls: ['views/audioMessage/customAudioMessage.css',
      'views/customerRoute/customerRoute.css'],
})
export class DeleteCustomAudioMessageForm { 
    @Input() messageName:string;
    @Input() idMessage:number;
    @Output() onClose:EventEmitter<number> = new EventEmitter<number>();
    @Output() onDelete:EventEmitter<number> = new EventEmitter<number>();

    public deleteMessage() {
        this.onDelete.emit(this.idMessage);
    }

    public closeWindow() {
        this.onClose.emit();
    }
}

@Component({ selector: 'customAudioMessageForm',
      templateUrl: 'views/audioMessage/customAudioMessageDetails.html',
      styleUrls: ['views/audioMessage/customAudioMessage.css',
      'views/customerRoute/customerRoute.css'],
      providers: [AudioInfosService, AudioMessageService],
    
})
export class CustomAudioMessageForm { 

    @ViewChild(ModalComponent) modal:ModalComponent;
    
    @Input() messageName:string;
    @Input() phaseName:string;
    @Input() trackName:string;    
    @Input() idMessage:string;
    @Input() isAdding:boolean;

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onModify: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();

    public Adding:boolean;

    private _audioTrackList: Array<Template> = new Array<Template>(); 
    private _phaseList: Array<RefPhase> = new Array<RefPhase>();;
    private _title:string;
    private _isModifying:boolean;
    private _isPresent:boolean;
    
    private _fileName:string;

    private _modifiedTemplate:Template;

    private panelHelp(){

        this.modal.open();
    }    

    private listenMessage() {

        this.svas.getAudioFile(this.trackName).subscribe(
            data => { window.open(data.url);}
        ); 
    } 

    private modifyMessage() {

        this._isModifying = true;
    }

    private saveMessage() {

        this._isModifying = false;
        
        this._modifiedTemplate.templateName = this.messageName;

        if(this._fileName!="") {
            this._modifiedTemplate.templateContent.templateContentTxt = this._fileName;
            this._modifiedTemplate.templateContent.templateContentName = this._fileName.split('.')[0];
        }

        this.svam.updateAudioMessageModel(this._modifiedTemplate, this._modifiedTemplate.idTemplate).subscribe(
            response => {
                if(response.status == 200) {

                    this.onSave.emit();
                }
            },
            error => {
                alert(error);
            }
        );
    }

    private createMessage() {

        this._isModifying = false;

        if(this._fileName!="") {
            //Constitution de l'objet à modifier
            this._modifiedTemplate = new Template();
            this._modifiedTemplate.access = 1;
            this._modifiedTemplate.templateContent = new TemplateContent();
            this._modifiedTemplate.templateName = this.messageName;
            this._modifiedTemplate.templateTemplate = "";   
            this._modifiedTemplate.templateSize = 0;     
            this._modifiedTemplate.templateType = new TemplateType(); 
            this._modifiedTemplate.templateType.templateTypeId = 0;   
            this._modifiedTemplate.templateType.templateTypeDescription = "Par défaut - Configuration par l'IHM";       
            this._modifiedTemplate.display = 1;
            this._modifiedTemplate.codePhase = this._phaseList.find(el => el.libPhase==this.phaseName).codePhase;
            this._modifiedTemplate.format = new Format();
            this._modifiedTemplate.format.formatDescription = "Messages vocaux";
            this._modifiedTemplate.format.formatId = "vocal";

            this._modifiedTemplate.templateContent.templateContentTxt = this._fileName;
            this._modifiedTemplate.templateContent.templateContentId = this._fileName.split('.')[0];
            this._modifiedTemplate.templateContent.templateContentName = this._fileName.split('.')[0];

            this.svam.createAudioMessageModel(this._modifiedTemplate).subscribe(
                response => {
                    if(response.status == 200) {

                        this.onCreate.emit();
                    }
                },
                error => {
                    alert(error);
                }
            );
        }
        else {
            alert("Veuillez uploader un fichier audio pour ce modèle")
        }
    }

    private cancel() {

        this._isModifying = false;       
        //On réinitialise le nom du fichier audio
        this.trackName = this._modifiedTemplate.templateContent.templateContentTxt;
    }    

    private uploadMessage(e: any) {

        const formData = new FormData();
        formData.append('file', e.target.files[0]);      

         this.svas.isAudioFileA8k(formData).subscribe(res => {

            if(res.json()==true){
                //Upload du fichier
                this.svas.updateAudioFile(e.target.files[0].name, formData).subscribe(
                    response => {
                        if(response.status == 200) {

                            alert("L'upload du fichier a été réalisé avec succés")    
                             this._fileName = e.target.files[0].name;
                             this.trackName = this._fileName;
                        }         
                    },
                    error => {alert("L'upload du fichier a échoué: " + error)},
                    () => {}
                );
            }
            else {
                alert("Le fichier audio envoyé n'a pas pu être converti. Assurez-vous que celui-ci est bien au format WAV encodé en mono sur 8 bits avec le codec ALAW.");
            }
        });

        /*//Upload du fichier
        this.svas.updateAudioFile(e.target.files[0].name, formData).subscribe(
            response => {
                if(response.status == 200) {

                    alert("L'upload du fichier a été réalisé avec succés")    
                    this._fileName = e.target.files[0].name;
                }         
            },
            error => {alert("L'upload du fichier a échoué")},
            () => {}
        );*/
    }

    ngOnChanges() {      

        if(!this.isAdding) {

            this._title = "118712 - Consultation d'un message vocal";
            this._fileName = "";
            this._isModifying = false;

            //Récupération du détail du Template
            if(this.idMessage!=null) {
                this.svam.getAudioMessageModel(this.idMessage).subscribe(data => {
                    this._modifiedTemplate = data.json();
                    this.trackName = this._modifiedTemplate.templateContent.templateContentTxt;
                });
                if(this._phaseList.find(el => el.libPhase==this.phaseName)!=undefined) {
                    
                    this._audioTrackList = new Array<Template>();
                    this.svam.getMessageModelList(this._phaseList.find(el => el.libPhase==this.phaseName).codePhase).subscribe(data => {

                            this._audioTrackList = data.json();    
                            if(this._audioTrackList.find(el => el.templateName==this.messageName)!=undefined) {                    
                                this.svam.getIsAudioFilePresent(this._audioTrackList.find(el => el.templateName==this.messageName).templateContent.templateContentTxt).subscribe(data=>{
                                    
                                    this._isPresent = data.json();
                                });
                            }
                    });   
                }
            }
        }
        else {
                   
            this._title = "118712 - Création d'un message vocal";
            this._fileName = "";
            this._isModifying = false;
            this.messageName = ""
        }
    }


    //Constructor
    constructor(private svas: AudioInfosService, private svam: AudioMessageService, private svre: RefService) {

        this._title = "118712 - Consultation d'un message vocal";
        this._isModifying = false;
        this.isAdding = false;

        this.svre.getRefPhase().subscribe(data => {
            this._phaseList = data.json();           
        });
    }
}

class RefPhase {
    codePhase: string;
    libPhase: string;
    maxTrack: number;
}

