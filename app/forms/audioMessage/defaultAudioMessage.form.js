"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var defaultAudioMessage_component_1 = require("../../components/audioMessage/defaultAudioMessage.component");
var ref_service_1 = require("../../services/ref/ref.service");
var audioMessage_service_1 = require("../../services/audioMessage/audioMessage.service");
var audio_service_1 = require("../../services/audio/audio.service");
var DefaultAudioMessageForm = /** @class */ (function () {
    //Constructor
    function DefaultAudioMessageForm(svas, svam, svre, elRef) {
        var _this = this;
        this.svas = svas;
        this.svam = svam;
        this.svre = svre;
        this.elRef = elRef;
        this.onSave = new core_1.EventEmitter();
        this._phaseList = new Array();
        this._filename = "";
        this._title = "118712 - Consultation d'un message vocal";
        this._isModifying = false;
        this.svre.getRefDefAudioTrackPhases().subscribe(function (data) {
            _this._phaseList = data.json();
        });
        this.svam.getRefDefAudioTrackList().subscribe(function (data) {
            _this._audioTrackList = data.json();
            _this._audioTrackList.forEach(function (el) {
                if (el.filename != null) {
                    _this.svam.getIsAudioFilePresent(el.filename).subscribe(function (data) {
                        _this._isPresent = data.json();
                    });
                }
            });
        });
    }
    DefaultAudioMessageForm.prototype.listenMessage = function () {
        this.svas.getDefAudioFile(this._filename).subscribe(function (data) { window.open(data.url); });
    };
    DefaultAudioMessageForm.prototype.uploadMessage = function (e) {
        var _this = this;
        this._formData = new FormData();
        this._formData.append('file', e.target.files[0]);
        this.svas.isAudioFileA8k(this._formData).subscribe(function (res) {
            if (res.json() == true) {
                //Upload du fichier
                _this.svas.updateDefaultAudioFile(e.target.files[0].name, _this._formData).subscribe(function (response) {
                    if (response.status == 200) {
                        alert("L'upload du fichier a été réalisé avec succés");
                        _this._filename = e.target.files[0].name;
                    }
                }, function (error) {
                    alert("L'upload du fichier a échoué: " + error);
                    _this._filename = "";
                }, function () { });
            }
            else {
                alert("Le fichier audio envoyé n'a pas pu être converti. Assurez-vous que celui-ci est bien au format WAV encodé en mono sur 8 bits avec le codec ALAW.");
                _this._formData = null;
            }
        });
    };
    DefaultAudioMessageForm.prototype.setSelected = function (value) {
        this.phaseName = value;
    };
    DefaultAudioMessageForm.prototype.saveMessage = function () {
        var _this = this;
        this._isModifying = false;
        /*this._modifiedRefTrack = new RefDefAudioTrack();*/
        this._modifiedRefTrack.filename = this.idMessage;
        /*this._modifiedRefTrack.serviceName = "118712";*/
        this._modifiedRefTrack.libelle = this.messageName;
        this._modifiedRefTrack.phaseLib = this.phaseName;
        this.svre.updateRefDefaultAudioTrack(this._modifiedRefTrack, this.idMessage).subscribe(function (response) {
            if (response.status == 200) {
                if (_this._formData != null) {
                    //Upload du fichier associé du fichier
                    _this.svas.updateDefaultAudioFile(_this.idMessage, _this._formData).subscribe(function (response) {
                        if (response.status == 200) {
                        }
                    }, function (error) {
                        alert("L'enregistrement du fichier audio a échoué: " + error);
                    }, function () { });
                }
                _this.onSave.emit();
            }
        }, function (error) {
            alert("L'enregistrement des informations a échoué: " + error);
        });
    };
    DefaultAudioMessageForm.prototype.modifyMessage = function () {
        this._isModifying = true;
    };
    DefaultAudioMessageForm.prototype.cancel = function () {
        this._isModifying = false;
        this._filename = this._modifiedRefTrack.filename;
        this._formData = null;
    };
    DefaultAudioMessageForm.prototype.ngOnChanges = function () {
        var _this = this;
        this._isModifying = false;
        if (this.idMessage != undefined && this.idMessage != "") {
            this.svam.getRefDefAudioTrackList().subscribe(function (data) {
                _this._audioTrackList = data.json();
                _this._modifiedRefTrack = new defaultAudioMessage_component_1.RefDefAudioTrack();
                _this._modifiedRefTrack = _this._audioTrackList.find(function (el) { return el.filename == _this.idMessage; });
                _this.phaseName = _this._modifiedRefTrack.phaseLib;
                _this._filename = _this._modifiedRefTrack.filename;
                _this._formData = null;
            });
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DefaultAudioMessageForm.prototype, "messageName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DefaultAudioMessageForm.prototype, "phaseName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DefaultAudioMessageForm.prototype, "idMessage", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DefaultAudioMessageForm.prototype, "onSave", void 0);
    DefaultAudioMessageForm = __decorate([
        core_1.Component({ selector: 'defaultAudioMessageForm',
            templateUrl: 'views/audioMessage/defaultAudioMessageDetails.html',
            styleUrls: ['views/audioMessage/defaultAudioMessage.css',
                'views/customerRoute/customerRoute.css'],
            providers: [ref_service_1.RefService, audioMessage_service_1.AudioMessageService],
        }),
        __metadata("design:paramtypes", [audio_service_1.AudioInfosService, audioMessage_service_1.AudioMessageService, ref_service_1.RefService, core_1.ElementRef])
    ], DefaultAudioMessageForm);
    return DefaultAudioMessageForm;
}());
exports.DefaultAudioMessageForm = DefaultAudioMessageForm;
var RefDefAudioTrackPhase = /** @class */ (function () {
    function RefDefAudioTrackPhase() {
    }
    return RefDefAudioTrackPhase;
}());
//# sourceMappingURL=defaultAudioMessage.form.js.map