import { Component, Input, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';

import { RefDefAudioTrack } from '../../components/audioMessage/defaultAudioMessage.component';

import { RefService } from '../../services/ref/ref.service';
import {AudioMessageService} from '../../services/audioMessage/audioMessage.service';
import {AudioInfosService} from '../../services/audio/audio.service';

@Component({ selector: 'defaultAudioMessageForm',
      templateUrl: 'views/audioMessage/defaultAudioMessageDetails.html',
      styleUrls: ['views/audioMessage/defaultAudioMessage.css',
      'views/customerRoute/customerRoute.css'],
      providers: [RefService, AudioMessageService],
})
export class DefaultAudioMessageForm { 

    @Input() messageName:string;
    @Input() phaseName:string;
    @Input() idMessage:string;

    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    
    private _phaseList: Array<RefDefAudioTrackPhase> = new Array<RefDefAudioTrackPhase>();
    private _title:string;
    private _isModifying:boolean;
    private _isPresent:boolean;
    private _audioTrackList: Array<RefDefAudioTrack>;
    private _modifiedRefTrack: RefDefAudioTrack;
    private _filename:string = ""
    private _formData:FormData;

    private listenMessage() {

        this.svas.getDefAudioFile(this._filename).subscribe(
            data => { window.open(data.url);}
        ); 
    }       

    private uploadMessage(e: any) {

        this._formData = new FormData();
        this._formData.append('file', e.target.files[0]);        

        this.svas.isAudioFileA8k(this._formData).subscribe(res => {

            if(res.json()==true){
                //Upload du fichier
                this.svas.updateDefaultAudioFile(e.target.files[0].name, this._formData).subscribe(
                    response => {
                        if(response.status == 200) {

                            alert("L'upload du fichier a été réalisé avec succés")    
                            this._filename = e.target.files[0].name;
                        }         
                    },
                    error => {
                        alert("L'upload du fichier a échoué: " + error)
                        this._filename = "";
                    },
                    () => {}
                );
            }
            else {
                alert("Le fichier audio envoyé n'a pas pu être converti. Assurez-vous que celui-ci est bien au format WAV encodé en mono sur 8 bits avec le codec ALAW.");
                this._formData = null;
            }
        });        
    } 

    public setSelected(value:string) {      

        this.phaseName = value;
    }
    
    private saveMessage() {

        this._isModifying = false;
        
        /*this._modifiedRefTrack = new RefDefAudioTrack();*/
        this._modifiedRefTrack.filename = this.idMessage;
        /*this._modifiedRefTrack.serviceName = "118712";*/
        this._modifiedRefTrack.libelle = this.messageName;        
        this._modifiedRefTrack.phaseLib = this.phaseName;
        
        this.svre.updateRefDefaultAudioTrack(this._modifiedRefTrack, this.idMessage).subscribe(
            response => {
                if(response.status == 200) {

                    if(this._formData!=null) {
                        //Upload du fichier associé du fichier
                        this.svas.updateDefaultAudioFile(this.idMessage, this._formData).subscribe(
                            response => {
                                if(response.status == 200) {
  
                                }         
                            },
                            error => {
                                alert("L'enregistrement du fichier audio a échoué: " + error)
                            },
                            () => {}
                        );
                    }

                    this.onSave.emit();
                }
            },
            error => {
                alert("L'enregistrement des informations a échoué: " + error)
            }
        );
    }


    private modifyMessage() {

        this._isModifying = true;
    }

    private cancel() {

        this._isModifying = false;
        this._filename = this._modifiedRefTrack.filename;
        this._formData = null;
    } 

    ngOnChanges() {
        
        this._isModifying = false;

        if(this.idMessage!=undefined&&this.idMessage!=""){            
            this.svam.getRefDefAudioTrackList().subscribe(data => {
                this._audioTrackList = data.json();
                this._modifiedRefTrack = new RefDefAudioTrack();
                this._modifiedRefTrack = this._audioTrackList.find(el=>el.filename==this.idMessage);
                this.phaseName = this._modifiedRefTrack.phaseLib;
                this._filename = this._modifiedRefTrack.filename;
                this._formData = null;
            });
        }
    }


    //Constructor
    constructor(private svas: AudioInfosService, private svam: AudioMessageService, private svre: RefService, private elRef:ElementRef) {
        this._title = "118712 - Consultation d'un message vocal";
        this._isModifying = false;

        this.svre.getRefDefAudioTrackPhases().subscribe(data => {
            this._phaseList = data.json();   
        });

        this.svam.getRefDefAudioTrackList().subscribe(data => {
            this._audioTrackList = data.json();
            this._audioTrackList.forEach(el=>{
                    if(el.filename!=null) {
                        
                        this.svam.getIsAudioFilePresent(el.filename).subscribe(data=>{
                            this._isPresent = data.json();
                        });
                    }
            });
        });
    }
}

class RefDefAudioTrackPhase {
    libelle: string;
}

