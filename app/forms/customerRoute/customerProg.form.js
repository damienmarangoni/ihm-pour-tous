"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var mediaSimulator_service_1 = require("../../services/mediaSimulators/mediaSimulator.service");
var customerRoute_service_1 = require("../../services/customerRoute/customerRoute.service");
var customerRouteProg_mapper_1 = require("../../mappers/customerRouteProg.mapper");
var consultRouteProgForm = /** @class */ (function () {
    function consultRouteProgForm(svsd, svcr) {
        var _this = this;
        this.svsd = svsd;
        this.svcr = svcr;
        this.onCancel = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this.onModify = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onClose = new core_1.EventEmitter();
        this._isDailyChecked = true;
        this._isWeeklyChecked = false;
        this._isMonthlyChecked = false;
        this._isMondayChecked = false;
        this._isTuesdayChecked = false;
        this._isWedenesdayChecked = false;
        this._isThursdayChecked = false;
        this._isFridayChecked = false;
        this._isSaturdaydayChecked = false;
        this._isSundayChecked = false;
        this.dateListFlags = [1];
        this.isPeriodChecked = false;
        this.period = "jour(s)";
        this.periodLabel = "Tous les :";
        this.duration = "";
        this.hourList = ["00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30",
            "07:00", "07:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30",
            "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "21:30", "22:00", "22:30", "23:00", "23:30"];
        this.durationList = ["30mn", "1h", "2h", "3h", "4h", "5h", "6h", "7h", "8h", "9h", "10h", "11h", "0.5j", "18h", "1j", "2j", "3j", "4j", "1sem", "2sem", "23h30mn"];
        this.periodHourStart = "";
        this.periodHourEnd = "";
        this.interval = "";
        this.exception = "";
        this.trackList = ["1", "2", "3", "4", "5"];
        this._progMapper = new customerRouteProg_mapper_1.CustomerRouteProgMapper();
        // Modification des styles en fonction des sélections
        this.progDateStyles = { 'height': '13em' };
        this.progDateStyles1 = { 'height': '13em' };
        this.progDateStyles2 = { 'height': '22em' };
        this.progDateStyles3 = { 'height': '31em' };
        this.progDateStyles4 = { 'height': '22em' };
        this.progDateStyles5 = { 'height': '24.4em' };
        this.progDateStyles6 = { 'height': '24.8em' };
        this.divDetailsStyle = { 'height': '44em' };
        this.divDetailsStyle1 = { 'height': '46em' };
        this.divDetailsStyle2 = { 'height': '44em' };
        this.divDetailsStyle3 = { 'height': '49em' };
        this.divDetailsStyle4 = { 'height': '60em' };
        this.divDetailsStyle5 = { 'height': '56.5em' };
        this.divDetailsStyle6 = { 'height': '52em' };
        this.myButonStyle = { 'top': '38em' };
        this.myButonStyle1 = { 'top': '41em' };
        this.myButonStyle2 = { 'top': '38em' };
        this.myButonStyle3 = { 'top': '44em' };
        this.myButonStyle4 = { 'top': '55em' };
        this.myButonStyle5 = { 'top': '50.5em' };
        this.myButonStyle6 = { 'top': '47em' };
        this.helpStyle = { 'top': '42em' };
        this.helpStyle1 = { 'top': '44em' };
        this.helpStyle2 = { 'top': '42em' };
        this.helpStyle3 = { 'top': '47em' };
        this.helpStyle4 = { 'top': '58em' };
        this.helpStyle5 = { 'top': '54.5em' };
        this.helpStyle6 = { 'top': '50em' };
        //Initialisation des attributs des datepickers
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '12em'
        };
        this.periodDateStart = "";
        this.periodDateEnd = "";
        this.dateStart = [];
        this.dateEnd = [];
        this.myDateStart = [];
        this.myDateEnd = [];
        this._title = "118712 - Consultation Programmation";
        this._routeNameLabel = "Nom du parcours :";
        this._phaseNameLabel = "Nom de la phase :";
        this.svcr.getPrioritiesList().subscribe(function (data) {
            _this._priorities = data.json();
        });
    }
    Object.defineProperty(consultRouteProgForm.prototype, "priorities", {
        get: function () {
            return this._priorities;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "templates", {
        get: function () {
            return this._templates;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "routeNameLabel", {
        get: function () {
            return this._routeNameLabel;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "phaseNameLabel", {
        get: function () {
            return this._phaseNameLabel;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "model", {
        get: function () {
            return this._model;
        },
        set: function (value) {
            this._model = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "track", {
        get: function () {
            return this._track;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteProgForm.prototype, "priority", {
        get: function () {
            return this._priority;
        },
        set: function (value) {
            this._priority = value;
        },
        enumerable: true,
        configurable: true
    });
    consultRouteProgForm.prototype.modifyProg = function () {
        this.isModifying = true;
        this._title = "118712 - Modification Programmation";
        this.onModify.emit();
    };
    consultRouteProgForm.prototype.onDateChanged = function (event, which, position) {
        var day;
        var month;
        if (event.date.day.toString().length == 2) {
            day = event.date.day.toString();
        }
        else {
            day = "0" + event.date.day.toString();
        }
        if (event.date.month.toString().length == 2) {
            month = event.date.month.toString();
        }
        else {
            month = "0" + event.date.month.toString();
        }
        if (which == "start") {
            this.dateStart[position] = event.date.year.toString() + "-" + month + "-" + day;
        }
        else if ("end") {
            this.dateEnd[position] = event.date.year.toString() + "-" + month + "-" + day;
        }
    };
    consultRouteProgForm.prototype.deleteProg = function () {
        var _this = this;
        this.svcr.deleteCustomerRouteProg(this._progMapper.programmationDetailsToDisplay.idProgramParent).subscribe(function (response) {
            _this.onClose.emit();
            _this.ngOnChanges();
        }, function (error) {
            alert("ERROR: La suppression de la programmation à echouée");
        }, function () { });
    };
    consultRouteProgForm.prototype.createOrModifyProg = function (action) {
        var _this = this;
        //Binding de l'affichage sur l'objet à mapper FromDisplay
        this._progMapper.programmationDetailsFromDisplay = new customerRouteProg_mapper_1.ProgrammationDetailsFromDisplay();
        this._progMapper.programmationDetailsFromDisplay.dateList = new Array();
        this._progMapper.programmationDetailsFromDisplay.messageName = this.model;
        this._progMapper.programmationDetailsFromDisplay.priority = this.priority;
        this._progMapper.programmationDetailsFromDisplay.templateContentTxt = this.messageContent;
        this._progMapper.programmationDetailsFromDisplay.periodChecked = this.isPeriodChecked;
        if ((!this.isPeriodChecked)) {
            for (var i = 0; i < this.dateListFlags.length; i++) {
                if (this.dateStart[i] != "0-00-00" && this.dateEnd[i] != "0-00-00" &&
                    this.dateStart[i] != undefined && this.dateEnd[i] != undefined) {
                    var programDate = new customerRouteProg_mapper_1.ProgramDate();
                    programDate.dateStart = this.dateStart[i];
                    programDate.dateEnd = this.dateEnd[i];
                    this._progMapper.programmationDetailsFromDisplay.dateList.push(programDate);
                }
                else if (this.dateStart[i] == "0-00-00" || this.dateStart[i] == undefined) {
                    alert("WARNING: Veuillez renseigner la date de début");
                    action = "annuler";
                }
                else if (this.dateEnd[i] == "0-00-00" || this.dateEnd[i] == undefined) {
                    alert("WARNING: Veuillez renseigner la date de fin");
                    action = "annuler";
                }
            }
        }
        else {
            var programPeriod = new customerRouteProg_mapper_1.ProgramPeriod();
            programPeriod.dateStart = this.periodDateStart;
            programPeriod.dateEnd = this.periodDateEnd;
            programPeriod.hourStart = this.periodHourStart; //faire un traitement pour supprimer le H
            programPeriod.hourEnd = this.periodHourEnd; //faire un traitement pour supprimer le H
            programPeriod.duration = this.duration;
            if (this.period == "jour(s)") {
                programPeriod.type = "DAILY";
            }
            else if (this.period == "semaine(s)") {
                programPeriod.type = "WEEKLY";
            }
            else if (this.period == "mois") {
                programPeriod.type = "MONTHLY";
            }
            programPeriod.interval = this.interval;
            programPeriod.mondayChecked = this._isMondayChecked;
            programPeriod.tuesdayChecked = this._isTuesdayChecked;
            programPeriod.wedenesdayChecked = this._isWedenesdayChecked;
            programPeriod.thursdayChecked = this._isThursdayChecked;
            programPeriod.fridayChecked = this._isFridayChecked;
            programPeriod.saturdayChecked = this._isSaturdaydayChecked;
            programPeriod.sundayChecked = this._isSundayChecked;
            programPeriod.exception = this.exception;
            programPeriod.the = this.the;
            this._progMapper.programmationDetailsFromDisplay.periodDetails = programPeriod;
        }
        if (this.model != undefined && this.priority != undefined) {
            //alert("ici")
            this._progMapper.customerRouteProgFromDisplayMapper(this.phaseName, this._templates, this._templateContents, this._priorities);
            if (action == 'create') {
                this.svcr.createCustomerRouteProg(this.idRoute, this._progMapper.programmationeDetailsToSend).subscribe(function (response) {
                    _this.onCreate.emit();
                    _this.isModifying = false;
                    _this.Adding = false;
                }, function (error) {
                    alert("ERROR: La création de la programmation à echouée");
                }, function () { });
            }
            else if (action == 'modify') {
                this.svcr.modifyCustomerRouteProg(this.idRoute, this._progMapper.programmationeDetailsToSend).subscribe(function (response) {
                    _this.onSave.emit();
                    _this.isModifying = false;
                    _this.Adding = false;
                }, function (error) {
                    alert("ERROR: La modification de la programmation à echouée");
                }, function () { });
            }
        }
        else if (this.model == undefined) {
            alert("WARNING: Le modèle du message n'est pas renseigné");
        }
        else if (this.priority == undefined) {
            alert("WARNING: La priorité n'est pas renseigné");
        }
    };
    consultRouteProgForm.prototype.cancel = function () {
        this.isModifying = false;
        this._title = "118712 - Consultation Programmation";
        this.onCancel.emit();
    };
    consultRouteProgForm.prototype.panelHelp = function () {
        this.modal.open();
    };
    //Redimensionnement de l'écran suite à un ajout de dates
    consultRouteProgForm.prototype.dateAdd = function () {
        if (this.dateListFlags.length < 3) {
            if (this.dateListFlags.length == 1) {
                this.dateListFlags.push(2);
                this.progDateStyles = this.progDateStyles2;
                this.divDetailsStyle = this.divDetailsStyle6;
                this.myButonStyle = this.myButonStyle6;
                this.helpStyle = this.helpStyle6;
            }
            else if (this.dateListFlags.length == 2) {
                this.dateListFlags.push(3);
                this.progDateStyles = this.progDateStyles3;
                this.divDetailsStyle = this.divDetailsStyle4;
                this.myButonStyle = this.myButonStyle4;
                this.helpStyle = this.helpStyle4;
            }
        }
        else {
            alert("WARNING: Le nombre de plage est limité à 3");
        }
    };
    //Redimensionnement de l'écran suite à une suppression de dates
    consultRouteProgForm.prototype.dateRemove = function () {
        this.dateListFlags.pop();
        this.dateListResizing();
    };
    //Redimensionnement de l'écran suite à l'ajout/suppression d'une date
    consultRouteProgForm.prototype.dateListResizing = function () {
        if (this.dateListFlags.length == 1) {
            this.progDateStyles = this.progDateStyles1;
            this.divDetailsStyle = this.divDetailsStyle1;
            this.myButonStyle = this.myButonStyle1;
            this.helpStyle = this.helpStyle1;
        }
        else if (this.dateListFlags.length == 2) {
            this.progDateStyles = this.progDateStyles2;
            this.divDetailsStyle = this.divDetailsStyle6;
            this.myButonStyle = this.myButonStyle6;
            this.helpStyle = this.helpStyle6;
        }
        else if (this.dateListFlags.length == 3) {
            this.progDateStyles = this.progDateStyles3;
            this.divDetailsStyle = this.divDetailsStyle4;
            this.myButonStyle = this.myButonStyle4;
            this.helpStyle = this.helpStyle4;
        }
    };
    //Redimensionnement de l'écran suite au choix du type (liste de dates ou périodes)
    consultRouteProgForm.prototype.typeChoiceResizing = function (value) {
        this.isPeriodChecked = value;
        /*this._progMapper.programmationDetailsToDisplay.periodChecked = value;*/
        if (this.phaseName != "Sms") {
            if ((this.isPeriodChecked == true) && (!this.Adding)) {
                if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "DAILY") {
                    this.periodCheckedResizing("Quotidien");
                }
                else if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "WEEKLY") {
                    this.periodCheckedResizing("Hebdomadaire");
                }
                else if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "MONTHLY") {
                    this.periodCheckedResizing("Mensuel");
                }
            }
            else if (this.isPeriodChecked == false) {
                this.dateListResizing();
            }
            else {
                this.periodCheckedResizing("Quotidien");
            }
        }
        else {
            if ((this.isPeriodChecked == true) && (!this.Adding)) {
                if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "DAILY") {
                    this.periodCheckedResizing("Quotidien");
                }
                else if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "WEEKLY") {
                    this.periodCheckedResizing("Hebdomadaire");
                }
                else if (this._progMapper.programmationDetailsToDisplay.periodDetails.type == "MONTHLY") {
                    this.periodCheckedResizing("Mensuel");
                }
            }
            else if (this.isPeriodChecked == false) {
                this.dateListResizing();
            }
            else {
                this.periodCheckedResizing("Quotidien");
            }
        }
    };
    //Redimensionnement de l'écran suite au choix de périodes (quotidien, hebdomadaire, mensuel)
    consultRouteProgForm.prototype.periodCheckedResizing = function (value) {
        if (this.phaseName != "Sms") {
            if (value == "Quotidien") {
                this._isDailyChecked = true;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = false;
                this.period = "jour(s)";
                this.periodLabel = "Tous les :";
                this.progDateStyles = this.progDateStyles4;
                this.divDetailsStyle = this.divDetailsStyle1;
                this.myButonStyle = this.myButonStyle1;
                this.helpStyle = this.helpStyle1;
            }
            else if (value == "Hebdomadaire") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = true;
                this._isMonthlyChecked = false;
                this.period = "semaine(s)";
                this.periodLabel = "Toutes les :";
                this.progDateStyles = this.progDateStyles5;
                this.divDetailsStyle = this.divDetailsStyle3;
                this.myButonStyle = this.myButonStyle3;
                this.helpStyle = this.helpStyle3;
            }
            else if (value == "Mensuel") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = true;
                this.period = "mois";
                this.periodLabel = "Tous les :";
                this.progDateStyles = this.progDateStyles6;
                this.divDetailsStyle = this.divDetailsStyle3;
                this.myButonStyle = this.myButonStyle3;
                this.helpStyle = this.helpStyle3;
            }
        }
        else {
            if (value == "Quotidien") {
                this._isDailyChecked = true;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = false;
                this.period = "jour(s)";
                this.periodLabel = "Tous les :";
                this.progDateStyles = this.progDateStyles4;
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
            else if (value == "Hebdomadaire") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = true;
                this._isMonthlyChecked = false;
                this.period = "semaine(s)";
                this.periodLabel = "Toutes les :";
                this.progDateStyles = this.progDateStyles5;
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
            else if (value == "Mensuel") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = true;
                this.period = "mois";
                this.periodLabel = "Tous les :";
                this.progDateStyles = this.progDateStyles6;
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
        }
    };
    consultRouteProgForm.prototype.phaseNameTraduction = function (phaseName) {
        var traduction;
        switch (phaseName) {
            case "Sms":
                traduction = "ENVOI";
                break;
            case "Promo":
                traduction = "PROMO";
                break;
            case "Attente":
                traduction = "ATT";
                break;
            case "Tarif":
                traduction = "TARIF";
                break;
            case "Accueil":
                traduction = "ACC";
                break;
        }
        return traduction;
    };
    //A chaque consultation d'une Programmation différente
    consultRouteProgForm.prototype.ngOnChanges = function () {
        var _this = this;
        //Reset de l'action modifier pour la consult d'une prog différente   
        this.isModifying = false;
        if (!this.Adding) {
            this._title = "118712 - Consultation Programmation";
            this.svcr.getCustomerRouteProg(parseInt(this.idRoute), this.phaseNameTraduction(this.phaseName)).subscribe(function (data) {
                //Récupération des données du serveur et mapping sur l'objet ToDisplay
                _this._progMapper.programmationDetailsReceived = new customerRouteProg_mapper_1.ProgrammationDetails();
                _this._progMapper.programmationDetailsReceived = data.json();
                if (_this._progMapper.programmationDetailsReceived[0] != undefined) {
                    _this._progMapper.customerRouteProgToDisplayMapper();
                }
                else {
                    _this._progMapper.programmationDetailsToDisplay = new customerRouteProg_mapper_1.ProgrammationDetailsToDisplay();
                }
                //Binding de l'objet mappé ToDisplay sur les éléments du formulaire
                _this._model = _this._progMapper.programmationDetailsToDisplay.messageName;
                _this._priority = _this._progMapper.programmationDetailsToDisplay.priority;
                _this.isPeriodChecked = _this._progMapper.programmationDetailsToDisplay.periodChecked;
                _this.typeChoiceResizing(_this.isPeriodChecked);
                if (_this.phaseName == "Sms") {
                    _this.messageContent = _this._progMapper.programmationDetailsToDisplay.templateContentTxt;
                }
                if ((!_this.isPeriodChecked)) {
                    _this.dateListFlags = [];
                    _this.dateStart = [];
                    _this.myDateStart = [];
                    _this.dateEnd = [];
                    _this.myDateEnd = [];
                    if (_this._progMapper.programmationDetailsToDisplay.dateList != undefined) {
                        _this._progMapper.programmationDetailsToDisplay.dateList.forEach(function (element) {
                            _this.dateStart.push(element.dateStart);
                            _this.myDateStart.push({ date: { year: element.dateStart.slice(0, 4),
                                    month: parseInt(element.dateStart.slice(5, 7)),
                                    day: parseInt(element.dateStart.slice(8)) } });
                            _this.dateEnd.push(element.dateEnd);
                            _this.myDateEnd.push({ date: { year: element.dateEnd.slice(0, 4),
                                    month: parseInt(element.dateEnd.slice(5, 7)),
                                    day: parseInt(element.dateEnd.slice(8)) } });
                            _this.dateListFlags.push(_this.dateStart.length);
                            _this.dateListResizing();
                        });
                    }
                }
                else {
                    _this.dateListFlags = [1];
                    _this.periodDateStart = _this._progMapper.programmationDetailsToDisplay.periodDetails.dateStart;
                    _this.myPeriodStartDate = { date: { year: _this.periodDateStart.slice(0, 4),
                            month: parseInt(_this.periodDateStart.slice(4, 6)),
                            day: parseInt(_this.periodDateStart.slice(6)) } };
                    _this.periodDateEnd = _this._progMapper.programmationDetailsToDisplay.periodDetails.dateEnd;
                    _this.myPeriodEndDate = { date: { year: _this.periodDateEnd.slice(0, 4),
                            month: parseInt(_this.periodDateEnd.slice(4, 6)),
                            day: parseInt(_this.periodDateEnd.slice(6)) } };
                    _this.periodHourStart = _this._progMapper.programmationDetailsToDisplay.periodDetails.hourStart.slice(0, 2) +
                        ":" + _this._progMapper.programmationDetailsToDisplay.periodDetails.hourStart.slice(2, 4);
                    _this.periodHourEnd = _this._progMapper.programmationDetailsToDisplay.periodDetails.hourEnd.slice(0, 2) +
                        ":" + _this._progMapper.programmationDetailsToDisplay.periodDetails.hourEnd.slice(2, 4);
                    _this.duration = _this._progMapper.programmationDetailsToDisplay.periodDetails.duration;
                    /*this._track = this._progMapper.programmationDetailsToDisplay.periodDetails.track;*/
                    _this._track = "1";
                    if (_this._progMapper.programmationDetailsToDisplay.periodDetails.type == "DAILY") {
                        _this.periodCheckedResizing("Quotidien");
                    }
                    else if (_this._progMapper.programmationDetailsToDisplay.periodDetails.type == "WEEKLY") {
                        _this.periodCheckedResizing("Hebdomadaire");
                    }
                    else if (_this._progMapper.programmationDetailsToDisplay.periodDetails.type == "MONTHLY") {
                        _this.periodCheckedResizing("Mensuel");
                    }
                    _this.interval = _this._progMapper.programmationDetailsToDisplay.periodDetails.interval;
                    _this._isMondayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.mondayChecked;
                    _this._isTuesdayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.tuesdayChecked;
                    _this._isWedenesdayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.wedenesdayChecked;
                    _this._isThursdayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.thursdayChecked;
                    _this._isFridayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.fridayChecked;
                    _this._isSaturdaydayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.saturdayChecked;
                    _this._isSundayChecked = _this._progMapper.programmationDetailsToDisplay.periodDetails.sundayChecked;
                    _this.exception = _this._progMapper.programmationDetailsToDisplay.periodDetails.exception;
                    _this.the = _this._progMapper.programmationDetailsToDisplay.periodDetails.the;
                }
            });
        }
        else {
            this._title = "118712 - Création Programmation";
            this.dateListFlags = [1];
            this.dateListResizing();
            this.isPeriodChecked = false;
        }
        this.svcr.getPhaseMessageModelList(this.phaseNameTraduction(this.phaseName)).subscribe(function (data) {
            _this._templates = data.json();
        });
        this.svcr.getTemplateContentsList().subscribe(function (data) {
            _this._templateContents = data.json();
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], consultRouteProgForm.prototype, "routeName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], consultRouteProgForm.prototype, "phaseName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], consultRouteProgForm.prototype, "step", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], consultRouteProgForm.prototype, "idRoute", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], consultRouteProgForm.prototype, "Adding", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteProgForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteProgForm.prototype, "onCreate", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteProgForm.prototype, "onModify", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteProgForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteProgForm.prototype, "onClose", void 0);
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], consultRouteProgForm.prototype, "modal", void 0);
    consultRouteProgForm = __decorate([
        core_1.Component({ selector: 'consultRouteProgForm',
            templateUrl: 'views/customerRoute/customerRouteProgDetails.html',
            styleUrls: ['views/customerRoute/customerRoute.css'],
            providers: [mediaSimulator_service_1.SimulatorDataService, customerRoute_service_1.CustomerRouteService]
        }),
        __metadata("design:paramtypes", [mediaSimulator_service_1.SimulatorDataService, customerRoute_service_1.CustomerRouteService])
    ], consultRouteProgForm);
    return consultRouteProgForm;
}());
exports.consultRouteProgForm = consultRouteProgForm;
//# sourceMappingURL=customerProg.form.js.map