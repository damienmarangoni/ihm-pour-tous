import { Component, Input, Directive, Output, EventEmitter } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import {IMyDpOptions, IMyDateModel} from 'mydatepicker';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';

import {CustomerRouteMapper} from '../../mappers/customerRoute.mapper';
import {CustomerRouteProgMapper, ProgrammationDetailsToDisplay, ProgrammationDetailsFromDisplay, ProgramDate, ProgramPeriod, RefPriority, Template, 
    TemplateContent, ProgrammationDetails} from '../../mappers/customerRouteProg.mapper';

@Component({ selector: 'consultRouteProgForm',
      templateUrl: 'views/customerRoute/customerRouteProgDetails.html',
      styleUrls: ['views/customerRoute/customerRoute.css'],
      providers: [SimulatorDataService, CustomerRouteService]
})
export class consultRouteProgForm { 
    @Input() routeName:string;
    @Input() phaseName:string; 
    @Input() step:number;   
    @Input() idRoute:string;
    @Input() Adding:boolean;

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();
    @Output() onModify: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    @Output() onClose: EventEmitter<number> = new EventEmitter<number>();

    @ViewChild(ModalComponent) modal: ModalComponent;   

    private _title: string;
    private _details: string[];  
    private _model:string;
    private _track:string;
    private _priority:string;
    private _routeNameLabel:string;
    private _phaseNameLabel:string;
    private _priorities:Array<RefPriority>;    
    private _templates:Array<Template>;  
    private _templateContents:Array<TemplateContent>;
    private _isDailyChecked:boolean = true;
    private _isWeeklyChecked:boolean = false;
    private _isMonthlyChecked:boolean = false;
    private _isMondayChecked:boolean = false;
    private _isTuesdayChecked:boolean = false;
    private _isWedenesdayChecked:boolean = false;
    private _isThursdayChecked:boolean = false;
    private _isFridayChecked:boolean = false;
    private _isSaturdaydayChecked:boolean = false;
    private _isSundayChecked:boolean = false;

    public isModifying: boolean;
    public dateListFlags:number[] = [1];
    public isPeriodChecked: boolean = false;
    public period:string = "jour(s)";
    public periodLabel:string = "Tous les :";    
    public duration: string = "";
    public hourList: string[] = ["00:00","00:30","01:00","01:30","02:00","02:30","03:00","03:30","04:00","04:30","05:00","05:30","06:00","06:30",
        "07:00","07:30","09:00","09:30","10:00","10:30","11:00","11:30","12:00","12:30","13:00","13:30","14:00","14:30",
        "15:00","15:30","16:00","16:30","17:00","17:30","18:00","18:30","19:00","19:30","20:00","21:30","22:00","22:30","23:00","23:30"];
    public durationList: string[] = ["30mn","1h","2h","3h","4h","5h","6h","7h","8h","9h","10h","11h","0.5j","18h","1j","2j","3j","4j","1sem","2sem","23h30mn"];
    public periodHourStart: string = "";
    public periodHourEnd: string = "";
    public interval: string = "";
    public exception: string = "";
    public trackList: string[] = ["1","2","3","4","5"];
    public the: string;
    public messageContent: string;

    private _progMapper: CustomerRouteProgMapper = new CustomerRouteProgMapper();

    // Modification des styles en fonction des sélections
    public progDateStyles = {'height': '13em'}
    public progDateStyles1 = {'height': '13em'}    
    public progDateStyles2 = {'height': '22em'}    
    public progDateStyles3 = {'height': '31em'}
    public progDateStyles4 = {'height': '22em'};
    public progDateStyles5 = {'height': '24.4em'};
    public progDateStyles6 = {'height': '24.8em'};

    public divDetailsStyle = {'height': '44em'};
    public divDetailsStyle1 = {'height': '46em'};
    public divDetailsStyle2 = {'height': '44em'};
    public divDetailsStyle3 = {'height': '49em'};
    public divDetailsStyle4 = {'height': '60em'};           
    public divDetailsStyle5 = {'height': '56.5em'};        
    public divDetailsStyle6 = {'height': '52em'};

    public myButonStyle = {'top': '38em'};
    public myButonStyle1 = {'top': '41em'};
    public myButonStyle2 = {'top': '38em'};
    public myButonStyle3 = {'top': '44em'};
    public myButonStyle4 = {'top': '55em'};
    public myButonStyle5 = {'top': '50.5em'};
    public myButonStyle6 = {'top': '47em'};

    public helpStyle = {'top': '42em'};
    public helpStyle1 = {'top': '44em'};
    public helpStyle2 = {'top': '42em'};
    public helpStyle3 = {'top': '47em'};
    public helpStyle4 = {'top': '58em'};
    public helpStyle5 = {'top': '54.5em'};
    public helpStyle6 = {'top': '50em'};

    //Initialisation des attributs des datepickers
    private myDatePickerOptions: IMyDpOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            inline: false,
            width: '12em'
        };
        
    public periodDateStart: string = "";
    public periodDateEnd: string = "";
    private myPeriodStartDate: Object;
    private myPeriodEndDate: Object;
    
    public dateStart: string[] = [];
    public dateEnd: string[] = [];
    private myDateStart: Array<Object> = [];
    private myDateEnd: Array<Object> = [];

    public get priorities() {

        return this._priorities;
    }

    public get templates() {

        return this._templates;
    }

    public get title() {

        return this._title;
    }

    public get routeNameLabel() {

        return this._routeNameLabel;
    }

    public get phaseNameLabel() {

        return this._phaseNameLabel;
    }

    public get model() {

        return this._model;
    }

    public set model(value:string) {

        this._model = value;
    }

    public get track() {

        return this._track;
    }

    public get priority() {

        return this._priority;
    }

    public set priority(value:string) {

        this._priority = value;
    }

    public modifyProg() {

        this.isModifying = true;
        this._title = "118712 - Modification Programmation";        
        this.onModify.emit();
    }

    onDateChanged(event: IMyDateModel, which:string, position: number) {
        let day:string;
        let month:string;
        if(event.date.day.toString().length==2) {
            day = event.date.day.toString();
        }
        else {            
            day = "0" + event.date.day.toString();
        }
        if(event.date.month.toString().length==2) {
            month = event.date.month.toString();
        }
        else {            
            month = "0" + event.date.month.toString();
        }

        if(which=="start") {            
            this.dateStart[position] = event.date.year.toString() + "-" + month + "-" + day;
        }
        else if("end"){            
            this.dateEnd[position] = event.date.year.toString() + "-" + month + "-" + day;
        }
    }

    public deleteProg(){
        this.svcr.deleteCustomerRouteProg(this._progMapper.programmationDetailsToDisplay.idProgramParent).subscribe(
                            response => {                              
                                this.onClose.emit();     
                                this.ngOnChanges();           
                            },
                            error => {
                                alert("ERROR: La suppression de la programmation à echouée");
                            },
                            () =>{}
                );
    }

    public createOrModifyProg(action:string) {

        //Binding de l'affichage sur l'objet à mapper FromDisplay
        this._progMapper.programmationDetailsFromDisplay = new ProgrammationDetailsFromDisplay();
        this._progMapper.programmationDetailsFromDisplay.dateList = new Array<ProgramDate>();
        this._progMapper.programmationDetailsFromDisplay.messageName = this.model;
        this._progMapper.programmationDetailsFromDisplay.priority = this.priority;
        this._progMapper.programmationDetailsFromDisplay.templateContentTxt = this.messageContent;           
        this._progMapper.programmationDetailsFromDisplay.periodChecked = this.isPeriodChecked;   

        if((!this.isPeriodChecked)) {   
            for(let i=0;i<this.dateListFlags.length;i++) {
                
                if(this.dateStart[i]!="0-00-00"&&this.dateEnd[i]!="0-00-00"&&
                   this.dateStart[i]!=undefined&&this.dateEnd[i]!=undefined) {
                    let programDate: ProgramDate = new ProgramDate();
                    programDate.dateStart = this.dateStart[i];
                    programDate.dateEnd = this.dateEnd[i];
                    this._progMapper.programmationDetailsFromDisplay.dateList.push(programDate);
                }
                else if(this.dateStart[i]=="0-00-00"||this.dateStart[i]==undefined) {
                    alert("WARNING: Veuillez renseigner la date de début")
                    action = "annuler";
                }
                else if(this.dateEnd[i]=="0-00-00"||this.dateEnd[i]==undefined) {
                    alert("WARNING: Veuillez renseigner la date de fin")
                    action = "annuler";
                }
            } 
        }
        else {                             
            let programPeriod: ProgramPeriod = new ProgramPeriod();    
            programPeriod.dateStart = this.periodDateStart;  
            programPeriod.dateEnd = this.periodDateEnd;
            programPeriod.hourStart = this.periodHourStart; //faire un traitement pour supprimer le H
            programPeriod.hourEnd = this.periodHourEnd; //faire un traitement pour supprimer le H
            programPeriod.duration = this.duration;
 
            if(this.period=="jour(s)") {
                programPeriod.type = "DAILY";
            }
            else if(this.period=="semaine(s)") {
                programPeriod.type = "WEEKLY";
            }
            else if(this.period=="mois") {
                programPeriod.type = "MONTHLY";
            }

            programPeriod.interval = this.interval;
            programPeriod.mondayChecked = this._isMondayChecked;
            programPeriod.tuesdayChecked = this._isTuesdayChecked;
            programPeriod.wedenesdayChecked = this._isWedenesdayChecked;
            programPeriod.thursdayChecked = this._isThursdayChecked;
            programPeriod.fridayChecked = this._isFridayChecked;
            programPeriod.saturdayChecked = this._isSaturdaydayChecked;
            programPeriod.sundayChecked = this._isSundayChecked;
            programPeriod.exception = this.exception;
            programPeriod.the = this.the;

            this._progMapper.programmationDetailsFromDisplay.periodDetails = programPeriod;  
        }

        if(this.model!=undefined&&this.priority!=undefined) {
            //alert("ici")
            this._progMapper.customerRouteProgFromDisplayMapper(this.phaseName, this._templates, this._templateContents, this._priorities);
            if(action=='create') {
                this.svcr.createCustomerRouteProg(this.idRoute, this._progMapper.programmationeDetailsToSend).subscribe(
                            response => {                              
                                this.onCreate.emit();  
                                this.isModifying = false;   
                                this.Adding = false;        
                            },
                            error => {
                                alert("ERROR: La création de la programmation à echouée");
                            },
                            () =>{}
                );
            }
            else if(action=='modify'){
                 this.svcr.modifyCustomerRouteProg(this.idRoute, this._progMapper.programmationeDetailsToSend).subscribe(
                            response => {
                                this.onSave.emit();      
                                this.isModifying = false;   
                                this.Adding = false;         
                            },
                            error => {
                                alert("ERROR: La modification de la programmation à echouée");
                            },
                            () =>{}
                );
            }
        }
        else if(this.model==undefined) {
            alert("WARNING: Le modèle du message n'est pas renseigné");
        }
        else if(this.priority==undefined) {
            alert("WARNING: La priorité n'est pas renseigné");
        }
    }

    public cancel() {

        this.isModifying = false;     
        this._title = "118712 - Consultation Programmation";        
        this.onCancel.emit();
    }

    public panelHelp() { 

        this.modal.open();
    }

    //Redimensionnement de l'écran suite à un ajout de dates
    public dateAdd() {

        if(this.dateListFlags.length<3) {
            if (this.dateListFlags.length==1) {
                this.dateListFlags.push(2);
                this.progDateStyles = this.progDateStyles2;
                this.divDetailsStyle = this.divDetailsStyle6;
                this.myButonStyle = this.myButonStyle6;
                this.helpStyle = this.helpStyle6;
            }
            else if (this.dateListFlags.length==2) {                
                this.dateListFlags.push(3);
                this.progDateStyles = this.progDateStyles3;
                this.divDetailsStyle = this.divDetailsStyle4;
                this.myButonStyle = this.myButonStyle4;
                this.helpStyle = this.helpStyle4;
            }
        } 
        else {
            alert("WARNING: Le nombre de plage est limité à 3");
        }
    }

    //Redimensionnement de l'écran suite à une suppression de dates
    public dateRemove() {

        this.dateListFlags.pop();
        this.dateListResizing();
    }

    //Redimensionnement de l'écran suite à l'ajout/suppression d'une date
    public dateListResizing() {

         if (this.dateListFlags.length==1) {
            this.progDateStyles = this.progDateStyles1;
            this.divDetailsStyle = this.divDetailsStyle1;
            this.myButonStyle = this.myButonStyle1;
            this.helpStyle = this.helpStyle1;
        }
        else if (this.dateListFlags.length==2) {       
            this.progDateStyles = this.progDateStyles2;
            this.divDetailsStyle = this.divDetailsStyle6;
            this.myButonStyle = this.myButonStyle6;
            this.helpStyle = this.helpStyle6;
        }
        else if (this.dateListFlags.length==3) {    
            this.progDateStyles = this.progDateStyles3;
            this.divDetailsStyle = this.divDetailsStyle4;
            this.myButonStyle = this.myButonStyle4;
            this.helpStyle = this.helpStyle4;
        }
    }
    
    //Redimensionnement de l'écran suite au choix du type (liste de dates ou périodes)
    public typeChoiceResizing(value:boolean) {

            this.isPeriodChecked = value;
            /*this._progMapper.programmationDetailsToDisplay.periodChecked = value;*/
            if (this.phaseName != "Sms") {
                if((this.isPeriodChecked == true)&&(!this.Adding)) {  
                    if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="DAILY") {
                        this.periodCheckedResizing("Quotidien");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="WEEKLY") {
                        this.periodCheckedResizing("Hebdomadaire");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="MONTHLY") {
                        this.periodCheckedResizing("Mensuel");
                    }
                }
                else if(this.isPeriodChecked == false) {                    
                    this.dateListResizing();
                }
                else {
                    this.periodCheckedResizing("Quotidien");
                }
            }
            else {
                if((this.isPeriodChecked == true)&&(!this.Adding)) {    
                    if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="DAILY") {
                        this.periodCheckedResizing("Quotidien");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="WEEKLY") {
                        this.periodCheckedResizing("Hebdomadaire");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="MONTHLY") {
                        this.periodCheckedResizing("Mensuel");
                    }
                }
                else if(this.isPeriodChecked == false) {                     
                    this.dateListResizing();
                }
                else {
                    this.periodCheckedResizing("Quotidien");
                }
            }
    }

    //Redimensionnement de l'écran suite au choix de périodes (quotidien, hebdomadaire, mensuel)
    public periodCheckedResizing(value:string) {
                   
        if (this.phaseName != "Sms") {
            if (value == "Quotidien") {
                this._isDailyChecked = true;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = false;
                this.period = "jour(s)"
                this.periodLabel = "Tous les :"

                this.progDateStyles = this.progDateStyles4;
                this.divDetailsStyle = this.divDetailsStyle1;
                this.myButonStyle = this.myButonStyle1;
                this.helpStyle = this.helpStyle1;
            }
            else if (value == "Hebdomadaire") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = true;
                this._isMonthlyChecked = false;
                this.period = "semaine(s)"
                this.periodLabel = "Toutes les :"

                this.progDateStyles = this.progDateStyles5;
                this.divDetailsStyle = this.divDetailsStyle3;
                this.myButonStyle = this.myButonStyle3;
                this.helpStyle = this.helpStyle3;
            }
            else if (value == "Mensuel") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = true;
                this.period = "mois"
                this.periodLabel = "Tous les :"
                this.progDateStyles = this.progDateStyles6;
                this.divDetailsStyle = this.divDetailsStyle3;
                this.myButonStyle = this.myButonStyle3;
                this.helpStyle = this.helpStyle3;
            }
        }
        else {
            if (value == "Quotidien") {
                this._isDailyChecked = true;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = false;
                this.period = "jour(s)"
                this.periodLabel = "Tous les :"

                this.progDateStyles = this.progDateStyles4;                
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
            else if (value == "Hebdomadaire") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = true;
                this._isMonthlyChecked = false;
                this.period = "semaine(s)"
                this.periodLabel = "Toutes les :"

                this.progDateStyles = this.progDateStyles5;                
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
            else if (value == "Mensuel") {
                this._isDailyChecked = false;
                this._isWeeklyChecked = false;
                this._isMonthlyChecked = true;
                this.period = "mois"
                this.periodLabel = "Tous les :"
                this.progDateStyles = this.progDateStyles6;           
                this.divDetailsStyle = this.divDetailsStyle5;
                this.myButonStyle = this.myButonStyle5;
                this.helpStyle = this.helpStyle5;
            }
        }
    }

    private phaseNameTraduction(phaseName:string):string {

        let traduction:string;
        switch(phaseName) {
            case "Sms":
                traduction = "ENVOI";
                break;
            case "Promo":
                traduction = "PROMO";
                break;
            case "Attente":
                traduction = "ATT";
                break;
            case "Tarif":
                traduction = "TARIF";
                break;
            case "Accueil":
                traduction = "ACC";
                break;
        }
        return traduction;
    }

    //A chaque consultation d'une Programmation différente
    public ngOnChanges() {   

        //Reset de l'action modifier pour la consult d'une prog différente   
        this.isModifying = false; 
        if(!this.Adding) {           
            this._title = "118712 - Consultation Programmation";
            this.svcr.getCustomerRouteProg(parseInt(this.idRoute), this.phaseNameTraduction(this.phaseName)).subscribe(data => {
                //Récupération des données du serveur et mapping sur l'objet ToDisplay
                this._progMapper.programmationDetailsReceived = new ProgrammationDetails();
                this._progMapper.programmationDetailsReceived = data.json(); 
                if(this._progMapper.programmationDetailsReceived[0]!=undefined) {  
                    this._progMapper.customerRouteProgToDisplayMapper();  
                } 
                else {
                    this._progMapper.programmationDetailsToDisplay = new ProgrammationDetailsToDisplay();
                }

                //Binding de l'objet mappé ToDisplay sur les éléments du formulaire
                this._model = this._progMapper.programmationDetailsToDisplay.messageName;
                this._priority = this._progMapper.programmationDetailsToDisplay.priority;
                this.isPeriodChecked = this._progMapper.programmationDetailsToDisplay.periodChecked;
                this.typeChoiceResizing(this.isPeriodChecked);
                if(this.phaseName == "Sms") {
                    this.messageContent = this._progMapper.programmationDetailsToDisplay.templateContentTxt;
                }
            
                if((!this.isPeriodChecked)) {                
                    this.dateListFlags = [];
                    this.dateStart=[];
                    this.myDateStart=[];
                    this.dateEnd=[];
                    this.myDateEnd=[];
                    if(this._progMapper.programmationDetailsToDisplay.dateList!=undefined) {
                        this._progMapper.programmationDetailsToDisplay.dateList.forEach(element => {
                            this.dateStart.push(element.dateStart);
                            this.myDateStart.push( {date: { year: element.dateStart.slice(0,4), 
                                month: parseInt(element.dateStart.slice(5,7)),
                                day: parseInt(element.dateStart.slice(8))}});

                            this.dateEnd.push(element.dateEnd);  
                            this.myDateEnd.push( {date: { year: element.dateEnd.slice(0,4), 
                                month: parseInt(element.dateEnd.slice(5,7)),
                                day: parseInt(element.dateEnd.slice(8))}});

                            this.dateListFlags.push(this.dateStart.length);
                            this.dateListResizing();
                        }); 
                    }
                }
                else {                             
                    this.dateListFlags = [1];
                    this.periodDateStart = this._progMapper.programmationDetailsToDisplay.periodDetails.dateStart;             
                    this.myPeriodStartDate = {date: {year: this.periodDateStart.slice(0,4), 
                        month: parseInt(this.periodDateStart.slice(4,6)),
                        day: parseInt(this.periodDateStart.slice(6))}};

                    this.periodDateEnd = this._progMapper.programmationDetailsToDisplay.periodDetails.dateEnd;           
                    this.myPeriodEndDate = {date: {year: this.periodDateEnd.slice(0,4), 
                        month: parseInt(this.periodDateEnd.slice(4,6)),
                        day: parseInt(this.periodDateEnd.slice(6))}};
                        
                    this.periodHourStart = this._progMapper.programmationDetailsToDisplay.periodDetails.hourStart.slice(0,2) + 
                        ":" + this._progMapper.programmationDetailsToDisplay.periodDetails.hourStart.slice(2,4);
                    this.periodHourEnd = this._progMapper.programmationDetailsToDisplay.periodDetails.hourEnd.slice(0,2) + 
                        ":" + this._progMapper.programmationDetailsToDisplay.periodDetails.hourEnd.slice(2,4);
                    
                    this.duration = this._progMapper.programmationDetailsToDisplay.periodDetails.duration;             
                    /*this._track = this._progMapper.programmationDetailsToDisplay.periodDetails.track;*/
                    this._track = "1";

                    if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="DAILY") {
                        this.periodCheckedResizing("Quotidien");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="WEEKLY") {
                        this.periodCheckedResizing("Hebdomadaire");
                    }
                    else if(this._progMapper.programmationDetailsToDisplay.periodDetails.type=="MONTHLY") {
                        this.periodCheckedResizing("Mensuel");
                    }
                    
                    this.interval = this._progMapper.programmationDetailsToDisplay.periodDetails.interval;
                    this._isMondayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.mondayChecked;
                    this._isTuesdayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.tuesdayChecked;
                    this._isWedenesdayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.wedenesdayChecked;
                    this._isThursdayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.thursdayChecked;
                    this._isFridayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.fridayChecked;
                    this._isSaturdaydayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.saturdayChecked;
                    this._isSundayChecked = this._progMapper.programmationDetailsToDisplay.periodDetails.sundayChecked;
                    this.exception = this._progMapper.programmationDetailsToDisplay.periodDetails.exception;
                    this.the = this._progMapper.programmationDetailsToDisplay.periodDetails.the;
                }
            });
        }
        else {            
            this._title = "118712 - Création Programmation";            
            this.dateListFlags = [1];            
            this.dateListResizing();
            this.isPeriodChecked = false;
        }

        this.svcr.getPhaseMessageModelList(this.phaseNameTraduction(this.phaseName)).subscribe(data => {
            this._templates  = data.json(); 
        });

        this.svcr.getTemplateContentsList().subscribe(data => {
            this._templateContents  = data.json(); 
        });
    }


    constructor(private svsd: SimulatorDataService, private svcr: CustomerRouteService) {  
        this._title = "118712 - Consultation Programmation";
        this._routeNameLabel = "Nom du parcours :";
        this._phaseNameLabel = "Nom de la phase :";

        this.svcr.getPrioritiesList().subscribe(data => {
            this._priorities = data.json();
        });
    }
}