"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var mediaSimulator_service_1 = require("../../services/mediaSimulators/mediaSimulator.service");
var customerRoute_service_1 = require("../../services/customerRoute/customerRoute.service");
var customerRoute_mapper_1 = require("../../mappers/customerRoute.mapper");
var modifyRouteForm = /** @class */ (function () {
    function modifyRouteForm() {
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], modifyRouteForm.prototype, "routeName", void 0);
    modifyRouteForm = __decorate([
        core_1.Component({ selector: 'modifyRouteForm',
            template: '<h1>Modifier parcours {{routeName}} et ses programmations</h1>'
        })
    ], modifyRouteForm);
    return modifyRouteForm;
}());
exports.modifyRouteForm = modifyRouteForm;
var deleteRouteForm = /** @class */ (function () {
    function deleteRouteForm() {
        this.onClose = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    deleteRouteForm.prototype.deleteRoute = function () {
        this.onDelete.emit(this.idRoute);
    };
    deleteRouteForm.prototype.closeWindow = function () {
        this.onClose.emit();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], deleteRouteForm.prototype, "routeName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], deleteRouteForm.prototype, "idRoute", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], deleteRouteForm.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], deleteRouteForm.prototype, "onDelete", void 0);
    deleteRouteForm = __decorate([
        core_1.Component({ selector: 'deleteRouteForm',
            templateUrl: 'views/customerRoute/customerRouteDelete.html',
            styleUrls: ['views/customerRoute/customerRoute.css']
        })
    ], deleteRouteForm);
    return deleteRouteForm;
}());
exports.deleteRouteForm = deleteRouteForm;
var addRouteForm = /** @class */ (function () {
    function addRouteForm() {
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], addRouteForm.prototype, "routeName", void 0);
    addRouteForm = __decorate([
        core_1.Component({ selector: 'addRouteForm',
            template: '<h1>Ajouter {{routeName}}</h1>'
        })
    ], addRouteForm);
    return addRouteForm;
}());
exports.addRouteForm = addRouteForm;
var consultRouteForm = /** @class */ (function () {
    function consultRouteForm(svsd, svcr) {
        var _this = this;
        this.svsd = svsd;
        this.svcr = svcr;
        this.onCancel = new core_1.EventEmitter();
        this.onModify = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this._isModifying = false;
        this._isAdding = false;
        this._createdRouteName = "";
        this._mapper = new customerRoute_mapper_1.CustomerRouteMapper();
        this._title = "118712 - Consultation Parcours client";
        //Init des listes de choix
        svsd.getDUList().subscribe(function (data) {
            _this._duList = data.json();
        });
        svsd.getCommunitiesList().subscribe(function (data) {
            _this._communitiesList = data.json();
        });
        svsd.getSegmentsList().subscribe(function (data) {
            _this._segmentsList = data.json();
        });
    }
    Object.defineProperty(consultRouteForm.prototype, "createdRouteName", {
        get: function () {
            return this._createdRouteName;
        },
        set: function (value) {
            this._createdRouteName = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "isModifying", {
        get: function () {
            return this._isModifying;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "duList", {
        get: function () {
            return this._duList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "communitiesList", {
        get: function () {
            return this._communitiesList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "segmentsList", {
        get: function () {
            return this._segmentsList;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "duDetails", {
        get: function () {
            return this._duDetails;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "communitiesDetails", {
        get: function () {
            return this._communitiesDetails;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(consultRouteForm.prototype, "segmentsDetails", {
        get: function () {
            return this._segmentsDetails;
        },
        enumerable: true,
        configurable: true
    });
    consultRouteForm.prototype.modifyRoute = function () {
        this._isModifying = true;
        this._title = "118712 - Modification Parcours client";
        this.onModify.emit();
    };
    consultRouteForm.prototype.saveRoute = function () {
        var _this = this;
        //Récupération des détails du Parcours client
        this._details = new Array();
        this._details[0] = this._duDetails;
        this._details[1] = this._communitiesDetails;
        this._details[2] = this._segmentsDetails;
        if (((this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == undefined && this._duDetails.length >= 1) ||
            (this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == "DU 0 Tous réseaux" && this._duDetails.length == 1)) &&
            ((this._communitiesDetails.find(function (element) { return element == "Toutes communautés"; }) == undefined && this._communitiesDetails.length >= 1) ||
                (this._communitiesDetails.find(function (element) { return element == "Toutes communautés"; }) == "Toutes communautés" && this._communitiesDetails.length == 1) &&
                    ((this._segmentsDetails.find(function (element) { return element == "Toutes catégories"; }) == undefined && this._segmentsDetails.length >= 1) ||
                        (this._segmentsDetails.find(function (element) { return element == "Toutes catégories"; }) == "Toutes catégories" && this._segmentsDetails.length == 1)))) {
            this._isModifying = false;
            this._title = "118712 - Consultation Parcours client";
            //Mapping de l'affichage sur l'objet à envoyer
            this._mapper.customerRouteDetailsFromDisplay = this._details;
            this._mapper.customerRouteDetailsFromDisplayMapper(this.idRoute, this.routeName, this._segmentsList, this._communitiesList);
            this.svcr.updateCustomerRouteDetails(this._mapper.customerRouteDetailsToSend.qualificationExpressionsDTOs, this.idRoute).subscribe(function (response) {
                console.log("Success Response" + response);
                _this.onSave.emit();
            }, function (error) { console.log("Error happened" + error); }, function () { console.log("the subscription is completed"); });
            //this.onSave.emit();
        }
        else if (this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == "DU 0 Tous réseaux" && this._duDetails.length > 1) {
            alert("WARNING: Vous ne pouvez sélectionner Tous réseaux et un autre indicateur.");
        }
        else if (this._communitiesDetails.find(function (element) { return element == "Toutes catégories"; }) == undefined && this._communitiesDetails.length > 1) {
            alert("WARNING: Vous ne pouvez sélectionner Toutes communautés et une autre communautés.");
        }
    };
    consultRouteForm.prototype.createRoute = function () {
        var _this = this;
        //Récupération des détails du Parcours client
        this._details = new Array();
        this._details[0] = this._duDetails;
        this._details[1] = this._communitiesDetails;
        this._details[2] = this._segmentsDetails;
        if (((this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == undefined && this._duDetails.length >= 1) ||
            (this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == "DU 0 Tous réseaux" && this._duDetails.length == 1)) &&
            ((this._communitiesDetails.find(function (element) { return element == "Toutes communautés"; }) == undefined && this._communitiesDetails.length >= 1) ||
                (this._communitiesDetails.find(function (element) { return element == "Toutes communautés"; }) == "Toutes communautés" && this._communitiesDetails.length == 1) &&
                    ((this._segmentsDetails.find(function (element) { return element == "Toutes catégories"; }) == undefined && this._segmentsDetails.length >= 1) ||
                        (this._segmentsDetails.find(function (element) { return element == "Toutes catégories"; }) == "Toutes catégories" && this._segmentsDetails.length == 1)))) {
            this._isModifying = false;
            this._isAdding = false;
            //Mapping de l'affichage sur l'objet à envoyer
            this._mapper.customerRouteDetailsFromDisplay = this._details;
            this._mapper.customerRouteDetailsFromDisplayMapper(null, this.createdRouteName, this._segmentsList, this._communitiesList);
            if (this.createdRouteName != null && this.createdRouteName != "") {
                this.svcr.createCustomerRouteDetails(this._mapper.customerRouteDetailsToSend).subscribe(function (response) {
                    console.log("Success Response" + response);
                    _this.onCreate.emit();
                }, function (error) { console.log("Error happened" + error); }, function () {
                    console.log("the subscription is completed");
                });
            }
            else {
                alert("WARNING: Veuillez renseigner le nom du parcours client à créer");
            }
        }
        else if (this._duDetails.find(function (element) { return element == "DU 0 Tous réseaux"; }) == "DU 0 Tous réseaux" && this._duDetails.length > 1) {
            alert("WARNING: Vous ne pouvez sélectionner Tous réseaux et un autre indicateur.");
        }
        else if (this._communitiesDetails.find(function (element) { return element == "Toutes catégories"; }) == undefined && this._communitiesDetails.length > 1) {
            alert("WARNING: Vous ne pouvez sélectionner Toutes communautés et une autre communautés.");
        }
    };
    consultRouteForm.prototype.cancel = function () {
        this._isModifying = false;
        this._title = "118712 - Consultation Parcours client";
        this.onCancel.emit();
    };
    consultRouteForm.prototype.setSelected = function (area, e) {
        if (area == "du") {
            this.selectedDu = e;
        }
        else if (area == "communities") {
            this.selectedCommunities = e;
        }
        else if (area == "segments") {
            this.selectedSegments = e;
        }
    };
    consultRouteForm.prototype.appendTextArea = function (area) {
        var details = new Array();
        var list = new Array();
        var label0 = "";
        var selected;
        if (area == "du") {
            details = this._duDetails;
            label0 = "DU 0 Tous réseaux";
            list = this._duList;
            selected = this.selectedDu;
        }
        else if (area == "communities") {
            details = this._communitiesDetails;
            label0 = "Toutes communautés";
            //list = this._communitiesList;
            selected = this.selectedCommunities;
        }
        else if (area == "segments") {
            details = this._segmentsDetails;
            label0 = "Toutes catégories";
            //list = this._segmentsList;
            selected = this.selectedSegments;
        }
        if (selected == undefined) {
            /*selected = list[0];*/
            selected = label0;
        }
        var newList = new Array();
        var isPresent = false;
        details.forEach(function (data) {
            //On copie les données précédentes dans la nouvelle liste
            newList.push(data);
            //On vérifie que la nouvelle donnée à insérer n'est pas déjà dans la liste
            if (data == selected) {
                isPresent = true;
            }
        });
        if (!isPresent) {
            newList.push(selected);
        }
        //Réinitialisation de la liste avec les nouveaux éléments
        if (area == "du") {
            this._duDetails = new Array();
            this._duDetails = newList;
        }
        else if (area == "communities") {
            this._communitiesDetails = new Array();
            this._communitiesDetails = newList;
        }
        else if (area == "segments") {
            this._segmentsDetails = new Array();
            this._segmentsDetails = newList;
        }
    };
    consultRouteForm.prototype.cleartextArea = function (area) {
        if (area == "du") {
            this._duDetails = new Array();
            this._duDetails.push("DU 0 Tous réseaux");
        }
        else if (area == "communities") {
            this._communitiesDetails = new Array();
            this._communitiesDetails.push("Toutes communautés");
        }
        else if (area == "segments") {
            this._segmentsDetails = new Array();
            this._segmentsDetails.push("Toutes catégories");
        }
    };
    consultRouteForm.prototype.removeItem = function (area, item) {
        var newList = new Array();
        if (area == "du") {
            this._duDetails.forEach(function (data) {
                if (data != item) {
                    newList.push(data);
                }
            });
            if (newList.length == 0) {
                newList.push("DU 0 Tous réseaux");
            }
            this._duDetails = newList;
        }
        else if (area == "communities") {
            this._communitiesDetails.forEach(function (data) {
                if (data != item) {
                    newList.push(data);
                }
            });
            if (newList.length == 0) {
                newList.push("Toutes communautés");
            }
            this._communitiesDetails = newList;
        }
        else if (area == "segments") {
            this._segmentsDetails.forEach(function (data) {
                if (data != item) {
                    newList.push(data);
                }
            });
            if (newList.length == 0) {
                newList.push("Toutes catégories");
            }
            this._segmentsDetails = newList;
        }
    };
    consultRouteForm.prototype.panelHelp = function () {
        this.modal.open();
    };
    //A chaque consultation d'un Parcours client différent
    //OU 
    //à chaque création de parcours
    consultRouteForm.prototype.ngOnChanges = function () {
        var _this = this;
        this._duDetails = new Array();
        this._communitiesDetails = new Array();
        this._segmentsDetails = new Array();
        if (this.routeName != null) {
            this._title = "118712 - Consultation Parcours client";
            this._isAdding = false;
            this._isModifying = false;
            //Récupération du détail du Parcours client
            this.svcr.getCustomerRouteDetails(parseInt(this.idRoute)).subscribe(function (data) {
                _this._mapper.customerRouteDetails = data.json();
                _this._communitiesList.forEach(function (element) {
                    var community;
                    community = element;
                    _this._mapper.communitiesDetails.push(community.libCommunity);
                });
                if (_this._mapper.customerRouteDetails != undefined) {
                    _this._mapper.customerRouteDetailsToDisplayMapper();
                }
                _this._details = _this._mapper.customerRouteDetailsToDisplay;
                if (_this._details != null) {
                    _this._duDetails = _this._details[0];
                    _this._communitiesDetails = _this._details[1];
                    _this._segmentsDetails = _this._details[2];
                }
                else {
                    _this._duDetails.push("DU 0 Tous réseaux");
                    _this._communitiesDetails.push("Toutes communautés");
                    _this._segmentsDetails.push("Toutes catégories");
                }
            });
        }
        else {
            this._title = "118712 - Création Parcours client";
            this._isAdding = true;
            this._isModifying = false;
            this._duDetails.push("DU 0 Tous réseaux");
            this._communitiesDetails.push("Toutes communautés");
            this._segmentsDetails.push("Toutes catégories");
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], consultRouteForm.prototype, "routeName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], consultRouteForm.prototype, "idRoute", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], consultRouteForm.prototype, "Adding", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteForm.prototype, "onModify", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], consultRouteForm.prototype, "onCreate", void 0);
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], consultRouteForm.prototype, "modal", void 0);
    consultRouteForm = __decorate([
        core_1.Component({ selector: 'consultRouteForm',
            templateUrl: 'views/customerRoute/customerRouteDetails.html',
            styleUrls: ['views/customerRoute/customerRoute.css'],
            providers: [mediaSimulator_service_1.SimulatorDataService, customerRoute_service_1.CustomerRouteService]
        }),
        __metadata("design:paramtypes", [mediaSimulator_service_1.SimulatorDataService, customerRoute_service_1.CustomerRouteService])
    ], consultRouteForm);
    return consultRouteForm;
}());
exports.consultRouteForm = consultRouteForm;
//# sourceMappingURL=customerRoute.form.js.map