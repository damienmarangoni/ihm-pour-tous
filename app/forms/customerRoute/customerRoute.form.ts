import { Component, Input, Directive, Output, EventEmitter } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';

import {CustomerRouteMapper, RefCategory, RefCommunity} from '../../mappers/customerRoute.mapper';
import {CustomerRouteProgMapper} from '../../mappers/customerRouteProg.mapper';

@Component({ selector: 'modifyRouteForm',
    template: '<h1>Modifier parcours {{routeName}} et ses programmations</h1>'
})
export class modifyRouteForm { 
    @Input() routeName:string;
}

@Component({ selector: 'deleteRouteForm',
    templateUrl: 'views/customerRoute/customerRouteDelete.html',
      styleUrls: ['views/customerRoute/customerRoute.css']
})
export class deleteRouteForm { 
    @Input() routeName:string;
    @Input() idRoute:number;
    @Output() onClose:EventEmitter<number> = new EventEmitter<number>();
    @Output() onDelete:EventEmitter<number> = new EventEmitter<number>();

    public deleteRoute() {
        this.onDelete.emit(this.idRoute);
    }

    public closeWindow() {
        this.onClose.emit();
    }
}

@Component({ selector: 'addRouteForm',
    template: '<h1>Ajouter {{routeName}}</h1>'
})
export class addRouteForm { 
    @Input() routeName:string;
}

@Component({ selector: 'consultRouteForm',
      templateUrl: 'views/customerRoute/customerRouteDetails.html',
      styleUrls: ['views/customerRoute/customerRoute.css'],
      providers: [SimulatorDataService, CustomerRouteService]
})
export class consultRouteForm { 
    @Input() routeName:string;
    @Input() idRoute:string;
    @Input() Adding:boolean;

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onModify: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();

    @ViewChild(ModalComponent) modal: ModalComponent;   

    private _duList: Array<string>;
    private _communitiesList: Array<RefCommunity>;
    private _segmentsList: Array<RefCategory>;
    private _details: Array<string[]>;
    private _duDetails: string[];    
    private _communitiesDetails: string[];
    private _segmentsDetails: string[];
    private _isModifying: boolean = false;    
    private _isAdding: boolean = false;   
    private _title: string;
    private _createdRouteName: string = "";

    private _mapper: CustomerRouteMapper = new CustomerRouteMapper();

    public selectedDu: string;
    public selectedCommunities: string;
    public selectedSegments: string;

    public get createdRouteName() {
        return this._createdRouteName;
    }

    public set createdRouteName(value:string) {
        this._createdRouteName = value;
    }

    public get isModifying() {
        return this._isModifying;
    }

    public get title() {
        return this._title;
    }

    public get duList(): Array<string> {
      return this._duList;
    }

    public get communitiesList(): Array<RefCommunity> {
      return this._communitiesList;
    }

    public get segmentsList(): Array<RefCategory> {
      return this._segmentsList;
    }

    public get duDetails(): Array<string> {
      return this._duDetails;
    }

    public get communitiesDetails(): Array<string> {
      return this._communitiesDetails;
    }

    public get segmentsDetails(): Array<string> {
      return this._segmentsDetails;
    }

    public modifyRoute() {
        this._isModifying = true;
        this._title = "118712 - Modification Parcours client";             
        this.onModify.emit();
    }

    public saveRoute() {
        
        //Récupération des détails du Parcours client
        this._details = new Array<string[]>();
        this._details[0] = this._duDetails;
        this._details[1] = this._communitiesDetails;
        this._details[2] = this._segmentsDetails;
        
        if(((this._duDetails.find(element => element == "DU 0 Tous réseaux")==undefined&&this._duDetails.length>=1) ||
           (this._duDetails.find(element => element == "DU 0 Tous réseaux")=="DU 0 Tous réseaux"&&this._duDetails.length==1)) &&
           ((this._communitiesDetails.find(element => element == "Toutes communautés")==undefined&&this._communitiesDetails.length>=1) ||
           (this._communitiesDetails.find(element => element == "Toutes communautés")=="Toutes communautés"&&this._communitiesDetails.length==1) &&
           ((this._segmentsDetails.find(element => element == "Toutes catégories")==undefined&&this._segmentsDetails.length>=1) ||
           (this._segmentsDetails.find(element => element == "Toutes catégories")=="Toutes catégories"&&this._segmentsDetails.length==1)))) {

            this._isModifying = false;
            this._title = "118712 - Consultation Parcours client";  

            //Mapping de l'affichage sur l'objet à envoyer
            this._mapper.customerRouteDetailsFromDisplay = this._details;        
            this._mapper.customerRouteDetailsFromDisplayMapper(this.idRoute, this.routeName, this._segmentsList, this._communitiesList);  
    
            this.svcr.updateCustomerRouteDetails(this._mapper.customerRouteDetailsToSend.qualificationExpressionsDTOs, this.idRoute).subscribe(
                    response => {
                        console.log("Success Response" + response);
                        this.onSave.emit();
                    },
                    error => {console.log("Error happened" + error)},
                    () =>{console.log("the subscription is completed")});

            //this.onSave.emit();
        }
        else if(this._duDetails.find(element => element == "DU 0 Tous réseaux")=="DU 0 Tous réseaux"&&this._duDetails.length>1) {
            alert("WARNING: Vous ne pouvez sélectionner Tous réseaux et un autre indicateur.")
        }
        else if(this._communitiesDetails.find(element => element == "Toutes catégories")==undefined&&this._communitiesDetails.length>1) {
            alert("WARNING: Vous ne pouvez sélectionner Toutes communautés et une autre communautés.")
        }
    }

    public createRoute() {

        //Récupération des détails du Parcours client
        this._details = new Array<string[]>();
        this._details[0] = this._duDetails;
        this._details[1] = this._communitiesDetails;
        this._details[2] = this._segmentsDetails;

        if(((this._duDetails.find(element => element == "DU 0 Tous réseaux")==undefined&&this._duDetails.length>=1) ||
           (this._duDetails.find(element => element == "DU 0 Tous réseaux")=="DU 0 Tous réseaux"&&this._duDetails.length==1)) &&
           ((this._communitiesDetails.find(element => element == "Toutes communautés")==undefined&&this._communitiesDetails.length>=1) ||
           (this._communitiesDetails.find(element => element == "Toutes communautés")=="Toutes communautés"&&this._communitiesDetails.length==1) &&
           ((this._segmentsDetails.find(element => element == "Toutes catégories")==undefined&&this._segmentsDetails.length>=1) ||
           (this._segmentsDetails.find(element => element == "Toutes catégories")=="Toutes catégories"&&this._segmentsDetails.length==1)))) {

            this._isModifying = false;
            this._isAdding = false

            //Mapping de l'affichage sur l'objet à envoyer
            this._mapper.customerRouteDetailsFromDisplay = this._details;        
            this._mapper.customerRouteDetailsFromDisplayMapper(null, this.createdRouteName, this._segmentsList, this._communitiesList);  
            if(this.createdRouteName!=null&&this.createdRouteName!="") {
                this.svcr.createCustomerRouteDetails(this._mapper.customerRouteDetailsToSend).subscribe(
                        response => {
                            console.log("Success Response" + response)                    
                            this.onCreate.emit();
                        },
                        error => {console.log("Error happened" + error)},
                        () =>{console.log("the subscription is completed")
                });
             }
             else {
                 alert("WARNING: Veuillez renseigner le nom du parcours client à créer")
             }
        }
        else if(this._duDetails.find(element => element == "DU 0 Tous réseaux")=="DU 0 Tous réseaux"&&this._duDetails.length>1) {
            alert("WARNING: Vous ne pouvez sélectionner Tous réseaux et un autre indicateur.")
        }
        else if(this._communitiesDetails.find(element => element == "Toutes catégories")==undefined&&this._communitiesDetails.length>1) {
            alert("WARNING: Vous ne pouvez sélectionner Toutes communautés et une autre communautés.")
        }
    }

    public cancel() {
        this._isModifying = false;
        this._title = "118712 - Consultation Parcours client";        
        this.onCancel.emit();
    }

    public setSelected(area:string, e: string) {
        if (area=="du") {        
            this.selectedDu = e;
        }
        else if (area=="communities") {    
            this.selectedCommunities = e;
        }
        else if (area=="segments") {    
            this.selectedSegments = e;
        }
    }    

    public appendTextArea(area:string) { 
        let details = new Array<string>();
        let list = new Array<string>();
        let label0: string = "";
        let selected:string;

        if (area=="du") {
            details = this._duDetails;
            label0 = "DU 0 Tous réseaux";
            list = this._duList;
            selected = this.selectedDu;
        }
        else if (area=="communities") {
            details = this._communitiesDetails;
            label0 = "Toutes communautés";
            //list = this._communitiesList;
            selected = this.selectedCommunities;
        }
        else if (area=="segments") {
            details = this._segmentsDetails;
            label0 = "Toutes catégories";
            //list = this._segmentsList;
            selected = this.selectedSegments;
        }

        if (selected==undefined) {
            /*selected = list[0];*/
            selected = label0;
        }

        let newList = new Array<string>();
        let isPresent: boolean = false;

        details.forEach(data => {
            //On copie les données précédentes dans la nouvelle liste
            newList.push(data)

            //On vérifie que la nouvelle donnée à insérer n'est pas déjà dans la liste
            if(data == selected) {
                isPresent = true;
            }
        });     

        if (!isPresent) {
            newList.push(selected);
        }  

        //Réinitialisation de la liste avec les nouveaux éléments
        if (area=="du") {
            this._duDetails = new Array<string>();
            this._duDetails = newList;
        }
        else if (area=="communities") {
            this._communitiesDetails = new Array<string>();
            this._communitiesDetails = newList;
        }
        else if (area=="segments") {
            this._segmentsDetails = new Array<string>();
            this._segmentsDetails = newList;
        }
    }

    public cleartextArea(area:string) {
        if (area=="du") {
            this._duDetails = new Array<string>();
            this._duDetails.push("DU 0 Tous réseaux");
        }
        else if (area=="communities") {
            this._communitiesDetails = new Array<string>();
            this._communitiesDetails.push("Toutes communautés");
        }
        else if (area=="segments") {
            this._segmentsDetails = new Array<string>();
            this._segmentsDetails.push("Toutes catégories");
        }
    }

    public removeItem(area:string, item: string) {
        let newList = new Array<string>();
        if (area=="du") {
            this._duDetails.forEach(data => {
                if (data != item) {
                    newList.push(data);
                }
            });
            if(newList.length==0){                
                newList.push("DU 0 Tous réseaux");
            }
            this._duDetails = newList;
        }
        else if (area=="communities") {
            this._communitiesDetails.forEach(data => {
                if (data != item) {
                    newList.push(data);
                }
            });
            if(newList.length==0){                
                newList.push("Toutes communautés");
            }
            this._communitiesDetails = newList;
        }
        else if (area=="segments") {
            this._segmentsDetails.forEach(data => {
                if (data != item) {
                    newList.push(data);
                }
            });
            if(newList.length==0){                
                newList.push("Toutes catégories");
            }
            this._segmentsDetails = newList;
        }
    }

    public panelHelp() {    
        this.modal.open();
    }

    //A chaque consultation d'un Parcours client différent
    //OU 
    //à chaque création de parcours
    public ngOnChanges() {

        this._duDetails = new Array<string>();    
        this._communitiesDetails = new Array<string>(); 
        this._segmentsDetails = new Array<string>();  

        if (this.routeName!=null) {
            this._title = "118712 - Consultation Parcours client";  
            this._isAdding = false;
            this._isModifying = false;
                    
            //Récupération du détail du Parcours client
            this.svcr.getCustomerRouteDetails(parseInt(this.idRoute)).subscribe(data => {       
                this._mapper.customerRouteDetails  = data.json(); 
                this._communitiesList.forEach(element => {
                    let community:any;
                    community = element;
                    this._mapper.communitiesDetails.push(community.libCommunity); 
                });           

                if(this._mapper.customerRouteDetails!=undefined) {    
                    this._mapper.customerRouteDetailsToDisplayMapper();  
                }  

                this._details =  this._mapper.customerRouteDetailsToDisplay;
                if(this._details!=null) {
                    this._duDetails = this._details[0];    
                    this._communitiesDetails = this._details[1];   
                    this._segmentsDetails = this._details[2]; 
                }
                else {
                    this._duDetails.push("DU 0 Tous réseaux");
                    this._communitiesDetails.push("Toutes communautés");
                    this._segmentsDetails.push("Toutes catégories");
                }
            });              
        }
        else {
            this._title = "118712 - Création Parcours client";
            this._isAdding = true;
            this._isModifying = false;   

            this._duDetails.push("DU 0 Tous réseaux");
            this._communitiesDetails.push("Toutes communautés");
            this._segmentsDetails.push("Toutes catégories");
        }
    }
    
    
    constructor(private svsd: SimulatorDataService, private svcr: CustomerRouteService) {
        this._title = "118712 - Consultation Parcours client";  

        //Init des listes de choix
        svsd.getDUList().subscribe(data => {
            this._duList  = data.json()
        });
        svsd.getCommunitiesList().subscribe(data => {
            this._communitiesList = data.json()
        });
        svsd.getSegmentsList().subscribe(data => {
            this._segmentsList = data.json()
        }); 
    }
}

