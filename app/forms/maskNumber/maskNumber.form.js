"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var MaskNumber_service_1 = require("../../services/maskNumber/MaskNumber.service");
var maskNumber_mapper_1 = require("../../mappers/maskNumber.mapper");
var ng2_dragula_1 = require("ng2-dragula");
var MaskNumberForm = /** @class */ (function () {
    function MaskNumberForm(svmn) {
        this.svmn = svmn;
        this._createdNumGIS = "";
        this._mapper = new maskNumber_mapper_1.MaskNumberMapper();
        this.onCancel = new core_1.EventEmitter();
        this.onModify = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this.onTestNum = new core_1.EventEmitter();
        this._isModifying = false;
        this._title = "118712 - Consultation du numéro noir";
    }
    Object.defineProperty(MaskNumberForm.prototype, "createdNumGIS", {
        get: function () {
            return this._createdNumGIS;
        },
        set: function (value) {
            this._createdNumGIS = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskNumberForm.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskNumberForm.prototype, "isUpdating", {
        get: function () {
            return this._isModifying;
        },
        enumerable: true,
        configurable: true
    });
    /************************************************/
    /* Modification d'un numéro masqué                  */
    /************************************************/
    MaskNumberForm.prototype.savemask = function () {
        if (this.descMask != null && this.descMask != " ") {
            this._mapper.maskNumbToSend.maskDescription = this.descMask;
            this._mapper.maskNumbToSend.maskMaskedNb = this.numeroGIS;
            this._mapper.maskNumbToSend.maskMessage = this.SMSMask;
            this._mapper.maskNumbToSend.maskPublicNb = this.numeroPublic;
            // Appel l'evènement qui controle si le numéro GIS est bon puis envoi la création
            this.onSave.emit(this._mapper.maskNumbToSend);
        }
        else {
            alert("le champ description doit obligatoirement être rempli!");
        }
    };
    /************************************************/
    /* Creation d'un numéro masqué                  */
    /************************************************/
    MaskNumberForm.prototype.createMask = function () {
        // Peut etre tester la sytaxe du numero masque
        if (this.numeroGIS != null && this.numeroGIS != " " && this.numeroGIS != "118712") {
            if (this.descMask != null && this.descMask != " ") {
                this._mapper.maskNumbToSend.maskDescription = this.descMask;
                this._mapper.maskNumbToSend.maskMaskedNb = this.numeroGIS;
                this._mapper.maskNumbToSend.maskMessage = this.SMSMask;
                this._mapper.maskNumbToSend.maskPublicNb = this.numeroPublic;
                // Appel l'evènement qui controle si le numéro GIS est bon puis envoi la création
                this.onTestNum.emit(this._mapper.maskNumbToSend);
            }
            else {
                alert("le champ description doit obligatoirement être rempli!");
            }
        }
        else {
            alert("le champ Numero GIS doit obligatoirement être rempli! : " + this.numeroGIS);
        }
    };
    MaskNumberForm.prototype.modifymask = function () {
        this._isModifying = true;
        this._title = "118712 - Modification du numéro noir";
        this.onModify.emit();
    };
    MaskNumberForm.prototype.cancel = function () {
        this._isModifying = false;
        this._title = "118712 - Consultation du numéro noir";
        this.onCancel.emit();
    };
    MaskNumberForm.prototype.ngOnChanges = function () {
        this._isModifying = false;
        this._title = "118712 - Consultation du numéro noir";
    };
    MaskNumberForm.prototype.panelHelp = function () {
        this.modal.open();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MaskNumberForm.prototype, "descMask", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MaskNumberForm.prototype, "numeroGIS", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MaskNumberForm.prototype, "numeroPublic", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], MaskNumberForm.prototype, "SMSMask", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MaskNumberForm.prototype, "isAdding", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], MaskNumberForm.prototype, "isConsulting", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MaskNumberForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MaskNumberForm.prototype, "onModify", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MaskNumberForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MaskNumberForm.prototype, "onCreate", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], MaskNumberForm.prototype, "onTestNum", void 0);
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], MaskNumberForm.prototype, "modal", void 0);
    MaskNumberForm = __decorate([
        core_1.Component({ selector: 'maskNumberForm',
            templateUrl: 'views/maskNumber/maskNumberDetails.html',
            styleUrls: ['views/maskNumber/maskNumberDetails.css', 'node_modules/dragula/dist/dragula.css'],
            providers: [ng2_dragula_1.DragulaService, MaskNumber_service_1.MaskNumberService]
        }),
        __metadata("design:paramtypes", [MaskNumber_service_1.MaskNumberService])
    ], MaskNumberForm);
    return MaskNumberForm;
}());
exports.MaskNumberForm = MaskNumberForm;
/************************************************/
/* Suppression d'un numéro masqué               */
/************************************************/
var DeleteMaskNumberForm = /** @class */ (function () {
    function DeleteMaskNumberForm() {
        this.onCancel = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    DeleteMaskNumberForm.prototype.deleteMaskNumber = function () {
        this.onDelete.emit(this.numeroGIS);
    };
    DeleteMaskNumberForm.prototype.closeWindow = function () {
        this.onCancel.emit();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DeleteMaskNumberForm.prototype, "numeroGIS", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DeleteMaskNumberForm.prototype, "isDeleting", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], DeleteMaskNumberForm.prototype, "isAdding", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteMaskNumberForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteMaskNumberForm.prototype, "onDelete", void 0);
    DeleteMaskNumberForm = __decorate([
        core_1.Component({ selector: 'DeleteMaskNumberForm',
            templateUrl: 'views/maskNumber/maskNumberDelete.html',
            styleUrls: ['views/maskNumber/maskNumberDetails.css']
        })
    ], DeleteMaskNumberForm);
    return DeleteMaskNumberForm;
}());
exports.DeleteMaskNumberForm = DeleteMaskNumberForm;
//# sourceMappingURL=maskNumber.form.js.map