import { Component, Input, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { MaskNumberService } from '../../services/maskNumber/MaskNumber.service';
import {MaskNumberMapper, MaskNumberToSend} from '../../mappers/maskNumber.mapper';


import { DragulaModule, DragulaService } from 'ng2-dragula';

@Component({ selector: 'maskNumberForm',
      templateUrl: 'views/maskNumber/maskNumberDetails.html',
      styleUrls: ['views/maskNumber/maskNumberDetails.css','node_modules/dragula/dist/dragula.css'],
      providers: [DragulaService,  MaskNumberService]
})

export class MaskNumberForm { 
    @Input() descMask:string;
    @Input() numeroGIS:string; 
    @Input() numeroPublic:string;   
    @Input() SMSMask:string;
    @Input() isAdding:boolean;
    @Input() isConsulting:boolean; 

    private _createdNumGIS:string="";
    private _mapper: MaskNumberMapper = new MaskNumberMapper();
    private _isModifying: boolean;
    private _title: string;

    public get createdNumGIS() {
        return this._createdNumGIS;
    }

    public set createdNumGIS(value:string) {
        this._createdNumGIS = value;
    }

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onModify: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<MaskNumberToSend> = new EventEmitter<MaskNumberToSend>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();
    @Output() onTestNum: EventEmitter<MaskNumberToSend> = new EventEmitter<MaskNumberToSend>();

    @ViewChild(ModalComponent) modal: ModalComponent; 

    public get title() {
      return this._title;
    }

    public get isUpdating() {
        return this._isModifying;
    }

     /************************************************/
    /* Modification d'un numéro masqué                  */
    /************************************************/
    public savemask(){
        if (this.descMask != null && this.descMask!=" ")
        {
            this._mapper.maskNumbToSend.maskDescription = this.descMask;
            this._mapper.maskNumbToSend.maskMaskedNb = this.numeroGIS;
            this._mapper.maskNumbToSend.maskMessage = this.SMSMask;
            this._mapper.maskNumbToSend.maskPublicNb = this.numeroPublic;

            // Appel l'evènement qui controle si le numéro GIS est bon puis envoi la création
            this.onSave.emit(this._mapper.maskNumbToSend); 

        }
        else{
            alert ("le champ description doit obligatoirement être rempli!");
        }
    }
    /************************************************/
    /* Creation d'un numéro masqué                  */
    /************************************************/
    public createMask() { 
        // Peut etre tester la sytaxe du numero masque
        if (this.numeroGIS != null && this.numeroGIS!=" " && this.numeroGIS!="118712")
        {
  
            if (this.descMask != null && this.descMask!=" ")
            {
                this._mapper.maskNumbToSend.maskDescription = this.descMask;
                this._mapper.maskNumbToSend.maskMaskedNb = this.numeroGIS;
                this._mapper.maskNumbToSend.maskMessage = this.SMSMask;
                this._mapper.maskNumbToSend.maskPublicNb = this.numeroPublic;

                // Appel l'evènement qui controle si le numéro GIS est bon puis envoi la création
                this.onTestNum.emit(this._mapper.maskNumbToSend); 

            }
            else{
                alert ("le champ description doit obligatoirement être rempli!");
            }
        }
        else
        {
            
            alert ("le champ Numero GIS doit obligatoirement être rempli! : " + this.numeroGIS);
        }
    }

    public modifymask(){
        this._isModifying = true;
        this._title = "118712 - Modification du numéro noir"; 
        this.onModify.emit();
    }
    public cancel(){
        this._isModifying = false;
        this._title ="118712 - Consultation du numéro noir";
        this.onCancel.emit();
    }
    public ngOnChanges(){
        this._isModifying = false;
        this._title ="118712 - Consultation du numéro noir";
    }

    public panelHelp() { 
        this.modal.open();
    }

    public constructor(private svmn: MaskNumberService) {
        this._isModifying = false;
        this._title ="118712 - Consultation du numéro noir";
        
    }

}

/************************************************/
/* Suppression d'un numéro masqué               */
/************************************************/
@Component({ selector: 'DeleteMaskNumberForm',
    templateUrl: 'views/maskNumber/maskNumberDelete.html',
      styleUrls: ['views/maskNumber/maskNumberDetails.css']
})
export class DeleteMaskNumberForm { 
    @Input() numeroGIS:string;
    @Input() isDeleting:boolean;
    @Input() isAdding:boolean;
    
    @Output() onCancel:EventEmitter<number> = new EventEmitter<number>();
    @Output() onDelete:EventEmitter<string> = new EventEmitter<string>();

    public deleteMaskNumber() {
        this.onDelete.emit(this.numeroGIS);       
        
    }

    public closeWindow() {
        this.onCancel.emit();
    }
 
}
