"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var ref_service_1 = require("../../services/ref/ref.service");
var WrittenMessage_service_1 = require("../../services/writtenMessage/WrittenMessage.service");
var writtenMessage_mapper_1 = require("../../mappers/writtenMessage.mapper");
var ng2_dragula_1 = require("ng2-dragula");
var DeleteMessageModelForm = /** @class */ (function () {
    function DeleteMessageModelForm() {
        this.onClose = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    DeleteMessageModelForm.prototype.deleteModel = function () {
        this.onDelete.emit(this.idTemplate);
    };
    DeleteMessageModelForm.prototype.closeWindow = function () {
        this.onClose.emit();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DeleteMessageModelForm.prototype, "templateName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], DeleteMessageModelForm.prototype, "idTemplate", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteMessageModelForm.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteMessageModelForm.prototype, "onDelete", void 0);
    DeleteMessageModelForm = __decorate([
        core_1.Component({ selector: 'deleteMessageModelForm',
            templateUrl: 'views/writtenMessage/writtenMessageModelDelete.html',
            styleUrls: ['views/writtenMessage/writtenMessageModel.css', 'node_modules/dragula/dist/dragula.css',
                'views/customerRoute/customerRoute.css']
        })
    ], DeleteMessageModelForm);
    return DeleteMessageModelForm;
}());
exports.DeleteMessageModelForm = DeleteMessageModelForm;
var WrittenMessageModelForm = /** @class */ (function () {
    //Constructor
    function WrittenMessageModelForm(dragulaService, svwm, svrt, elRef) {
        var _this = this;
        this.dragulaService = dragulaService;
        this.svwm = svwm;
        this.svrt = svrt;
        this.elRef = elRef;
        this.onCancel = new core_1.EventEmitter();
        this.onModify = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this._mapper = new writtenMessage_mapper_1.WrittenMessageMapper();
        this.items = new Array();
        this.essai = [];
        this.withPriorityContentTemp = [];
        this.noPriorityContent = [];
        this.messageText = new Array();
        this.textList = [];
        this._isContentDisplayed = false;
        this.modelContent = "";
        dragulaService.setOptions('bag-one', {
            copy: function (el, source) {
                if (el.id == "carriage-return" && source.className != "container2") {
                    return true;
                }
                else {
                    return false;
                }
            },
            moves: function (el, source, handle, sibling) {
                return true;
            },
            copySortSource: true,
            accepts: function (el, target, source, sibling) {
                if (target.className == "container1 container4" && el.id != "carriage-return") {
                    el.id = 'origin-left';
                }
                else if (target.className == "container2" && el.id != "carriage-return") {
                    el.id = 'origin-right';
                }
                if ((target.className == "container1 container4") && (el.id == "carriage-return")) {
                    return false;
                }
                else if ((target.className == "container1") && (el.id == "carriage-return")) {
                    return false;
                }
                else if ((target.className == "container1 container4") && (el.getElementsByClassName("carriage-return").item(0) != null)) {
                    return false;
                }
                else {
                    return true;
                }
            }
        });
        dragulaService.drop.subscribe(function (value) {
            var bag = value[0];
            var element = value[1];
            var dest = value[2];
            var source = value[3];
            //Cas particulier des Pieds de SMS, une list de choix doit apparaitre 
            if (element.id == "origin-right" && (element.innerText == "Pied de SMS par défaut" || element.innerText == "Pied de SMS(2)" || element.innerText == "Pied de SMS court par défaut")) {
                var content = "";
                for (var i = 0; i < _this.textList.length; i++) {
                    content += '<option  value="' + _this.textList[i] + '">' + _this.textList[i] + '</option>';
                }
                if (element.innerText == "Pied de SMS par défaut") {
                    element.innerHTML += '<select class="caca">' + content + '</select>';
                    //Pour faire en sorte d'avoir un event déclenché
                    _this.elRef.nativeElement.querySelector('.caca').addEventListener('change', function (evt) { return _this.selectTemplate(evt); });
                }
                else if (element.innerText == "Pied de SMS court par défaut") {
                    element.innerHTML += '<select class="coco">' + content + '</select>';
                    //Pour faire en sorte d'avoir un event déclenché
                    _this.elRef.nativeElement.querySelector('.coco').addEventListener('change', function (evt) { return _this.selectTemplate(evt); });
                }
                else if (element.innerText == "Pied de SMS(2)") {
                    element.innerHTML += '<select class="cucu">' + content + '</select>';
                    //Pour faire en sorte d'avoir un event déclenché
                    _this.elRef.nativeElement.querySelector('.cucu').addEventListener('change', function (evt) { return _this.selectTemplate(evt); });
                }
            }
            else if (element.id == "origin-left" && (element.innerHTML.split('<')[0] == "Pied de SMS par défaut" || element.innerHTML.split('<')[0] == "Pied de SMS(2)" || element.innerHTML.split('<')[0] == "Pied de SMS court par défaut")) {
                element.innerHTML = element.innerHTML.split('<')[0];
            }
            if (bag == "bag-one" && source.innerText == "" && element.innerText != "" && element.id != "origin-right") {
                _this.essai = [];
                _this.noPriorityContent = [];
            }
            else {
                if (value[0] == "bag-one" && element.innerText != "" && element.id != "carriage-return") {
                    if (dest != null) {
                        if (dest.innerText != source.innerText) {
                            if (_this.essai.find(function (el) { return el == element.innerHTML.split('<')[0]; }) == undefined && _this.noPriorityContent.find(function (el) { return el == element.innerHTML.split('<')[0]; }) == undefined) {
                                _this.essai.push(element.innerHTML.split('<')[0]);
                            }
                            else {
                                var toto_1 = [];
                                _this.essai.forEach(function (el) {
                                    if (el != element.innerHTML.split('<')[0]) {
                                        toto_1.push(el);
                                    }
                                });
                                _this.essai = toto_1;
                                var tata_1 = [];
                                _this.noPriorityContent.forEach(function (el) {
                                    if (el != element.innerHTML.split('<')[0]) {
                                        tata_1.push(el);
                                    }
                                });
                                _this.noPriorityContent = tata_1;
                            }
                        }
                    }
                }
            }
            _this.updateContent(source.className);
        });
        dragulaService.over.subscribe(function (value) {
            var bag = value[0];
            var element = value[1];
            var dest = value[2];
            var source = value[3];
        });
        //Pour la supression de l'élément carriage-return
        dragulaService.cancel.subscribe(function (value) {
            if (value[1].id == "carriage-return" || value[1].getElementsByClassName("carriage-return").item(0) != null) {
                value[1].innerHTML = "";
                value[1].className = "poubelle";
                document.getElementsByClassName("poubelle").item(0).remove();
                _this.updateContent("container2");
            }
        });
        this._title = "118712 - Consultation du modèle de message écrits";
        this.svrt.getWrittenMessageModelList().subscribe(function (data) {
            _this.items = data.json();
        });
        this.svwm.getWrittenMessageTextList().subscribe(function (data) {
            _this.messageText = data.json();
            _this.messageText.forEach(function (el) {
                _this.textList.push(el.templateContentId);
            });
        });
    }
    Object.defineProperty(WrittenMessageModelForm.prototype, "isModifying", {
        //Getters and setters
        get: function () {
            return this._isModifying;
        },
        set: function (value) {
            this._isModifying = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageModelForm.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageModelForm.prototype, "panelHelpId", {
        get: function () {
            return this._panelHelpId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageModelForm.prototype, "myHelpStyle", {
        get: function () {
            return this._myHelpStyle;
        },
        set: function (style) {
            this._myHelpStyle = style;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageModelForm.prototype, "isContentDisplayed", {
        get: function () {
            return this._isContentDisplayed;
        },
        set: function (status) {
            this._isContentDisplayed = status;
        },
        enumerable: true,
        configurable: true
    });
    //Methods
    WrittenMessageModelForm.prototype.modifyModel = function () {
        this._isModifying = true;
        this._title = "118712 - Modification du modèle de message écrit";
        this.onModify.emit();
    };
    WrittenMessageModelForm.prototype.cancel = function () {
        this._isModifying = false;
        this._title = "118712 - Consultation du modèle de message écrit";
        this.ngOnChanges();
        //Suppression des carriage return résiduels
        for (var i = 0; i < document.getElementsByClassName("container2").item(0).children.length; i++) {
            document.getElementsByClassName("container2").item(0).getElementsByClassName("greenElement").item(i).remove();
        }
        this.onCancel.emit();
    };
    WrittenMessageModelForm.prototype.createModel = function () {
        var _this = this;
        this._mapper.templateDetailsToSend.templateTemplate = this.modelContent.replace(/\\n/g, "\n");
        this._mapper.templateDetailsToSend.templateName = this.templateName;
        this._mapper.templateDetailsToSend.codePhase = "ENVOI";
        this._mapper.templateDetailsToSend.format = new writtenMessage_mapper_1.Format();
        this._mapper.templateDetailsToSend.format.formatDescription = "Short Message Service";
        this._mapper.templateDetailsToSend.format.formatId = "sms";
        this._mapper.templateDetailsToSend.templateType = new writtenMessage_mapper_1.TemplateType();
        this._mapper.templateDetailsToSend.templateType.templateTypeDescription = "Par défaut - Configuration par l'IHM";
        this._mapper.templateDetailsToSend.templateType.templateTypeId = 0;
        this._mapper.templateDetailsToSend.templateContent = new writtenMessage_mapper_1.TemplateContent();
        //Détermination du templateContentId (colonne "Nom du contenu")
        if (this.modelContent.split("template_content(")[1] != undefined &&
            this.modelContent.split("template_content(")[1].split(")")[0] != undefined) {
            this._mapper.templateDetailsToSend.templateContent.templateContentId = this.modelContent.split("template_content(")[1].split(")")[0];
        }
        else {
            this._mapper.templateDetailsToSend.templateContent.templateContentId = "MessageDefaut";
        }
        this.svwm.createWrittenMessageModel(this._mapper.templateDetailsToSend).subscribe(function (response) {
            if (response.status == 200) {
                _this.onCreate.emit();
                _this._isModifying = false;
                _this._title = "118712 - Consultation du modèle de message écrits";
            }
        }, function (error) { alert(error); });
    };
    WrittenMessageModelForm.prototype.saveModel = function () {
        var _this = this;
        this._mapper.templateDetailsReceived.templateTemplate = this.modelContent.replace(/\\n/g, "\n");
        //Détermination du templateContentId (colonne "Nom du contenu")
        if (this.modelContent.split("template_content(")[1] != undefined &&
            this.modelContent.split("template_content(")[1].split(")")[0] != undefined) {
            this._mapper.templateDetailsReceived.templateContent.templateContentId = this.modelContent.split("template_content(")[1].split(")")[0];
        }
        else {
            this._mapper.templateDetailsReceived.templateContent.templateContentId = "MessageDefaut";
        }
        this.svwm.updateWrittenMessageModel(this._mapper.templateDetailsReceived, this.idTemplate).subscribe(function (response) {
            if (response.status == 200) {
                _this.onSave.emit();
                _this._isModifying = false;
                _this._title = "118712 - Consultation du modèle de message écrits";
            }
        }, function (error) { alert(error); });
    };
    WrittenMessageModelForm.prototype.panelHelp = function (id) {
        this._panelHelpId = id;
        if (id == 1) {
            this.myHelpStyle = { 'top': '53em' };
        }
        else if (id == 2) {
            this.myHelpStyle = { 'top': '33em' };
        }
        else if (id == 3) {
            this.myHelpStyle = { 'top': '16em' };
        }
        this.modal.open();
    };
    WrittenMessageModelForm.prototype.contentDisplay = function () {
        this.isContentDisplayed = true;
    };
    WrittenMessageModelForm.prototype.contentHide = function () {
        this.isContentDisplayed = false;
    };
    //Logique de traitement dynamique de l'affichage des éléments
    WrittenMessageModelForm.prototype.updateContent = function (src) {
        var content = "";
        var separator = "";
        this.modelContent = "";
        var j = 0;
        var k = 0;
        var _loop_1 = function (i) {
            if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }) != undefined ||
                this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }) != undefined) {
                if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }) != undefined &&
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML != "cr") {
                    content = "$" + this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }).keyword;
                }
                else if (this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }) != undefined &&
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML != "cr") {
                    content = "$" + this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML; }).keyword;
                }
                else if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML == "cr") {
                    content = "\\n";
                    separator = "";
                }
                //Changement priorité éléments
                if (document.getElementsByClassName("container2").item(0).getElementsByClassName("blueElement").length -
                    document.getElementsByClassName("container2").item(0).getElementsByClassName("cr").length ==
                    (document.getElementsByClassName("container3").item(0).children.length + document.getElementsByClassName("container5").item(0).children.length)) {
                    for (var k_1 = 0; k_1 < document.getElementsByClassName("container3").item(0).children.length; k_1++) {
                        if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML ==
                            document.getElementsByClassName("container3").item(0).children.item(k_1).innerHTML) {
                            //let n:number=k+1;
                            content += "[" + (k_1) + "]";
                        }
                    }
                }
                else {
                    if (this_1.noPriorityContent.length != 0) {
                        for (var k_2 = 0; k_2 < this_1.essai.length; k_2++) {
                            if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML ==
                                this_1.essai[k_2]) {
                                content += "[" + (j) + "]";
                                j++;
                            }
                        }
                    }
                    else {
                        content += "[" + (j) + "]";
                        j++;
                    }
                }
                separator = " ";
            }
            else {
                if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<').length <= 1) {
                    if (content != "\\n") {
                        this_1.modelContent = this_1.modelContent.substring(0, this_1.modelContent.length - 1);
                    }
                    content = "\\n";
                    separator = "";
                }
                else {
                    //Cas des éléments composés
                    if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }) != undefined ||
                        this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }) != undefined) {
                        if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }) != undefined) {
                            if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content") {
                                content = "$" + this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.firstFooterContent + ")";
                            }
                            else if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content_short") {
                                content = "$" + this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.secondFooterContent + ")";
                            }
                            else if (this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content_url") {
                                content = "$" + this_1.items.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.thirdFooterContent + ")";
                            }
                        }
                        else if (this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }) != undefined) {
                            if (this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content") {
                                content = "$" + this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.firstFooterContent + ")";
                            }
                            else if (this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content_short") {
                                content = "$" + this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.secondFooterContent + ")";
                            }
                            else if (this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword == "template_content_url") {
                                content = "$" + this_1.itemsOrder.find(function (item) { return item.description == document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]; }).keyword
                                    + "(" + this_1.thirdFooterContent + ")";
                            }
                        }
                        //Changement priorité éléments
                        if ((document.getElementsByClassName("container2").item(0).getElementsByClassName("blueElement").length -
                            document.getElementsByClassName("container2").item(0).getElementsByClassName("cr").length) ==
                            (document.getElementsByClassName("container3").item(0).children.length + document.getElementsByClassName("container5").item(0).children.length)) {
                            for (var k_3 = 0; k_3 < document.getElementsByClassName("container3").item(0).children.length; k_3++) {
                                if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0] ==
                                    document.getElementsByClassName("container3").item(0).children.item(k_3).innerHTML) {
                                    //let n:number=k+1;
                                    content += "[" + (k_3) + "]";
                                }
                            }
                        }
                        else {
                            if (this_1.noPriorityContent.length != 0) {
                                for (var k_4 = 0; k_4 < this_1.essai.length; k_4++) {
                                    if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0] ==
                                        this_1.essai[k_4]) {
                                        content += "[" + (j) + "]";
                                        j++;
                                    }
                                }
                            }
                            else {
                                content += "[" + (j) + "]";
                                j++;
                            }
                        }
                    }
                    else {
                        /*if(content!="\\n") {
                            this.modelContent = this.modelContent.substring(0,this.modelContent.length-1);
                        }*/
                        content = "\\n";
                        separator = "";
                    }
                }
            }
            this_1.modelContent += content + separator;
        };
        var this_1 = this;
        for (var i = 0; i < document.getElementsByClassName("container2").item(0).children.length; i++) {
            _loop_1(i);
        }
    };
    WrittenMessageModelForm.prototype.selectTemplate = function (e) {
        if (e.target.className == "Pied de SMS par défaut" || e.target.className == "caca") {
            this.firstFooterContent = e.target.value;
        }
        else if (e.target.className == "Pied de SMS court par défaut" || e.target.className == "coco") {
            this.secondFooterContent = e.target.value;
        }
        else if (e.target.className == "Pied de SMS(2)" || e.target.className == "cucu") {
            this.thirdFooterContent = e.target.value;
        }
        this.updateContent("");
    };
    //Appellé à chaque changement de templateId 
    WrittenMessageModelForm.prototype.ngOnChanges = function () {
        var _this = this;
        this.items = new Array();
        this.itemsOrder = new Array();
        //Init de la valeur de l'input pour les éléments composés
        this.firstFooterContent = "MessageDefaut";
        this.secondFooterContent = "MessageDefaut";
        this.thirdFooterContent = "MessageDefaut";
        this.essai = [];
        this.withPriorityContentTemp = [];
        this.noPriorityContent = [];
        //Reset de l'action modifier pour la consult d'une prog différente   
        this.isModifying = false;
        if (!this.Adding) {
            if (this.idTemplate != undefined) {
                //Récupération de la liste totale des items
                this.svrt.getWrittenMessageModelList().subscribe(function (data) {
                    _this.items = data.json();
                    //Récupération du détail du Template
                    _this.svwm.getWrittenMessageModel(_this.idTemplate).subscribe(function (data) {
                        _this._mapper.templateDetailsReceived = new writtenMessage_mapper_1.Template();
                        _this._mapper.templateDetailsReceived = data.json();
                        //Mapping sur l'objet contenant les données d'affichage
                        _this._mapper.templateToDisplayMapper();
                        //Traitement pour l'affichage (Binding)     
                        _this.modelContent = _this._mapper.templateDetailsReceived.templateTemplate.replace(/\n/g, "\\n");
                        //Priorité des éléments
                        _this._mapper.templateDetailsToDisplay.elementWithoutPriority.forEach(function (el) {
                            if (el.substring(el.length - 1, el.length) == ' ') {
                                el = el.substring(0, el.length - 1);
                            }
                            if (_this.items.find(function (item) { return item.keyword == el; }) != undefined) {
                                _this.noPriorityContent.push(_this.items.find(function (item) { return item.keyword == el; }).description);
                            }
                        });
                        _this._mapper.templateDetailsToDisplay.elementWithPriority.forEach(function (el) {
                            if (_this.items.find(function (item) { return item.keyword == el[0]; }) != undefined) {
                                _this.essai.push(_this.items.find(function (item) { return item.keyword == el[0]; }).description);
                            }
                        });
                        //Ordre d'affichage
                        _this.itemsOrder = new Array();
                        _this._mapper.templateDetailsToDisplay.elementOrder.forEach(function (el) {
                            if (el.substring(el.length - 1, el.length) == ' ') {
                                el = el.substring(0, el.length - 1);
                            }
                            if (_this.items.find(function (item) { return item.keyword == el; }) != undefined) {
                                _this.itemsOrder.push(_this.items.find(function (item) { return item.keyword == el; }));
                                if (_this.items.find(function (item) { return item.keyword == el; }).keyword == "template_content_short") {
                                    _this.secondFooterContent = _this.modelContent.split('template_content_short')[1].split('(')[1].split(')')[0];
                                }
                                else if (_this.items.find(function (item) { return item.keyword == el; }).keyword == "template_content_url") {
                                    _this.thirdFooterContent = _this.modelContent.split('template_content_url')[1].split('(')[1].split(')')[0];
                                }
                                else if (_this.items.find(function (item) { return item.keyword == el; }).keyword == "template_content") {
                                    _this.firstFooterContent = _this.modelContent.split('template_content')[1].split('(')[1].split(')')[0];
                                }
                            }
                            else if (el == '\n') {
                                var refCr = new writtenMessage_mapper_1.RefTemplateItem();
                                refCr.description = "cr";
                                _this.itemsOrder.push(refCr);
                            }
                        });
                        //Eléments disponibles
                        _this.itemsTemp = new Array();
                        _this.items.forEach(function (el) {
                            var value = _this.itemsOrder.find(function (item) { return item.description == el.description; });
                            if (value == undefined) {
                                _this.itemsTemp.push(el);
                            }
                        });
                        _this.items = _this.itemsTemp;
                    });
                });
            }
        }
        else {
            this._isModifying = false;
            this.templateName = "";
            this.modelContent = "";
            this._title = "118712 - Création du modèle de message écrits";
            this.svrt.getWrittenMessageModelList().subscribe(function (data) {
                _this.items = data.json();
            });
        }
    };
    //Appellé quand les éléments sont affichés
    WrittenMessageModelForm.prototype.ngAfterViewChecked = function () {
        if (document.getElementsByClassName("container2").item(0).children.length > 0) {
            //Les éléments identifiés comme carriage-return sont modifiés pour correspondre au style
            for (var i = 0; i < document.getElementsByClassName("container2").item(0).children.length; i++) {
                if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML == "cr") {
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML = '<span class="carriage-return" style="position:relative;font-size:14px;" >&crarr;</span>';
                }
            }
            //Cas particulier des Pieds de SMS, une list de choix doit apparaitre
            for (var i = 0; i < document.getElementsByClassName("container2").item(0).children.length; i++) {
                if ((document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0, "Pied de SMS par défaut".length) == "Pied de SMS par défaut" ||
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0, "Pied de SMS(2)".length) == "Pied de SMS(2)" ||
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0, "Pied de SMS court par défaut".length) == "Pied de SMS court par défaut")) {
                    //NONE
                }
                else {
                    if (document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('span').length <= 1) {
                        var content = document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0];
                        document.getElementsByClassName("container2").item(0).children.item(i).innerHTML = content;
                    }
                }
            }
        }
    };
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], WrittenMessageModelForm.prototype, "modal", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], WrittenMessageModelForm.prototype, "templateName", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], WrittenMessageModelForm.prototype, "idTemplate", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], WrittenMessageModelForm.prototype, "Adding", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageModelForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageModelForm.prototype, "onModify", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageModelForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageModelForm.prototype, "onCreate", void 0);
    WrittenMessageModelForm = __decorate([
        core_1.Component({ selector: 'writtenMessageModelForm',
            templateUrl: 'views/writtenMessage/writtenMessageModelDetails.html',
            styleUrls: ['views/writtenMessage/writtenMessageModel.css', 'node_modules/dragula/dist/dragula.css',
                'views/customerRoute/customerRoute.css'],
            providers: [ng2_dragula_1.DragulaService, ref_service_1.RefService, WrittenMessage_service_1.WrittenMessageService]
        }),
        __metadata("design:paramtypes", [ng2_dragula_1.DragulaService, WrittenMessage_service_1.WrittenMessageService, ref_service_1.RefService, core_1.ElementRef])
    ], WrittenMessageModelForm);
    return WrittenMessageModelForm;
}());
exports.WrittenMessageModelForm = WrittenMessageModelForm;
//# sourceMappingURL=writtenMessageModel.form.js.map