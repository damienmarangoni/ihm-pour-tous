import { Component, Input, Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import {ViewChild, ViewEncapsulation, ContentChild} from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { CustomerRouteService } from '../../services/customerRoute/customerRoute.service';
import { RefService } from '../../services/ref/ref.service';
import { WrittenMessageService } from '../../services/writtenMessage/WrittenMessage.service';

import {CustomerRouteMapper} from '../../mappers/customerRoute.mapper';
import {CustomerRouteProgMapper} from '../../mappers/customerRouteProg.mapper';
import {WrittenMessageMapper,RefTemplateItem, Template, TemplateContent, Format, TemplateType} from '../../mappers/writtenMessage.mapper';

import { DragulaModule, DragulaService } from 'ng2-dragula';


@Component({ selector: 'deleteMessageModelForm',
    templateUrl: 'views/writtenMessage/writtenMessageModelDelete.html',
      styleUrls: ['views/writtenMessage/writtenMessageModel.css','node_modules/dragula/dist/dragula.css',
      'views/customerRoute/customerRoute.css']
})
export class DeleteMessageModelForm { 
    @Input() templateName:string;
    @Input() idTemplate:number;
    @Output() onClose:EventEmitter<number> = new EventEmitter<number>();
    @Output() onDelete:EventEmitter<number> = new EventEmitter<number>();

    public deleteModel() {
        this.onDelete.emit(this.idTemplate);
    }

    public closeWindow() {
        this.onClose.emit();
    }
}

@Component({ selector: 'writtenMessageModelForm',
      templateUrl: 'views/writtenMessage/writtenMessageModelDetails.html',
      styleUrls: ['views/writtenMessage/writtenMessageModel.css','node_modules/dragula/dist/dragula.css',
      'views/customerRoute/customerRoute.css'],
      providers: [DragulaService, RefService, WrittenMessageService]
})
export class WrittenMessageModelForm { 

    @ViewChild(ModalComponent) modal: ModalComponent;   

    @Input() templateName: string;
    @Input() idTemplate: string;   
    @Input() Adding:boolean;

    public id:string;     

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onModify: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();

    private _mapper: WrittenMessageMapper = new WrittenMessageMapper();

    public items:Array<RefTemplateItem> = new Array<RefTemplateItem>();   
    public itemsTemp:Array<RefTemplateItem>; 
    public itemsOrder:Array<RefTemplateItem>; 
    public essai: string[] = [];  
    public withPriorityContentTemp: string[] = [];     
    public noPriorityContent: string[] = [];
    public messageText: Array<TemplateContent> = new Array<TemplateContent>();
    public textList: string[] = [];

    private _isModifying: boolean;
    private _title: string;
    private _panelHelpId: number;
    private _myHelpStyle: any;
    private _isContentDisplayed: boolean = false;
    
    //Getters and setters
    public get isModifying() {
      return this._isModifying;
    }    

    public set isModifying(value:boolean) {
      this._isModifying = value;
    } 
    
    public get title() {
      return this._title;
    }
    
    public get panelHelpId() {
      return this._panelHelpId;
    }    

    public get myHelpStyle() {
      return this._myHelpStyle;
    }

    public set myHelpStyle(style:any) {
      this._myHelpStyle = style;
    }  

    public get isContentDisplayed() {
      return this._isContentDisplayed;
    }

    public set isContentDisplayed(status:boolean) {
      this._isContentDisplayed = status;
    }

    //Methods
    public modifyModel() {

        this._isModifying = true;
        this._title = "118712 - Modification du modèle de message écrit"; 
        this.onModify.emit();
    }

    public cancel() {

        this._isModifying = false;
        this._title = "118712 - Consultation du modèle de message écrit";    
        this.ngOnChanges();

        //Suppression des carriage return résiduels
        for(let i =0;i<document.getElementsByClassName("container2").item(0).children.length;i++) {  
           document.getElementsByClassName("container2").item(0).getElementsByClassName("greenElement").item(i).remove();
        }

        this.onCancel.emit();
    }

    public createModel() {

        this._mapper.templateDetailsToSend.templateTemplate = this.modelContent.replace(/\\n/g, "\n");
        this._mapper.templateDetailsToSend.templateName = this.templateName;
        this._mapper.templateDetailsToSend.codePhase = "ENVOI";
        this._mapper.templateDetailsToSend.format = new Format();
        this._mapper.templateDetailsToSend.format.formatDescription = "Short Message Service";
        this._mapper.templateDetailsToSend.format.formatId = "sms";
        this._mapper.templateDetailsToSend.templateType = new TemplateType();
        this._mapper.templateDetailsToSend.templateType.templateTypeDescription = "Par défaut - Configuration par l'IHM";
        this._mapper.templateDetailsToSend.templateType.templateTypeId = 0;
        this._mapper.templateDetailsToSend.templateContent = new TemplateContent();

        //Détermination du templateContentId (colonne "Nom du contenu")
        if(this.modelContent.split("template_content(")[1]!=undefined&&
           this.modelContent.split("template_content(")[1].split(")")[0]!=undefined) {

            this._mapper.templateDetailsToSend.templateContent.templateContentId = this.modelContent.split("template_content(")[1].split(")")[0];
        }
        else {

            this._mapper.templateDetailsToSend.templateContent.templateContentId = "MessageDefaut";
        }

        this.svwm.createWrittenMessageModel(this._mapper.templateDetailsToSend).subscribe(
                        response => {
                            if(response.status==200) {                    
                                this.onCreate.emit();
                                this._isModifying = false;                            
                                this._title = "118712 - Consultation du modèle de message écrits"; 
                            }
                        },
                        error => {alert(error)}
        );
    }

    public saveModel() {

        this._mapper.templateDetailsReceived.templateTemplate = this.modelContent.replace(/\\n/g, "\n");

        //Détermination du templateContentId (colonne "Nom du contenu")
        if(this.modelContent.split("template_content(")[1]!=undefined&&
           this.modelContent.split("template_content(")[1].split(")")[0]!=undefined) {

            this._mapper.templateDetailsReceived.templateContent.templateContentId = this.modelContent.split("template_content(")[1].split(")")[0];
        }
        else {

            this._mapper.templateDetailsReceived.templateContent.templateContentId = "MessageDefaut";
        }

        this.svwm.updateWrittenMessageModel(this._mapper.templateDetailsReceived, this.idTemplate).subscribe(
                        response => {
                            if(response.status==200) {                  
                                this.onSave.emit();
                                this._isModifying = false;                            
                                this._title = "118712 - Consultation du modèle de message écrits"; 
                            }
                        },
                        error => {alert(error)}
        );
    }

    public panelHelp(id:number) {   

        this._panelHelpId = id;
        if(id==1) {          
          this.myHelpStyle = {'top': '53em'};
        }
        else if(id==2) {
          this.myHelpStyle = {'top': '33em'};
        }
        else if(id==3) {
          this.myHelpStyle = {'top': '16em'};
        }
        this.modal.open();
    }

    public contentDisplay() {
      this.isContentDisplayed = true;
    }

    public contentHide() {
        this.isContentDisplayed = false;
    }

    public modelContent:string = "";
    //Logique de traitement dynamique de l'affichage des éléments
    public updateContent(src:string) {   

        let content:string = "";
        let separator:string = "";
        this.modelContent = "";       
        let j:number=0;  
        let k:number=0;
        for(let i =0;i<document.getElementsByClassName("container2").item(0).children.length;i++) {  
            if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML)!=undefined||
               this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML)!=undefined) {
                 if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML)!=undefined&&
                    document.getElementsByClassName("container2").item(0).children.item(i).innerHTML!="cr") {
                    content =  "$" + this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML).keyword;
                 }
                 else if(this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML)!=undefined&&
                         document.getElementsByClassName("container2").item(0).children.item(i).innerHTML!="cr") {
                    content =  "$" + this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML).keyword;
                 }
                 else if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML=="cr") {                    
                    content = "\\n";
                    separator = "";
                 }
                //Changement priorité éléments
                if(document.getElementsByClassName("container2").item(0).getElementsByClassName("blueElement").length-
                   document.getElementsByClassName("container2").item(0).getElementsByClassName("cr").length==
                   (document.getElementsByClassName("container3").item(0).children.length+document.getElementsByClassName("container5").item(0).children.length)) { 
                    for(let k =0;k<document.getElementsByClassName("container3").item(0).children.length;k++) {  
                        if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML==
                          document.getElementsByClassName("container3").item(0).children.item(k).innerHTML) {
                            //let n:number=k+1;
                            content += "["+(k)+"]";
                        }
                    }                       
                }  
                //Changement ordre d'affichage
                else {  
                  if(this.noPriorityContent.length!=0) {   
                    for(let k =0;k<this.essai.length;k++) { 
                        if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML==
                          this.essai[k]) {   
                            content += "["+(j)+"]";     
                            j++;  
                        }
                    }  
                  } 
                  else {
                      content += "["+(j)+"]";     
                      j++; 
                  }                   
                }    
                separator = " ";            
            }
            else {
                if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<').length<=1) {
                  if(content!="\\n") {
                      this.modelContent = this.modelContent.substring(0,this.modelContent.length-1);
                  }
                  content = "\\n";                          
                  separator = "";
                }
                else {
                    //Cas des éléments composés
                    if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0])!=undefined||
                       this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0])!=undefined) {
                          if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0])!=undefined) {

                              if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content") {

                                  content =  "$" + this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.firstFooterContent + ")";
                              }
                              else if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content_short") {

                                  content =  "$" + this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.secondFooterContent + ")";
                              }
                              else if(this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content_url") {

                                  content =  "$" + this.items.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.thirdFooterContent + ")";
                              }
                          }
                          else if(this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0])!=undefined) {
                              
                              if(this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content") {

                                  content =  "$" + this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.firstFooterContent + ")";
                              }
                              else if(this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content_short") {

                                  content =  "$" + this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.secondFooterContent + ")";
                              }
                              else if(this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword=="template_content_url") {

                                  content =  "$" + this.itemsOrder.find(item => item.description==document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]).keyword
                                      + "(" + this.thirdFooterContent + ")";
                              }
                          }
                          //Changement priorité éléments
                          if((document.getElementsByClassName("container2").item(0).getElementsByClassName("blueElement").length-
                             document.getElementsByClassName("container2").item(0).getElementsByClassName("cr").length)==
                            (document.getElementsByClassName("container3").item(0).children.length+document.getElementsByClassName("container5").item(0).children.length)) { 
                              for(let k =0;k<document.getElementsByClassName("container3").item(0).children.length;k++) {  
                                  if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]==
                                    document.getElementsByClassName("container3").item(0).children.item(k).innerHTML) {
                                      //let n:number=k+1;
                                      content += "["+(k)+"]";
                                  }
                              }                       
                          }  
                          //Changement ordre d'affichage
                          else {  
                              if(this.noPriorityContent.length!=0) {   
                                for(let k =0;k<this.essai.length;k++) { 
                                    if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0]==
                                      this.essai[k]) {   
                                        content += "["+(j)+"]";     
                                        j++;  
                                    }
                                }  
                              } 
                              else {
                                  content += "["+(j)+"]";     
                                  j++; 
                              }                   
                          }
                    }
                    else {
                        /*if(content!="\\n") {
                            this.modelContent = this.modelContent.substring(0,this.modelContent.length-1);
                        }*/
                        content = "\\n";                          
                        separator = "";
                    }
                }
            }     
            
            this.modelContent += content + separator;  
        }   
    }

    public firstFooterContent: string;
    public secondFooterContent: string;
    public thirdFooterContent: string;

    public selectTemplate(e:any) {

        if(e.target.className=="Pied de SMS par défaut"||e.target.className=="caca"){
            this.firstFooterContent = e.target.value;
        }
        else if(e.target.className=="Pied de SMS court par défaut"||e.target.className=="coco"){
            this.secondFooterContent = e.target.value;
        }
        else if(e.target.className=="Pied de SMS(2)"||e.target.className=="cucu"){
            this.thirdFooterContent = e.target.value;
        }

        this.updateContent("");
    }

    //Appellé à chaque changement de templateId 
    ngOnChanges() {
      
        this.items = new Array<RefTemplateItem>();
        this.itemsOrder = new Array<RefTemplateItem>();

        //Init de la valeur de l'input pour les éléments composés
        this.firstFooterContent = "MessageDefaut";
        this.secondFooterContent = "MessageDefaut";
        this.thirdFooterContent = "MessageDefaut";

        this.essai = [];  
        this.withPriorityContentTemp = [];     
        this.noPriorityContent = [];
        //Reset de l'action modifier pour la consult d'une prog différente   
        this.isModifying = false; 
        if(!this.Adding) { 
            if(this.idTemplate!=undefined) {

            //Récupération de la liste totale des items
            this.svrt.getWrittenMessageModelList().subscribe(data => {
                this.items = data.json();
            
                //Récupération du détail du Template
                this.svwm.getWrittenMessageModel(this.idTemplate).subscribe(data => {

                    this._mapper.templateDetailsReceived = new Template();
                    this._mapper.templateDetailsReceived = data.json();
                    //Mapping sur l'objet contenant les données d'affichage
                    this._mapper.templateToDisplayMapper(); 

                    //Traitement pour l'affichage (Binding)     
                    this.modelContent = this._mapper.templateDetailsReceived.templateTemplate.replace(/\n/g, "\\n");
                    //Priorité des éléments
                    this._mapper.templateDetailsToDisplay.elementWithoutPriority.forEach(el => {
                        if(el.substring(el.length-1,el.length)==' ') {

                            el = el.substring(0,el.length-1);
                        }
                        if(this.items.find(item => item.keyword==el)!=undefined) {

                            this.noPriorityContent.push(this.items.find(item => item.keyword==el).description);
                        }
                    });
                    this._mapper.templateDetailsToDisplay.elementWithPriority.forEach(el => {

                        if(this.items.find(item => item.keyword==el[0])!=undefined) {
                            this.essai.push(this.items.find(item => item.keyword==el[0]).description);
                        }
                    });

                    //Ordre d'affichage
                    this.itemsOrder = new Array<RefTemplateItem>();
                    this._mapper.templateDetailsToDisplay.elementOrder.forEach(el => {

                        if(el.substring(el.length-1,el.length)==' ') {

                            el = el.substring(0,el.length-1);
                        }

                        if(this.items.find(item => item.keyword==el)!=undefined) {

                            this.itemsOrder.push(this.items.find(item => item.keyword==el));

                            if(this.items.find(item => item.keyword==el).keyword=="template_content_short") {

                                    this.secondFooterContent = this.modelContent.split('template_content_short')[1].split('(')[1].split(')')[0];
                            }
                            else if(this.items.find(item => item.keyword==el).keyword=="template_content_url") {

                                    this.thirdFooterContent = this.modelContent.split('template_content_url')[1].split('(')[1].split(')')[0];
                            }
                            else if(this.items.find(item => item.keyword==el).keyword=="template_content") {
                                    
                                    this.firstFooterContent = this.modelContent.split('template_content')[1].split('(')[1].split(')')[0];
                            }
                        }
                        else if(el=='\n') {

                            let refCr:RefTemplateItem = new RefTemplateItem();
                            refCr.description = "cr";
                            this.itemsOrder.push(refCr)
                        }
                    });
                    //Eléments disponibles
                    this.itemsTemp = new Array<RefTemplateItem>();
                    this.items.forEach(el => {

                            let value = this.itemsOrder.find(item => item.description==el.description);
                            if(value==undefined) {

                            this.itemsTemp.push(el);
                            }
                    });
                    this.items = this.itemsTemp;
                }); 
                }); 
            }
        }
        else {       
            this._isModifying = false;
            this.templateName = "";
            this.modelContent = "";
            this._title = "118712 - Création du modèle de message écrits";

            this.svrt.getWrittenMessageModelList().subscribe(data => {
                this.items = data.json();
            });
        }
    }

    //Appellé quand les éléments sont affichés
    ngAfterViewChecked() {

      if(document.getElementsByClassName("container2").item(0).children.length>0) {
          //Les éléments identifiés comme carriage-return sont modifiés pour correspondre au style
          for(let i =0;i<document.getElementsByClassName("container2").item(0).children.length;i++) {  

            if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML=="cr") {

              document.getElementsByClassName("container2").item(0).children.item(i).innerHTML='<span class="carriage-return" style="position:relative;font-size:14px;" >&crarr;</span>';
            }
          }

          //Cas particulier des Pieds de SMS, une list de choix doit apparaitre
          for(let i =0;i<document.getElementsByClassName("container2").item(0).children.length;i++) {   
              
              if((document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0,"Pied de SMS par défaut".length)=="Pied de SMS par défaut"||                 
                 document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0,"Pied de SMS(2)".length)=="Pied de SMS(2)"||
                 document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0].substring(0,"Pied de SMS court par défaut".length)=="Pied de SMS court par défaut")) {                
                    //NONE
            }
              else {
                if(document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('span').length<=1) {

                  let content:string = document.getElementsByClassName("container2").item(0).children.item(i).innerHTML.split('<')[0];
                  document.getElementsByClassName("container2").item(0).children.item(i).innerHTML=content; 
                }
              }
          }
      }
    }

    //Constructor
    constructor(private dragulaService: DragulaService, private svwm: WrittenMessageService, private svrt: RefService, private elRef:ElementRef) {
      dragulaService.setOptions('bag-one', {
        copy: function (el, source) {        
          if(el.id=="carriage-return"&&source.className!="container2") {
            return true;
          }
          else {
            return false;
          }
        },
        moves: function (el, source, handle, sibling){
          return true
        },
        copySortSource: true,
        accepts: function (el, target, source, sibling) {

           if(target.className=="container1 container4"&&el.id!="carriage-return"){
             el.id='origin-left'
           }           
           else if(target.className=="container2"&&el.id!="carriage-return"){
             el.id='origin-right'
           }
          
          if((target.className=="container1 container4")&&(el.id=="carriage-return")) {
            return false;
          }
          else if((target.className=="container1")&&(el.id=="carriage-return")){
            return false;
          }
          else if((target.className=="container1 container4")&&(el.getElementsByClassName("carriage-return").item(0)!=null)) {
            return false;
          }
          else { 
            return true; 
          }
        }
      });

      dragulaService.drop.subscribe((value) => {
        let bag = value[0];
        let element = value[1];
        let dest = value[2];
        let source = value[3];
        //Cas particulier des Pieds de SMS, une list de choix doit apparaitre 
        if(element.id=="origin-right"&&(element.innerText=="Pied de SMS par défaut"||element.innerText=="Pied de SMS(2)"||element.innerText=="Pied de SMS court par défaut")) {
            let content:string = "";
            for(let i=0;i<this.textList.length;i++) {
                  content += '<option  value="' + this.textList[i] + '">' + this.textList[i] + '</option>';
            }    

            if(element.innerText=="Pied de SMS par défaut") {
                element.innerHTML += '<select class="caca">' + content + '</select>';                
                //Pour faire en sorte d'avoir un event déclenché
                this.elRef.nativeElement.querySelector('.caca').addEventListener('change', (evt) => this.selectTemplate(evt));
            }
            else if(element.innerText=="Pied de SMS court par défaut") {                
                element.innerHTML += '<select class="coco">' + content + '</select>';                
                //Pour faire en sorte d'avoir un event déclenché
                this.elRef.nativeElement.querySelector('.coco').addEventListener('change', (evt) => this.selectTemplate(evt));
            }
            else if(element.innerText=="Pied de SMS(2)") {                
                element.innerHTML += '<select class="cucu">' + content + '</select>';                
                //Pour faire en sorte d'avoir un event déclenché
                this.elRef.nativeElement.querySelector('.cucu').addEventListener('change', (evt) => this.selectTemplate(evt));
            }
        }
        else if(element.id=="origin-left"&&(element.innerHTML.split('<')[0]=="Pied de SMS par défaut"||element.innerHTML.split('<')[0]=="Pied de SMS(2)"||element.innerHTML.split('<')[0]=="Pied de SMS court par défaut")) {
            element.innerHTML = element.innerHTML.split('<')[0];     
        }

        if(bag=="bag-one"&&source.innerText==""&&element.innerText!=""&&element.id!="origin-right") {
          this.essai = [];
          this.noPriorityContent = [];
        }
        else {
          if(value[0]=="bag-one"&&element.innerText!=""&&element.id!="carriage-return") {
            if(dest!=null) {
                if(dest.innerText!=source.innerText) {
                  if(this.essai.find(el => el == element.innerHTML.split('<')[0])==undefined&&this.noPriorityContent.find(el => el == element.innerHTML.split('<')[0])==undefined) {
                    this.essai.push(element.innerHTML.split('<')[0]);
                  }
                  else {
                    let toto:string[] = [];
                    this.essai.forEach(el => {
                      if(el!=element.innerHTML.split('<')[0]){
                        toto.push(el);
                      }
                    });
                    this.essai=toto;  

                    let tata:string[] = [];
                    this.noPriorityContent.forEach(el => {
                      if(el!=element.innerHTML.split('<')[0]){
                        tata.push(el);
                      }
                    });
                    this.noPriorityContent=tata;            
                  }
                }
            }
          }
        }  
          this.updateContent(source.className);     
      });

       dragulaService.over.subscribe((value) => {
        let bag = value[0];
        let element = value[1];
        let dest = value[2];
        let source = value[3];
       });

      //Pour la supression de l'élément carriage-return
      dragulaService.cancel.subscribe((value) => {

        if(value[1].id=="carriage-return"||value[1].getElementsByClassName("carriage-return").item(0)!=null) {

          value[1].innerHTML =""
          value[1].className = "poubelle"      
          document.getElementsByClassName("poubelle").item(0).remove();
          this.updateContent("container2");  
        } 
      })

      this._title = "118712 - Consultation du modèle de message écrits";

      this.svrt.getWrittenMessageModelList().subscribe(data => {

            this.items = data.json();
      });
      
      this.svwm.getWrittenMessageTextList().subscribe(data => {

            this.messageText = data.json();
            this.messageText.forEach(el => {
                this.textList.push(el.templateContentId);
            })

            
      });
    }
}

