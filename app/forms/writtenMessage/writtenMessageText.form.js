"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var writtenMessage_service_1 = require("../../services/writtenMessage/writtenMessage.service");
var writtenMessage_mapper_1 = require("../../mappers/writtenMessage.mapper");
var DeleteWrittMessTextForm = /** @class */ (function () {
    function DeleteWrittMessTextForm() {
        this.onClose = new core_1.EventEmitter();
        this.onDelete = new core_1.EventEmitter();
    }
    DeleteWrittMessTextForm.prototype.deleteWrittMessText = function () {
        this.onDelete.emit(this.templateContentId);
    };
    DeleteWrittMessTextForm.prototype.closeWindow = function () {
        this.onClose.emit();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DeleteWrittMessTextForm.prototype, "templateContentId", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], DeleteWrittMessTextForm.prototype, "templateContentName", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteWrittMessTextForm.prototype, "onClose", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], DeleteWrittMessTextForm.prototype, "onDelete", void 0);
    DeleteWrittMessTextForm = __decorate([
        core_1.Component({
            selector: 'deleteWrittMessTextSelector',
            templateUrl: 'views/writtenMessage/writtenMessageTextDelete.html',
            styleUrls: ['views/writtenMessage/writtenMessageText.css']
        })
    ], DeleteWrittMessTextForm);
    return DeleteWrittMessTextForm;
}());
exports.DeleteWrittMessTextForm = DeleteWrittMessTextForm;
var WrittenMessageTextForm = /** @class */ (function () {
    function WrittenMessageTextForm(svcr) {
        this.svcr = svcr;
        this.onCancel = new core_1.EventEmitter();
        this.onUpdate = new core_1.EventEmitter();
        this.onSave = new core_1.EventEmitter();
        this.onCreate = new core_1.EventEmitter();
        this._title = "118712 - Consultation d'un pied de message écrit";
    }
    Object.defineProperty(WrittenMessageTextForm.prototype, "isUpdating", {
        //accesseurs
        get: function () {
            return this._isUpdating;
        },
        set: function (value) {
            this._isUpdating = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageTextForm.prototype, "title", {
        get: function () {
            return this._title;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageTextForm.prototype, "contractCustomers", {
        get: function () {
            return this._contractCustomers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WrittenMessageTextForm.prototype, "contractCustomerLabel", {
        get: function () {
            return this._contractCustomerLabel;
        },
        enumerable: true,
        configurable: true
    });
    //Methods
    WrittenMessageTextForm.prototype.updateWrttnMessageTxt = function () {
        this._isUpdating = true;
        this._title = "118712 - Modification d'un pied de message écrit";
        this.onUpdate.emit();
    };
    WrittenMessageTextForm.prototype.saveWrttnMessageTxt = function () {
        var _this = this;
        //let's get the proper constract customer ID available in the list, since we can't retrieve it directly from the form
        this.templateContent.contractCustomerId = this.contractCustomers.find(function (element) { return element.label == _this.templateContent.contractCustomerLabel; }).contractCustomerId;
        this.svcr.saveWrittenMessageText(this.templateContent, this.templateContent.templateContentId).subscribe(function (response) {
            if (response.status == 200) {
                _this.onSave.emit();
                _this._isUpdating = false;
                _this._title = "118712 - Consultation d'un pied de message écrit";
            }
        }, function (error) { alert(error); });
    };
    WrittenMessageTextForm.prototype.createWrttnMessageTxt = function () {
        var _this = this;
        if (this.templateContent.templateContentName != null && this.templateContent.templateContentName != "") {
            //let's get the proper constract customer ID available in the list, since we can't retrieve it directly from the form
            this.templateContent.contractCustomerId = this.contractCustomers.find(function (element) { return element.label == _this.templateContent.contractCustomerLabel; }).contractCustomerId;
            this.svcr.createWrittenMessageText(this.templateContent).subscribe(function (response) {
                if (response.status == 201) {
                    _this.onCreate.emit();
                    _this._isUpdating = false;
                    _this._title = "118712 - Consultation d'un pied de message écrit";
                }
            }, function (error) { alert(error); });
        }
        else {
            alert("WARNING: Veuillez renseigner le nom du pied de message écrit");
        }
    };
    WrittenMessageTextForm.prototype.cancel = function () {
        this._isUpdating = false;
        this.isAdding = false;
        this._title = "118712 - Consultation d'un pied de message écrit";
        this.onCancel.emit();
    };
    //Appelé à chaque changement d' attribut en input
    WrittenMessageTextForm.prototype.ngOnChanges = function () {
        var _this = this;
        this.svcr.getContractCustomerLabelList().subscribe(function (data) {
            _this._contractCustomers = data.json();
            // This is needed right here, to default-populate dropdown box, and avoid checking this field is fueled, 'cause badly needed on backend side
            _this.templateContent.contractCustomerLabel = _this._contractCustomers[0].label;
        });
        if (this.isAdding)
            this._title = "118712 - Création d'un pied de message écrit";
        if (this.isConsulting)
            this._title = "118712 - Consultation d'un pied de message écrit";
    };
    __decorate([
        core_2.ViewChild(ng2_bs3_modal_1.ModalComponent),
        __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
    ], WrittenMessageTextForm.prototype, "modal", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", writtenMessage_mapper_1.TemplateContent)
    ], WrittenMessageTextForm.prototype, "templateContent", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], WrittenMessageTextForm.prototype, "isAdding", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], WrittenMessageTextForm.prototype, "isConsulting", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageTextForm.prototype, "onCancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageTextForm.prototype, "onUpdate", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageTextForm.prototype, "onSave", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], WrittenMessageTextForm.prototype, "onCreate", void 0);
    WrittenMessageTextForm = __decorate([
        core_1.Component({
            selector: 'writtenMessageTextSelector',
            templateUrl: 'views/writtenMessage/writtenMessageTextDetails.html',
            styleUrls: ['views/writtenMessage/writtenMessageText.css'],
            providers: [writtenMessage_service_1.WrittenMessageService]
        }),
        __metadata("design:paramtypes", [writtenMessage_service_1.WrittenMessageService])
    ], WrittenMessageTextForm);
    return WrittenMessageTextForm;
}());
exports.WrittenMessageTextForm = WrittenMessageTextForm;
//# sourceMappingURL=writtenMessageText.form.js.map