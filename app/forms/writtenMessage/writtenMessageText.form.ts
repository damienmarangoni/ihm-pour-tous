import { Component, Input, Directive, Output, EventEmitter } from '@angular/core';
import { ViewChild, ViewEncapsulation, ContentChild } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { SimulatorDataService } from '../../services/mediaSimulators/mediaSimulator.service';
import { WrittenMessageService } from '../../services/writtenMessage/writtenMessage.service';

import { ContractCustomer, TemplateContent } from '../../mappers/writtenMessage.mapper';


@Component({
    selector: 'deleteWrittMessTextSelector',
    templateUrl: 'views/writtenMessage/writtenMessageTextDelete.html',
    styleUrls: ['views/writtenMessage/writtenMessageText.css']

})
export class DeleteWrittMessTextForm {
    @Input() templateContentId: string;
    @Input() templateContentName: string;
    @Output() onClose: EventEmitter<string> = new EventEmitter<string>();
    @Output() onDelete: EventEmitter<string> = new EventEmitter<string>();

    public deleteWrittMessText() {
        this.onDelete.emit(this.templateContentId);
    }

    public closeWindow() {
        this.onClose.emit();
    }
}



@Component({
    selector: 'writtenMessageTextSelector',
    templateUrl: 'views/writtenMessage/writtenMessageTextDetails.html',
    styleUrls: ['views/writtenMessage/writtenMessageText.css'],
    providers: [WrittenMessageService]
})
export class WrittenMessageTextForm {
    @ViewChild(ModalComponent) modal: ModalComponent;

    @Input() templateContent: TemplateContent;

    // @Input() templateContentName: string;
    // @Input() templateContentId: string;
    // @Input() contractCustomerLabel: string;
    // @Input() templateContentTxt: string;
    // @Input() templateContentShortTxt: string;

    @Input() isAdding: boolean;
    @Input() isConsulting: boolean;

    public id: string;

    private _isUpdating: boolean;
    private _title: string;
    private _contractCustomers: Array<ContractCustomer>;
    private _contractCustomerLabel: string;

    @Output() onCancel: EventEmitter<number> = new EventEmitter<number>();
    @Output() onUpdate: EventEmitter<number> = new EventEmitter<number>();
    @Output() onSave: EventEmitter<number> = new EventEmitter<number>();
    @Output() onCreate: EventEmitter<number> = new EventEmitter<number>();

    //accesseurs
    public get isUpdating() {
        return this._isUpdating;
    }

    public set isUpdating(value: boolean) {
        this._isUpdating = value;
    }


    public get title() {
        return this._title;
    }

    public get contractCustomers() {
        return this._contractCustomers;
    }

    
    public get contractCustomerLabel() {

        return this._contractCustomerLabel;
    }

    //Methods
    public updateWrttnMessageTxt() {

        this._isUpdating = true;
        this._title = "118712 - Modification d'un pied de message écrit";
        this.onUpdate.emit();
    }

    public saveWrttnMessageTxt() {

        //let's get the proper constract customer ID available in the list, since we can't retrieve it directly from the form
        this.templateContent.contractCustomerId = this.contractCustomers.find(element => element.label == this.templateContent.contractCustomerLabel).contractCustomerId;

        this.svcr.saveWrittenMessageText(this.templateContent, this.templateContent.templateContentId).subscribe(
            response => {
                if (response.status == 200) {
                    this.onSave.emit();
                    this._isUpdating = false;
                    this._title = "118712 - Consultation d'un pied de message écrit";
                }
            },
            error => { alert(error) });

    }

    public createWrttnMessageTxt() {

        if (this.templateContent.templateContentName != null && this.templateContent.templateContentName != "") {
           
            //let's get the proper constract customer ID available in the list, since we can't retrieve it directly from the form
            this.templateContent.contractCustomerId = this.contractCustomers.find(element => element.label == this.templateContent.contractCustomerLabel).contractCustomerId;
            this.svcr.createWrittenMessageText(this.templateContent).subscribe(
                response => {
                    if (response.status == 201) {
                        this.onCreate.emit();
                        this._isUpdating = false;
                        this._title = "118712 - Consultation d'un pied de message écrit";
                    }
                },
                error => { alert(error) }
            );

        } else {
            alert("WARNING: Veuillez renseigner le nom du pied de message écrit");
        }

    }

    public cancel() {

        this._isUpdating = false;
        this.isAdding = false;
        this._title = "118712 - Consultation d'un pied de message écrit";

        this.onCancel.emit();
    }

    //Appelé à chaque changement d' attribut en input
    public ngOnChanges() {

        this.svcr.getContractCustomerLabelList().subscribe(data => {
            this._contractCustomers = data.json()
            // This is needed right here, to default-populate dropdown box, and avoid checking this field is fueled, 'cause badly needed on backend side
            this.templateContent.contractCustomerLabel = this._contractCustomers[0].label;
            
        });

        
        if (this.isAdding)  this._title = "118712 - Création d'un pied de message écrit";

        if (this.isConsulting) this._title = "118712 - Consultation d'un pied de message écrit";


        



    }

    constructor(private svcr: WrittenMessageService) {
        this._title = "118712 - Consultation d'un pied de message écrit";


    }
}


