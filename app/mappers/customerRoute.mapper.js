"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomerRouteMapper = /** @class */ (function () {
    function CustomerRouteMapper() {
        /**************************************************************************/
        /* Mapping du détails d'un parcours pour l'affichage                      */
        /**************************************************************************/
        //Données pour l'affichage
        this.customerRouteDetailsToDisplay = [[]];
        this.communitiesDetails = [];
        this._customerRoute = new CustomerRouteToDisplay();
        this._customerRouteDetails = new CustomerRouteDetailsToDisplay();
        /**************************************************************************/
        /* Mapping de la liste parcours pour l'affichage                          */
        /**************************************************************************/
        //Données pour l'affichage
        this.customerRouteListToDisplay = [[]];
    }
    CustomerRouteMapper.prototype.customerRouteDetailsToDisplayMapper = function () {
        var _this = this;
        var du = [];
        var community = [];
        var category = [];
        this.customerRouteDetailsToDisplay = Array();
        this.customerRouteDetails[0].qualificationExpressionsDTOs.forEach(function (details) {
            if (details.service != null) {
                var duAlreadyStored = false;
                for (var i = 0; i < du.length; i++) {
                    if (du[i] == "DU " + details.service.serviceDu.toString() + " " + details.service.serviceDescription) {
                        duAlreadyStored = true;
                    }
                }
                if (!duAlreadyStored) {
                    du.push("DU " + details.service.serviceDu.toString() + " " + details.service.serviceDescription);
                }
            }
            else {
                du.push("DU 0 Tous réseaux");
            }
            //Traitement de la valeur communityFilter
            if (details.communityFilter != 0) {
                //S'agit-il d'un et ou d'un' ou ?
                //Renseigner le ET/OU : si le filtre est une puissance de deux : c'est un OU, sinon un ET
                var isEtOu = "";
                isEtOu = (Math.log(details.communityFilter) % Math.log(2) == 0 ? "OU" : "ET");
                if (isEtOu == "OU") {
                    var communityAlreadyStored = false;
                    for (var i = 0; i < community.length; i++) {
                        if (community[i] == _this.communitiesDetails[details.communityFilter]) {
                            communityAlreadyStored = true;
                        }
                    }
                    if (!communityAlreadyStored) {
                        community.push(_this.communitiesDetails[details.communityFilter]);
                    }
                }
                else {
                    //Trouver les valeurs de champ ET (par calcul binaire, communauté)
                    var chaineBinaire = details.communityFilter.toString(2);
                    //alert(details.communityFilter.toString(2))
                    //On obtient la chaine binaire, on va maintenant prendre chaque bit à 1,
                    //le monter au carré pour connaître chaque valeur        
                    var communityAlreadyStored = false;
                    for (var i = 0; i < chaineBinaire.length; i++) {
                        if (chaineBinaire.charAt(i) == '1') {
                            //alert(i)
                            for (var j = 0; j < community.length; j++) {
                                //alert(Math.round(Math.pow(2.0, i)))
                                if (community[j] == _this.communitiesDetails[Math.round(Math.pow(2.0, i))]) {
                                    communityAlreadyStored = true;
                                    //alert("true")
                                }
                            }
                            if (!communityAlreadyStored) {
                                community.push(_this.communitiesDetails[Math.round(Math.pow(2.0, i))]);
                            }
                        }
                    }
                }
            }
            else {
                if (community.find(function (comm) { return comm == "Toutes communautés"; }) == undefined) {
                    community.push("Toutes communautés");
                }
            }
            var categoryAlreadyStored = false;
            for (var i = 0; i < category.length; i++) {
                if (category[i] == details.refCategory.libCategory) {
                    categoryAlreadyStored = true;
                }
            }
            if (!categoryAlreadyStored) {
                category.push(details.refCategory.libCategory);
            }
        });
        this.customerRouteDetailsToDisplay = [du];
        this.customerRouteDetailsToDisplay.push(community);
        this.customerRouteDetailsToDisplay.push(category);
    };
    CustomerRouteMapper.prototype.customerRouteListToDisplayMapper = function () {
        var _this = this;
        //Suppression du premier élément du tableau
        //this.customerRouteListToDisplay.pop();
        this.customerRouteListToDisplay = Array();
        this.customerRouteListDetails.forEach(function (details) {
            _this._customerRoute.routeName = details.customerQualificationName.toString();
            _this._customerRoute.idRoute = details.customerQualificationId.toString();
            _this._customerRoute.accPhase = "Par défaut";
            _this._customerRoute.waitPhase = "Par défaut";
            _this._customerRoute.feePhase = "Par défaut";
            _this._customerRoute.promoPhase = "Par défaut";
            _this._customerRoute.smsPhase = "Par défaut";
            details.programParentDTOs.forEach(function (prog) {
                if (prog.template.codePhase == "ACC") {
                    _this._customerRoute.accPhase = prog.template.templateName.toString();
                }
                else if (prog.template.codePhase == "ATT") {
                    _this._customerRoute.waitPhase = prog.template.templateName.toString();
                }
                else if (prog.template.codePhase == "TARIF") {
                    _this._customerRoute.feePhase = prog.template.templateName.toString();
                }
                else if (prog.template.codePhase == "PROMO") {
                    _this._customerRoute.promoPhase = prog.template.templateName.toString();
                }
                else if (prog.template.codePhase == "ENVOI") {
                    _this._customerRoute.smsPhase = prog.template.templateName.toString();
                }
            });
            _this.customerRouteListToDisplay.push([_this._customerRoute.routeName,
                _this._customerRoute.idRoute,
                _this._customerRoute.accPhase,
                _this._customerRoute.feePhase,
                _this._customerRoute.waitPhase,
                _this._customerRoute.promoPhase,
                _this._customerRoute.smsPhase]);
        });
    };
    CustomerRouteMapper.prototype.customerRouteDetailsFromDisplayMapper = function (idRoute, routeName, segmentsList, communitiesList) {
        var _this = this;
        this._qualificationExpressionList = new Array();
        //Récupération des communautés
        var communityResponse = 0;
        var communityFilter = 0;
        this.customerRouteDetailsFromDisplay[1].forEach(function (community) {
            var isEtOu = "ET"; //On force le ET pour le moment
            if (isEtOu == "ET") {
                communityResponse += 4 * communitiesList.find(function (comm) { return comm.libCommunity == community; }).codeCommunity;
                communityFilter += 4 * communitiesList.find(function (comm) { return comm.libCommunity == community; }).codeCommunity;
            }
            else {
            }
        });
        //Récupération des catégories
        this.customerRouteDetailsFromDisplay[2].forEach(function (category) {
            var libCategory;
            var codeCategory;
            libCategory = segmentsList.find(function (segment) { return segment.libCategory == category; }).libCategory;
            codeCategory = segmentsList.find(function (segment) { return segment.libCategory == category; }).codeCategory;
            //Récupération des DUs
            _this.customerRouteDetailsFromDisplay[0].forEach(function (du) {
                _this._qualificationExpression = new QualificationExpression();
                _this._qualificationExpression.refCategory = new RefCategory();
                _this._qualificationExpression.service = new Service();
                var servDet = [];
                servDet = du.split(" ");
                _this._qualificationExpression.service.serviceDu = parseInt(servDet[1]);
                _this._qualificationExpression.service.serviceDescription = du.slice(servDet[0].length + servDet[1].length + 1);
                _this._qualificationExpression.refCategory.codeCategory = codeCategory;
                _this._qualificationExpression.refCategory.libCategory = libCategory;
                _this._qualificationExpression.communityResponse = communityResponse;
                _this._qualificationExpression.communityFilter = communityFilter;
                _this._qualificationExpression.idCustomerQualification = parseInt(idRoute);
                _this._qualificationExpressionList.push(_this._qualificationExpression);
            });
        });
        this.customerRouteDetailsToSend = new CustomerRouteDetails();
        this.customerRouteDetailsToSend.qualificationExpressionsDTOs = new Array();
        this.customerRouteDetailsToSend.customerQualificationName = routeName;
        this.customerRouteDetailsToSend.customerQualificationId = parseInt(idRoute);
        this.customerRouteDetailsToSend.qualificationExpressionsDTOs = this._qualificationExpressionList;
    };
    return CustomerRouteMapper;
}());
exports.CustomerRouteMapper = CustomerRouteMapper;
/**********************************************************/
/* Objets de destination pour l'affichage                 */
/**********************************************************/
var CustomerRouteToDisplay = /** @class */ (function () {
    function CustomerRouteToDisplay() {
    }
    return CustomerRouteToDisplay;
}());
exports.CustomerRouteToDisplay = CustomerRouteToDisplay;
var CustomerRouteDetailsToDisplay = /** @class */ (function () {
    function CustomerRouteDetailsToDisplay() {
    }
    return CustomerRouteDetailsToDisplay;
}());
exports.CustomerRouteDetailsToDisplay = CustomerRouteDetailsToDisplay;
/**********************************************************/
/* Objets en réception du service                         */
/**********************************************************/
var CustomerRouteDetails = /** @class */ (function () {
    function CustomerRouteDetails() {
    }
    return CustomerRouteDetails;
}());
exports.CustomerRouteDetails = CustomerRouteDetails;
var ProgramParent = /** @class */ (function () {
    function ProgramParent() {
    }
    return ProgramParent;
}());
exports.ProgramParent = ProgramParent;
var Location = /** @class */ (function () {
    function Location() {
    }
    return Location;
}());
exports.Location = Location;
var Campaign = /** @class */ (function () {
    function Campaign() {
    }
    return Campaign;
}());
exports.Campaign = Campaign;
var Template = /** @class */ (function () {
    function Template() {
    }
    return Template;
}());
exports.Template = Template;
var TemplateContent = /** @class */ (function () {
    function TemplateContent() {
    }
    return TemplateContent;
}());
exports.TemplateContent = TemplateContent;
var QualificationExpression = /** @class */ (function () {
    function QualificationExpression() {
    }
    return QualificationExpression;
}());
exports.QualificationExpression = QualificationExpression;
var Service = /** @class */ (function () {
    function Service() {
    }
    return Service;
}());
exports.Service = Service;
var RefCategory = /** @class */ (function () {
    function RefCategory() {
    }
    return RefCategory;
}());
exports.RefCategory = RefCategory;
var RefCommunity = /** @class */ (function () {
    function RefCommunity() {
    }
    return RefCommunity;
}());
exports.RefCommunity = RefCommunity;
/**********************************************************/
/* Objets à envoyer au service                         */
/**********************************************************/
/*export class QualificationExpressionToSend {
  communityFilter: number;
  refCategory: RefCategory
  communityResponse: number;
  idQualificationExpression: number;
  service: Service;
  idCustomerQualification: number;
}*/ 
//# sourceMappingURL=customerRoute.mapper.js.map