export class CustomerRouteMapper {

  /**************************************************************************/  
  /* Mapping du détails d'un parcours pour l'affichage                      */
  /**************************************************************************/  
  //Données pour l'affichage
  public customerRouteDetailsToDisplay: Array<string[]> = [[]];  
  //Données en provenance du serveur
  public customerRouteDetails: Array<CustomerRouteDetails>;
  public communitiesDetails: string[] = [];
  
  private _customerRoute: CustomerRouteToDisplay = new CustomerRouteToDisplay();
  private _customerRouteDetails: CustomerRouteDetailsToDisplay = new CustomerRouteDetailsToDisplay();

  public customerRouteDetailsToDisplayMapper() {
    let du: string[] = [];
    let community: string[] = [];
    let category: string[] = [];

    this.customerRouteDetailsToDisplay = Array<string[]>();

    this.customerRouteDetails[0].qualificationExpressionsDTOs.forEach((details) => { 
      if(details.service!=null) {        
        let duAlreadyStored: boolean = false;
        for(let i=0;i<du.length;i++) {
          if (du[i]=="DU " + details.service.serviceDu.toString() + " " + details.service.serviceDescription) {
            duAlreadyStored = true
          }
          
        }
        if(!duAlreadyStored) {
          du.push("DU " + details.service.serviceDu.toString() + " " + details.service.serviceDescription);
        }
      }
      else {
          du.push("DU 0 Tous réseaux");
      }

      //Traitement de la valeur communityFilter
      if(details.communityFilter!=0) {
        //S'agit-il d'un et ou d'un' ou ?
        //Renseigner le ET/OU : si le filtre est une puissance de deux : c'est un OU, sinon un ET
        let isEtOu: string = "";
        isEtOu=(Math.log(details.communityFilter)%Math.log(2)==0?"OU":"ET");

        if (isEtOu=="OU"){
          let communityAlreadyStored: boolean = false;
          for(let i=0;i<community.length;i++) {
            if (community[i]==this.communitiesDetails[details.communityFilter]) {
              communityAlreadyStored = true
            }
          }

          if(!communityAlreadyStored) {
            community.push(this.communitiesDetails[details.communityFilter]);
          }
        }
        else{
          //Trouver les valeurs de champ ET (par calcul binaire, communauté)
          let chaineBinaire: string = details.communityFilter.toString(2);
          //alert(details.communityFilter.toString(2))
          //On obtient la chaine binaire, on va maintenant prendre chaque bit à 1,
          //le monter au carré pour connaître chaque valeur        
          let communityAlreadyStored: boolean = false;
          for (let i=0;i<chaineBinaire.length;i++){
            if (chaineBinaire.charAt(i)=='1'){
              //alert(i)
              for(let j=0;j<community.length;j++) {
                //alert(Math.round(Math.pow(2.0, i)))
                if (community[j]==this.communitiesDetails[Math.round(Math.pow(2.0, i))]) {
                  communityAlreadyStored = true
                  //alert("true")
                }
              }
              if(!communityAlreadyStored) {
                community.push(this.communitiesDetails[Math.round(Math.pow(2.0, i))]);
              }
            }
          }
        }
      }
      else {
        if(community.find(comm => comm=="Toutes communautés")==undefined) {
          community.push("Toutes communautés");
        }
      }

      let categoryAlreadyStored: boolean = false;
        for(let i=0;i<category.length;i++) {
          if (category[i]==details.refCategory.libCategory) {
            categoryAlreadyStored = true
          }
        }
        if(!categoryAlreadyStored) {
          category.push(details.refCategory.libCategory);
        }
    });    

    this.customerRouteDetailsToDisplay = [du];   
    this.customerRouteDetailsToDisplay.push(community);   
    this.customerRouteDetailsToDisplay.push(category);  
  }

  /**************************************************************************/  
  /* Mapping de la liste parcours pour l'affichage                          */
  /**************************************************************************/  
  //Données pour l'affichage
  public customerRouteListToDisplay: Array<string[]> = [[]];
  //Données en provenance du serveur
  public customerRouteListDetails: Array<CustomerRouteDetails>;
  
  public customerRouteListToDisplayMapper() {

    //Suppression du premier élément du tableau
    //this.customerRouteListToDisplay.pop();
    this.customerRouteListToDisplay = Array<string[]>();
    this.customerRouteListDetails.forEach((details) => {
        this._customerRoute.routeName = details.customerQualificationName.toString();
        this._customerRoute.idRoute = details.customerQualificationId.toString();
        this._customerRoute.accPhase = "Par défaut";
        this._customerRoute.waitPhase = "Par défaut";
        this._customerRoute.feePhase = "Par défaut";
        this._customerRoute.promoPhase = "Par défaut";
        this._customerRoute.smsPhase = "Par défaut";
        details.programParentDTOs.forEach((prog) => {
            if(prog.template.codePhase == "ACC") {          
              this._customerRoute.accPhase = prog.template.templateName.toString();
            } 
            else if(prog.template.codePhase == "ATT") {
              this._customerRoute.waitPhase = prog.template.templateName.toString();
            }
            else if(prog.template.codePhase == "TARIF") {
              this._customerRoute.feePhase = prog.template.templateName.toString();
            }
            else if(prog.template.codePhase == "PROMO") {
              this._customerRoute.promoPhase = prog.template.templateName.toString();
            }
            else if(prog.template.codePhase == "ENVOI") {
              this._customerRoute.smsPhase = prog.template.templateName.toString();
            }    
        });

        this.customerRouteListToDisplay.push([this._customerRoute.routeName,
                                      this._customerRoute.idRoute,
                                      this._customerRoute.accPhase,
                                      this._customerRoute.feePhase,
                                      this._customerRoute.waitPhase,
                                      this._customerRoute.promoPhase,
                                      this._customerRoute.smsPhase]);   
    });
  }

  /**************************************************************************/
  /* Mapping du détails d'un parcours pour la création ou la modification   */
  /**************************************************************************/  
  //Données de l'affichage
  //public customerRouteDetailsFromDisplay: Array<string[]> = new Array<string[]>(); 
  public customerRouteDetailsFromDisplay: Array<string[]>;
  //Données à envoyer au serveur    
  public customerRouteDetailsToSend: CustomerRouteDetails;  

  private _qualificationExpressionList: Array<QualificationExpression>;  
  private _qualificationExpression: QualificationExpression;

  public customerRouteDetailsFromDisplayMapper(idRoute:string, routeName:string, segmentsList: Array<RefCategory>, communitiesList: Array<RefCommunity>) {

      this._qualificationExpressionList = new Array<QualificationExpression>();  
      //Récupération des communautés
      let communityResponse: number = 0;
      let communityFilter: number = 0;
      this.customerRouteDetailsFromDisplay[1].forEach(community => {
    
      let isEtOu: string = "ET"; //On force le ET pour le moment
      if (isEtOu=="ET") {
          communityResponse += 4*communitiesList.find(comm => comm.libCommunity==community).codeCommunity;
          communityFilter += 4*communitiesList.find(comm => comm.libCommunity==community).codeCommunity; 
      }
      else{

      }
    });
    //Récupération des catégories
    this.customerRouteDetailsFromDisplay[2].forEach(category => { 
      let libCategory: string;
      let codeCategory: number;
      libCategory = segmentsList.find(segment => segment.libCategory==category).libCategory;
      codeCategory = segmentsList.find(segment => segment.libCategory==category).codeCategory;
      //Récupération des DUs
      this.customerRouteDetailsFromDisplay[0].forEach(du => {    
        this._qualificationExpression = new QualificationExpression();  
        this._qualificationExpression.refCategory = new RefCategory();
        this._qualificationExpression.service = new Service();
        let servDet: string[] = [];
        servDet = du.split(" ");
        this._qualificationExpression.service.serviceDu = parseInt(servDet[1]);
        this._qualificationExpression.service.serviceDescription = du.slice(servDet[0].length+servDet[1].length+1);  
        this._qualificationExpression.refCategory.codeCategory = codeCategory;
        this._qualificationExpression.refCategory.libCategory = libCategory;
        this._qualificationExpression.communityResponse = communityResponse;
        this._qualificationExpression.communityFilter = communityFilter;
        this._qualificationExpression.idCustomerQualification = parseInt(idRoute);
        this._qualificationExpressionList.push(this._qualificationExpression);
      });
    });
    this.customerRouteDetailsToSend = new CustomerRouteDetails();
    this.customerRouteDetailsToSend.qualificationExpressionsDTOs = new Array<QualificationExpression>();
    this.customerRouteDetailsToSend.customerQualificationName = routeName;
    this.customerRouteDetailsToSend.customerQualificationId = parseInt(idRoute);
    this.customerRouteDetailsToSend.qualificationExpressionsDTOs = this._qualificationExpressionList;
  }
}

/**********************************************************/
/* Objets de destination pour l'affichage                 */
/**********************************************************/
export class CustomerRouteToDisplay {
  routeName: string;
  idRoute: string;
  accPhase: string;
  feePhase: string;
  waitPhase: string;
  promoPhase: string;
  smsPhase: string;
}

export class CustomerRouteDetailsToDisplay {
  service: string[];
  community: string[];
  category: string[];
}

/**********************************************************/
/* Objets en réception du service                         */
/**********************************************************/
export class CustomerRouteDetails {
  customerQualificationName: string;
  customerQualificationId: number;
  programParentDTOs: Array<ProgramParent>;
  qualificationExpressionsDTOs: Array<QualificationExpression>;
}

export class ProgramParent {
  acces: number;
  dateEnd: string;
  dateStart: string;
  idProgramParent: number;
  location: Location;
  campaign: Campaign;
  template: Template;
  templateContent: TemplateContent;
}

export class Location {
  idLocation: number;
  label: string;
}

export class Campaign {
  campaignName: string;
  dateEnd: string;
  dateStart: string;
  idCampaign: number;
}

export class Template {
  access: number;
  display: number;
  idTemplate: number;
  codePhase: string;
  templateName: string;
}

export class TemplateContent {
    templateContentId: number;
    templateContentName: string;
    templateContentShortTxt: string;
    templateContentTxt: string;
    idContractCustomer:number;
}

export class QualificationExpression {
  communityFilter: number;
  refCategory: RefCategory
  communityResponse: number;
  idQualificationExpression: number;
  service: Service;
  idCustomerQualification: number;
}

export class Service {
  serviceDescription: string;
  serviceDu: number;
  serviceUser: string;
}

export class RefCategory {
  codeCategory: number;
  libCategory: string;
}

export class RefCommunity {
    codeCommunity:number;
    libCommunity:string;
}

/**********************************************************/
/* Objets à envoyer au service                         */
/**********************************************************/
/*export class QualificationExpressionToSend {
  communityFilter: number;
  refCategory: RefCategory
  communityResponse: number;
  idQualificationExpression: number;
  service: Service;
  idCustomerQualification: number;
}*/