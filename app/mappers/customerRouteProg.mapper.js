"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CustomerRouteProgMapper = /** @class */ (function () {
    function CustomerRouteProgMapper() {
        /**************************************************************************/
        /* Mapping du détails d'une programmation                                 */
        /**************************************************************************/
        //Données à afficher
        this.programmationDetailsToDisplay = new ProgrammationDetailsToDisplay();
        //Données reçues du serveur
        this.programmationDetailsReceived = new ProgrammationDetails();
    }
    CustomerRouteProgMapper.prototype.customerRouteProgToDisplayMapper = function () {
        var _this = this;
        this.programmationDetailsToDisplay.idProgramParent = this.programmationDetailsReceived[0].idProgramParent;
        this.programmationDetailsToDisplay.messageName = this.programmationDetailsReceived[0].template.templateName;
        this.programmationDetailsToDisplay.priority = this.programmationDetailsReceived[0].refPriority.libPriority;
        if (this.programmationDetailsReceived[0].templateContent != null) {
            this.programmationDetailsToDisplay.templateContentTxt = this.programmationDetailsReceived[0].templateContent.templateContentTxt;
        }
        //Détermination du choix type: Liste de dates ou Périodes
        if ((this.programmationDetailsReceived[0].vevent == "") || (this.programmationDetailsReceived[0].vevent == null)) {
            this.programmationDetailsToDisplay.periodChecked = false;
            this.programmationDetailsToDisplay.dateList = new Array();
            this.programmationDetailsReceived[0].programs.forEach(function (element) {
                var programDate = new ProgramDate();
                programDate.dateEnd = element.dateEnd;
                programDate.dateStart = element.dateStart;
                _this.programmationDetailsToDisplay.dateList.push(programDate);
            });
            this.programmationDetailsToDisplay.periodDetails = new ProgramPeriod();
            this.programmationDetailsToDisplay.periodDetails.type = "DAILY";
        }
        else {
            this.programmationDetailsToDisplay.periodChecked = true;
            this.programmationDetailsToDisplay.periodDetails = new ProgramPeriod();
            //Décodage champ vevent
            var split = this.programmationDetailsReceived[0].vevent.split("\n");
            split.forEach(function (element) {
                var value = element.split(":");
                switch (value[0]) {
                    case "DTSTART":
                        var dateHourStart = void 0;
                        dateHourStart = value[1].split("T");
                        _this.programmationDetailsToDisplay.periodDetails.dateStart = dateHourStart[0];
                        _this.programmationDetailsToDisplay.periodDetails.hourStart = dateHourStart[1];
                        break;
                    case "DTEND":
                        var dateHourEnd = void 0;
                        dateHourEnd = value[1].split("T");
                        _this.programmationDetailsToDisplay.periodDetails.dateEnd = dateHourEnd[0];
                        _this.programmationDetailsToDisplay.periodDetails.hourEnd = dateHourEnd[1];
                        break;
                    case "DURATION":
                        var duration = void 0;
                        duration = value[1].slice(0, value[1].length - 1);
                        switch (value[1].slice(value[1].length - 1)) {
                            case "D":
                                duration = duration + "j";
                                break;
                            case "W":
                                duration = duration + "sem";
                                break;
                            case "H":
                                duration = duration + "h";
                                break;
                        }
                        _this.programmationDetailsToDisplay.periodDetails.duration = duration;
                        break;
                    case "RRULE":
                        var rules = void 0;
                        rules = value[1].split(";");
                        var rule1 = void 0;
                        rule1 = rules[0].split("=");
                        _this.programmationDetailsToDisplay.periodDetails.type = rule1[1];
                        var rule2 = void 0;
                        rule2 = rules[1].split("=");
                        _this.programmationDetailsToDisplay.periodDetails.interval = rule2[1];
                        if (rule1[1] != "DAILY") {
                            var rule3 = void 0;
                            rule3 = rules[2].split("=");
                            /*this.programmationDetailsToDisplay.periodDetails.byX = rule3[1];*/
                            var byX = void 0;
                            byX = rule3[1].split(",");
                            _this.programmationDetailsToDisplay.periodDetails.mondayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.tuesdayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.wedenesdayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.thursdayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.fridayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.saturdayChecked = false;
                            _this.programmationDetailsToDisplay.periodDetails.sundayChecked = false;
                            byX.forEach(function (element) {
                                if (element == "MO") {
                                    _this.programmationDetailsToDisplay.periodDetails.mondayChecked = true;
                                }
                                else if (element == "TU") {
                                    _this.programmationDetailsToDisplay.periodDetails.tuesdayChecked = true;
                                }
                                else if (element == "WE") {
                                    _this.programmationDetailsToDisplay.periodDetails.wedenesdayChecked = true;
                                }
                                else if (element == "TH") {
                                    _this.programmationDetailsToDisplay.periodDetails.thursdayChecked = true;
                                }
                                else if (element == "FR") {
                                    _this.programmationDetailsToDisplay.periodDetails.fridayChecked = true;
                                }
                                else if (element == "SA") {
                                    _this.programmationDetailsToDisplay.periodDetails.saturdayChecked = true;
                                }
                                else if (element == "SU") {
                                    _this.programmationDetailsToDisplay.periodDetails.sundayChecked = true;
                                }
                                else {
                                    _this.programmationDetailsToDisplay.periodDetails.the = element;
                                }
                            });
                        }
                        break;
                    case "EXDATE;VALUE=DATE":
                        var exdate_1;
                        exdate_1 = value[1].split(",");
                        _this.programmationDetailsToDisplay.periodDetails.exception = "";
                        exdate_1.forEach(function (element) {
                            if (element == exdate_1[0]) {
                                _this.programmationDetailsToDisplay.periodDetails.exception += element.slice(6) + "/" + element.slice(4, 6) + "/" + element.slice(0, 4);
                            }
                            else {
                                _this.programmationDetailsToDisplay.periodDetails.exception += "," + element.slice(6) + "/" + element.slice(4, 6) + "/" + element.slice(0, 4);
                            }
                        });
                        break;
                    default:
                        break;
                }
            });
        }
    };
    //Mapping du détails d'une programmation
    CustomerRouteProgMapper.prototype.customerRouteProgFromDisplayMapper = function (phase, templateList, templateContentList, refPriorityList) {
        var _this = this;
        this.programmationeDetailsToSend = new ProgrammationDetails();
        if (this.programmationDetailsReceived[0] != undefined) {
            this.programmationeDetailsToSend.idProgramParent = this.programmationDetailsReceived[0].idProgramParent;
        }
        this.programmationeDetailsToSend.programs = new Array();
        if (this.programmationDetailsFromDisplay.periodChecked == false) {
            for (var i = 0; i < this.programmationDetailsFromDisplay.dateList.length; i++) {
                var date1 = new Date(this.programmationeDetailsToSend.dateStart);
                var date2 = new Date(this.programmationDetailsFromDisplay.dateList[i].dateStart);
                if ((date1.getTime() > date2.getTime()) || this.programmationeDetailsToSend.dateStart == undefined) {
                    this.programmationeDetailsToSend.dateStart = this.programmationDetailsFromDisplay.dateList[i].dateStart;
                }
                var date3 = new Date(this.programmationeDetailsToSend.dateEnd);
                var date4 = new Date(this.programmationDetailsFromDisplay.dateList[i].dateEnd);
                if (date3.getTime() < date4.getTime() || this.programmationeDetailsToSend.dateEnd == undefined) {
                    this.programmationeDetailsToSend.dateEnd = this.programmationDetailsFromDisplay.dateList[i].dateEnd;
                }
                var prog = new Program();
                prog.access = 0;
                prog.dateStart = this.programmationDetailsFromDisplay.dateList[i].dateStart;
                prog.dateEnd = this.programmationDetailsFromDisplay.dateList[i].dateEnd;
                this.programmationeDetailsToSend.programs.push(prog);
            }
        }
        else {
        }
        this.programmationeDetailsToSend.campaign = new Campaign();
        this.programmationeDetailsToSend.campaign.idCampaign = 1;
        this.programmationeDetailsToSend.campaign.campaignName = "Hors campagne";
        this.programmationeDetailsToSend.location = new Location();
        this.programmationeDetailsToSend.location.idLocation = 1;
        //TO ADD: Partie programs
        this.programmationeDetailsToSend.template = new Template();
        this.programmationeDetailsToSend.template = templateList.find(function (element) { return element.templateName == _this.programmationDetailsFromDisplay.messageName; });
        this.programmationeDetailsToSend.refPriority = new RefPriority();
        this.programmationeDetailsToSend.refPriority = refPriorityList.find(function (element) { return element.libPriority == _this.programmationDetailsFromDisplay.priority; });
        this.programmationeDetailsToSend.templateContent = new TemplateContent();
        if (phase == "Sms") {
            this.programmationeDetailsToSend.templateContent = templateContentList.find(function (element) { return element.templateContentTxt == _this.programmationDetailsFromDisplay.templateContentTxt; });
        }
        else {
            this.programmationeDetailsToSend.templateContent = templateContentList.find(function (element) { return element.templateContentId == _this.programmationeDetailsToSend.template.templateContent.templateContentId; });
        }
    };
    return CustomerRouteProgMapper;
}());
exports.CustomerRouteProgMapper = CustomerRouteProgMapper;
//Objets pour l'affichage
var ProgrammationDetailsToDisplay = /** @class */ (function () {
    function ProgrammationDetailsToDisplay() {
    }
    return ProgrammationDetailsToDisplay;
}());
exports.ProgrammationDetailsToDisplay = ProgrammationDetailsToDisplay;
var ProgrammationDetailsFromDisplay = /** @class */ (function () {
    function ProgrammationDetailsFromDisplay() {
    }
    return ProgrammationDetailsFromDisplay;
}());
exports.ProgrammationDetailsFromDisplay = ProgrammationDetailsFromDisplay;
var ProgramDate = /** @class */ (function () {
    function ProgramDate() {
    }
    return ProgramDate;
}());
exports.ProgramDate = ProgramDate;
var ProgramPeriod = /** @class */ (function () {
    function ProgramPeriod() {
    }
    return ProgramPeriod;
}());
exports.ProgramPeriod = ProgramPeriod;
//Objets en réception du service (réutilisé pour l'envoi)
var ProgrammationDetails = /** @class */ (function () {
    function ProgrammationDetails() {
    }
    return ProgrammationDetails;
}());
exports.ProgrammationDetails = ProgrammationDetails;
var Location = /** @class */ (function () {
    function Location() {
    }
    return Location;
}());
var Campaign = /** @class */ (function () {
    function Campaign() {
    }
    return Campaign;
}());
var Program = /** @class */ (function () {
    function Program() {
    }
    return Program;
}());
var Template = /** @class */ (function () {
    function Template() {
    }
    return Template;
}());
exports.Template = Template;
var TemplateContent = /** @class */ (function () {
    function TemplateContent() {
    }
    return TemplateContent;
}());
exports.TemplateContent = TemplateContent;
var RefPriority = /** @class */ (function () {
    function RefPriority() {
    }
    return RefPriority;
}());
exports.RefPriority = RefPriority;
//# sourceMappingURL=customerRouteProg.mapper.js.map