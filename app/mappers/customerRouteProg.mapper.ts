export class CustomerRouteProgMapper {

  /**************************************************************************/  
  /* Mapping du détails d'une programmation                                 */
  /**************************************************************************/  
  //Données à afficher
  public programmationDetailsToDisplay: ProgrammationDetailsToDisplay = new ProgrammationDetailsToDisplay();
  //Données reçues du serveur
  public programmationDetailsReceived: ProgrammationDetails = new ProgrammationDetails();

  public customerRouteProgToDisplayMapper() {
    this.programmationDetailsToDisplay.idProgramParent = this.programmationDetailsReceived[0].idProgramParent;  
    this.programmationDetailsToDisplay.messageName = this.programmationDetailsReceived[0].template.templateName;  
    this.programmationDetailsToDisplay.priority = this.programmationDetailsReceived[0].refPriority.libPriority;  
    if( this.programmationDetailsReceived[0].templateContent!=null) {
        this.programmationDetailsToDisplay.templateContentTxt = this.programmationDetailsReceived[0].templateContent.templateContentTxt;
    }

    //Détermination du choix type: Liste de dates ou Périodes
    if((this.programmationDetailsReceived[0].vevent == "")||(this.programmationDetailsReceived[0].vevent == null)) {
        this.programmationDetailsToDisplay.periodChecked = false;

        this.programmationDetailsToDisplay.dateList = new Array<ProgramDate>();
        this.programmationDetailsReceived[0].programs.forEach(element => {
            let programDate = new ProgramDate();
            programDate.dateEnd = element.dateEnd;
            programDate.dateStart = element.dateStart;
            this.programmationDetailsToDisplay.dateList.push(programDate);
        }); 
        
        this.programmationDetailsToDisplay.periodDetails = new ProgramPeriod();      
        this.programmationDetailsToDisplay.periodDetails.type="DAILY";
    }
    else {
        this.programmationDetailsToDisplay.periodChecked = true;
        this.programmationDetailsToDisplay.periodDetails = new ProgramPeriod();    
        //Décodage champ vevent
        let split:string[] = this.programmationDetailsReceived[0].vevent.split("\n");
        split.forEach(element => {
            let value:string[] = element.split(":");
            switch(value[0]) {
                case "DTSTART":
                    let dateHourStart: string[];
                    dateHourStart = value[1].split("T");
                    this.programmationDetailsToDisplay.periodDetails.dateStart = dateHourStart[0];
                    this.programmationDetailsToDisplay.periodDetails.hourStart = dateHourStart[1];
                    break;
                case "DTEND":                    
                    let dateHourEnd: string[];
                    dateHourEnd = value[1].split("T");
                    this.programmationDetailsToDisplay.periodDetails.dateEnd = dateHourEnd[0];
                    this.programmationDetailsToDisplay.periodDetails.hourEnd = dateHourEnd[1];
                    break;
                case "DURATION": 
                    let duration: string;                    
                    duration = value[1].slice(0,value[1].length-1);   
                    switch(value[1].slice(value[1].length-1)) {
                        case "D":
                            duration = duration + "j"
                            break;
                        case "W":
                            duration = duration + "sem"
                            break;
                        case "H":
                            duration = duration + "h"
                            break;
                    }               
                    this.programmationDetailsToDisplay.periodDetails.duration = duration;
                    break;
                case "RRULE":
                    let rules: string[];                             
                    rules = value[1].split(";");                      
                    let rule1: string[];                             
                    rule1 = rules[0].split("=");  
                    this.programmationDetailsToDisplay.periodDetails.type = rule1[1];
                    let rule2: string[];                             
                    rule2 = rules[1].split("=");  
                    this.programmationDetailsToDisplay.periodDetails.interval = rule2[1];
                    if(rule1[1]!="DAILY") {
                        let rule3: string[];                             
                        rule3 = rules[2].split("="); 
                        /*this.programmationDetailsToDisplay.periodDetails.byX = rule3[1];*/
                        let byX: string[];                             
                        byX = rule3[1].split(","); 
                        this.programmationDetailsToDisplay.periodDetails.mondayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.tuesdayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.wedenesdayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.thursdayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.fridayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.saturdayChecked = false;
                        this.programmationDetailsToDisplay.periodDetails.sundayChecked = false;
                        byX.forEach(element => {          
                            if(element=="MO") {     
                                this.programmationDetailsToDisplay.periodDetails.mondayChecked = true;
                            }      
                            else if(element=="TU") {     
                                this.programmationDetailsToDisplay.periodDetails.tuesdayChecked = true;
                            }    
                            else if(element=="WE") {     
                                this.programmationDetailsToDisplay.periodDetails.wedenesdayChecked = true;
                            }    
                            else if(element=="TH") {     
                                this.programmationDetailsToDisplay.periodDetails.thursdayChecked = true;
                            }    
                            else if(element=="FR") {     
                                this.programmationDetailsToDisplay.periodDetails.fridayChecked = true;
                            }    
                            else if(element=="SA") {     
                                this.programmationDetailsToDisplay.periodDetails.saturdayChecked = true;
                            }    
                            else if(element=="SU") {     
                                this.programmationDetailsToDisplay.periodDetails.sundayChecked = true;
                            }
                            else {
                                this.programmationDetailsToDisplay.periodDetails.the = element;
                            }
                        });
                    }
                    break;
                case "EXDATE;VALUE=DATE":                    
                    let exdate: string[];                             
                    exdate = value[1].split(","); 
                    this.programmationDetailsToDisplay.periodDetails.exception = "";
                    exdate.forEach(element => {          
                        if(element==exdate[0]) {     
                            this.programmationDetailsToDisplay.periodDetails.exception += element.slice(6) + "/" + element.slice(4,6) + "/" + element.slice(0,4);
                        }      
                        else {     
                            this.programmationDetailsToDisplay.periodDetails.exception += "," + element.slice(6) + "/" + element.slice(4,6) + "/" + element.slice(0,4);
                        }
                    });
                    break;
                default:
                    break;
            }
        }); 
    }
    
  }

  /**************************************************************************/
  /* Mapping du détails d'un parcours pour la création ou la modification   */
  /**************************************************************************/ 
  //Données de l'affichage
  public programmationDetailsFromDisplay: ProgrammationDetailsFromDisplay;
  //Données à envoyer au serveur    
  public programmationeDetailsToSend: ProgrammationDetails;  

  //Mapping du détails d'une programmation
  public customerRouteProgFromDisplayMapper(phase:string, templateList: Array<Template>, templateContentList: Array<TemplateContent>, refPriorityList: Array<RefPriority>) {
    this.programmationeDetailsToSend = new ProgrammationDetails();

    if(this.programmationDetailsReceived[0]!=undefined) {
        this.programmationeDetailsToSend.idProgramParent = this.programmationDetailsReceived[0].idProgramParent;
    }

    this.programmationeDetailsToSend.programs = new Array<Program>();
    if(this.programmationDetailsFromDisplay.periodChecked==false) {    
        for(let i=0;i<this.programmationDetailsFromDisplay.dateList.length;i++) {

            let date1: Date = new Date(this.programmationeDetailsToSend.dateStart);
            let date2: Date = new Date(this.programmationDetailsFromDisplay.dateList[i].dateStart);
            if((date1.getTime()>date2.getTime()) || this.programmationeDetailsToSend.dateStart==undefined){
                this.programmationeDetailsToSend.dateStart = this.programmationDetailsFromDisplay.dateList[i].dateStart; 
            }   

            let date3: Date = new Date(this.programmationeDetailsToSend.dateEnd);
            let date4: Date = new Date(this.programmationDetailsFromDisplay.dateList[i].dateEnd); 
                 
            if(date3.getTime()<date4.getTime() || this.programmationeDetailsToSend.dateEnd==undefined){
                this.programmationeDetailsToSend.dateEnd = this.programmationDetailsFromDisplay.dateList[i].dateEnd; 
            }

            let prog:Program = new Program();
            prog.access = 0;
            prog.dateStart = this.programmationDetailsFromDisplay.dateList[i].dateStart; 
            prog.dateEnd = this.programmationDetailsFromDisplay.dateList[i].dateEnd; 
            this.programmationeDetailsToSend.programs.push(prog);
        }
    }
    else {

    }

    this.programmationeDetailsToSend.campaign = new Campaign();
    this.programmationeDetailsToSend.campaign.idCampaign = 1;
    this.programmationeDetailsToSend.campaign.campaignName = "Hors campagne"
    this.programmationeDetailsToSend.location = new Location();
    this.programmationeDetailsToSend.location.idLocation = 1;
    //TO ADD: Partie programs
    this.programmationeDetailsToSend.template = new Template();
    this.programmationeDetailsToSend.template = templateList.find(element => element.templateName == this.programmationDetailsFromDisplay.messageName);
    this.programmationeDetailsToSend.refPriority = new RefPriority();
    this.programmationeDetailsToSend.refPriority = refPriorityList.find(element => element.libPriority == this.programmationDetailsFromDisplay.priority);
    this.programmationeDetailsToSend.templateContent = new TemplateContent();

    if(phase=="Sms") {
        this.programmationeDetailsToSend.templateContent = templateContentList.find(element => element.templateContentTxt == this.programmationDetailsFromDisplay.templateContentTxt);
    }
    else {
        this.programmationeDetailsToSend.templateContent = templateContentList.find(element => element.templateContentId == this.programmationeDetailsToSend.template.templateContent.templateContentId);   
    }
  }
}

//Objets pour l'affichage
export class ProgrammationDetailsToDisplay {
    idProgramParent: number;
  messageName: string;
  priority: string;
  dateList: Array<ProgramDate>;
  periodDetails: ProgramPeriod;
  periodChecked: boolean;
  templateContentTxt: string;
}

export class ProgrammationDetailsFromDisplay {
  messageName: string;
  priority: string;
  dateList: Array<ProgramDate>;
  periodDetails: ProgramPeriod;
  periodChecked: boolean;
  templateContentTxt: string;
}

export class ProgramDate {
  dateEnd: string;
  dateStart: string;
}

export class ProgramPeriod {
  dateEnd: string;
  dateStart: string;
  hourEnd: string;
  hourStart: string;
  duration: string;
  type: string;    
  interval:string;
  mondayChecked: boolean;
  tuesdayChecked: boolean;
  wedenesdayChecked: boolean;
  thursdayChecked: boolean;
  fridayChecked: boolean;
  saturdayChecked: boolean;
  sundayChecked: boolean;
  the: string;
  exception: string;
}

//Objets en réception du service (réutilisé pour l'envoi)
export class ProgrammationDetails {
  idProgramParent: number;
  acces: number;
  dateEnd: string;
  dateStart: string;
  location: Location;
  campaign: Campaign;
  programs: Array<Program>;
  template: Template;
  templateContent: TemplateContent;
  refPriority: RefPriority;
  vevent: string;
}

class Location {
    idLocation: number;
    label: string;
}

class Campaign {
    campaignName: string;
    dateEnd: string;
    dateStart: string;
    idCampaign: number;
}

class Program {
    access: number;
    dateEnd: string;
    dateStart: string;
    idProgram: string;    
}

export class Template {
    access: number;
    display: number;
    idTemplate: number;
    codePhase: string;
    templateContent: TemplateContent;
    templateName: string;
}

export class TemplateContent {
    templateContentId: number;
    templateContentName: string;
    templateContentShortTxt: string;
    templateContentTxt: string;
    idContractCustomer:number;
}

export class RefPriority {
    idPriority: number;
    libPriority: string;
}

