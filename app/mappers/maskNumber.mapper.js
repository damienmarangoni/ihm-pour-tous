"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MaskNumberMapper = /** @class */ (function () {
    function MaskNumberMapper() {
        /**************************************************************************/
        /* Mapping de la liste des numeros noirs pour l'affichage                 */
        /**************************************************************************/
        //Données pour l'affichage
        this.maskNumberListToDisplay = [[]];
        this._maskNumber = new MaskNumberToDisplay();
        this.maskNumbToSend = new MaskNumberToSend();
    }
    MaskNumberMapper.prototype.maskNumberListToDisplayMapper = function () {
        var _this = this;
        this.maskNumberListToDisplay = Array();
        //alert (this.maskNumberListDetails[0].description);
        this.maskNumberListDetails.forEach(function (details) {
            _this._maskNumber.description = details.maskDescription;
            _this._maskNumber.numeroGIS = details.maskMaskedNb;
            _this._maskNumber.numeroPublic = details.maskPublicNb;
            _this._maskNumber.messageSMS = details.maskMessage;
            _this.maskNumberListToDisplay.push([details.maskDescription,
                _this._maskNumber.numeroGIS,
                _this._maskNumber.numeroPublic,
                _this._maskNumber.messageSMS]);
            //alert (this.maskNumberListToDisplay[0][0]);  
        });
    };
    /********************************************************/
    /* Renvoie true si un numero GIS est déjà dans la liste */
    /********************************************************/
    MaskNumberMapper.prototype.isNumeroGISExist = function (numAchercher) {
        if (this.maskNumberListDetails.find(function (el) { return el.maskMaskedNb == numAchercher; }) != undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    return MaskNumberMapper;
}());
exports.MaskNumberMapper = MaskNumberMapper;
/**********************************************************/
/* Objets de destination pour l'affichage                 */
/**********************************************************/
var MaskNumberToDisplay = /** @class */ (function () {
    function MaskNumberToDisplay() {
    }
    return MaskNumberToDisplay;
}());
exports.MaskNumberToDisplay = MaskNumberToDisplay;
/**********************************************************/
/* Objets à envoyer                         */
/**********************************************************/
var MaskNumberToSend = /** @class */ (function () {
    function MaskNumberToSend() {
    }
    return MaskNumberToSend;
}());
exports.MaskNumberToSend = MaskNumberToSend;
/**********************************************************/
/* Objets en réception du service                         */
/**********************************************************/
var MaskNumberDetails = /** @class */ (function () {
    function MaskNumberDetails() {
    }
    return MaskNumberDetails;
}());
exports.MaskNumberDetails = MaskNumberDetails;
//# sourceMappingURL=maskNumber.mapper.js.map