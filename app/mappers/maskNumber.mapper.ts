export class MaskNumberMapper {

  /**************************************************************************/  
  /* Mapping de la liste des numeros noirs pour l'affichage                 */
  /**************************************************************************/  
  //Données pour l'affichage
  public maskNumberListToDisplay: Array<string[]> = [[]];
  //Données en provenance du serveur
  public maskNumberListDetails: Array<MaskNumberDetails>;
  
  private _maskNumber: MaskNumberToDisplay = new MaskNumberToDisplay();
  public maskNumbToSend: MaskNumberToSend = new MaskNumberToSend();


  public maskNumberListToDisplayMapper() {

    this.maskNumberListToDisplay = Array<string[]>();
    //alert (this.maskNumberListDetails[0].description);
    this.maskNumberListDetails.forEach((details) => {
        
        this._maskNumber.description = details.maskDescription;
        this._maskNumber.numeroGIS = details.maskMaskedNb;
        this._maskNumber.numeroPublic = details.maskPublicNb;
        this._maskNumber.messageSMS = details.maskMessage;
       
        this.maskNumberListToDisplay.push([ details.maskDescription,
                                      this._maskNumber.numeroGIS,
                                      this._maskNumber.numeroPublic,
                                      this._maskNumber.messageSMS]); 
        //alert (this.maskNumberListToDisplay[0][0]);  
    });
  }

  /********************************************************/
  /* Renvoie true si un numero GIS est déjà dans la liste */
  /********************************************************/
  public isNumeroGISExist( numAchercher:string): boolean {
    if (this.maskNumberListDetails.find(el => el.maskMaskedNb == numAchercher)!=undefined) {
      return true;
    }
    else {
      return false;
    }
  }



}



/**********************************************************/
/* Objets de destination pour l'affichage                 */
/**********************************************************/
export class MaskNumberToDisplay {
  description: string;
  numeroGIS: string;
  numeroPublic: string;
  messageSMS: string;
  
}

/**********************************************************/
/* Objets à envoyer                         */
/**********************************************************/
export class MaskNumberToSend {
  maskDescription: string;
  maskMaskedNb: string;
  maskPublicNb: string;
  maskMessage: string; 
}

/**********************************************************/
/* Objets en réception du service                         */
/**********************************************************/
export class MaskNumberDetails {
  maskDescription: string;
  maskMaskedNb: string;
  maskPublicNb: string;
  maskMessage: string;
  
}
