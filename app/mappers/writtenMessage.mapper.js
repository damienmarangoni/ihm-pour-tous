"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WrittenMessageMapper = /** @class */ (function () {
    function WrittenMessageMapper() {
        this.templateDetailsReceived = new Template();
        this.templateDetailsToSend = new TemplateToSend();
        this.templateDetailsToDisplay = new TemplateDetailsToDisplay();
    }
    WrittenMessageMapper.prototype.templateToDisplayMapper = function () {
        var _this = this;
        this.templateDetailsToDisplay.elementOrder = [];
        this.templateDetailsToDisplay.elementWithPriority = [];
        this.templateDetailsToDisplay.elementWithoutPriority = [];
        var elToDisplay = [[]];
        var templateElement = [];
        //Au cas ou on ait un % au lieu d'un $
        this.templateDetailsReceived.templateTemplate = this.templateDetailsReceived.templateTemplate.replace(/%/g, "$");
        templateElement = this.templateDetailsReceived.templateTemplate.split('$');
        templateElement.forEach(function (el) {
            //Suppression d'un éventuel espace
            while (el.substring(el.length - 1, el.length) == ' ') {
                el = el.substring(0, el.length - 1);
            }
            //Traitement des sauts de ligne
            var carriageReturn = false;
            while (el.substring(el.length - 1, el.length) == '\n') {
                el = el.substring(0, el.length - 1);
                carriageReturn = true;
            }
            if (el.split(']').length > 1) {
                //Détermination liste des élément avec priorité
                var tmp = [];
                //Traitement des éléments composés (avec ())
                if (el.split('[')[0].split(')').length > 1) {
                    _this.templateDetailsToDisplay.elementOrder.push(el.split('(')[0]);
                    tmp.push(el.split('(')[0]);
                }
                else {
                    _this.templateDetailsToDisplay.elementOrder.push(el.split('[')[0]);
                    tmp.push(el.split('[')[0]);
                }
                tmp.push(el.split('[')[1].substring(0, el.split('[')[1].length - 1));
                elToDisplay.push(tmp);
            }
            else {
                if (el != "") {
                    //Traitement des éléments composés (avec ())
                    _this.templateDetailsToDisplay.elementOrder.push(el);
                    _this.templateDetailsToDisplay.elementWithoutPriority.push(el);
                }
            }
            if (carriageReturn == true) {
                _this.templateDetailsToDisplay.elementOrder.push("\n");
            }
        });
        var _loop_1 = function (i) {
            elToDisplay.forEach(function (el) {
                if (parseInt(el[1]) == i) {
                    _this.templateDetailsToDisplay.elementWithPriority.push(el);
                }
            });
        };
        for (var i = 0; i <= elToDisplay.length + 1; i++) {
            _loop_1(i);
        }
    };
    return WrittenMessageMapper;
}());
exports.WrittenMessageMapper = WrittenMessageMapper;
/*******************************************/
/* Objets pour l'affichage                 */
/*******************************************/
var TemplateDetailsToDisplay = /** @class */ (function () {
    function TemplateDetailsToDisplay() {
    }
    return TemplateDetailsToDisplay;
}());
exports.TemplateDetailsToDisplay = TemplateDetailsToDisplay;
/*******************************************/
/* Objets à envoyer                        */
/*******************************************/
var TemplateToSend = /** @class */ (function () {
    function TemplateToSend() {
    }
    return TemplateToSend;
}());
exports.TemplateToSend = TemplateToSend;
/*******************************************/
/* Objets en réception du service          */
/*******************************************/
var Template = /** @class */ (function () {
    function Template() {
    }
    return Template;
}());
exports.Template = Template;
var TemplateContent = /** @class */ (function () {
    function TemplateContent() {
    }
    return TemplateContent;
}());
exports.TemplateContent = TemplateContent;
var TemplateType = /** @class */ (function () {
    function TemplateType() {
    }
    return TemplateType;
}());
exports.TemplateType = TemplateType;
var Format = /** @class */ (function () {
    function Format() {
    }
    return Format;
}());
exports.Format = Format;
var RefTemplateItem = /** @class */ (function () {
    function RefTemplateItem() {
    }
    return RefTemplateItem;
}());
exports.RefTemplateItem = RefTemplateItem;
var ContractCustomer = /** @class */ (function () {
    function ContractCustomer() {
    }
    return ContractCustomer;
}());
exports.ContractCustomer = ContractCustomer;
//# sourceMappingURL=writtenMessage.mapper.js.map