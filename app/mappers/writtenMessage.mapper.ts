export class WrittenMessageMapper {

  public templateDetailsReceived: Template = new Template();
  public templateDetailsToSend: TemplateToSend = new TemplateToSend();
  public templateDetailsToDisplay: TemplateDetailsToDisplay = new TemplateDetailsToDisplay();

  public templateToDisplayMapper() {

        this.templateDetailsToDisplay.elementOrder = [];
        this.templateDetailsToDisplay.elementWithPriority = [];
        this.templateDetailsToDisplay.elementWithoutPriority = [];
              
        let elToDisplay: string[][]=[[]];
        let templateElement:string[] = [];

        //Au cas ou on ait un % au lieu d'un $
        this.templateDetailsReceived.templateTemplate = this.templateDetailsReceived.templateTemplate.replace(/%/g, "$");

        templateElement = this.templateDetailsReceived.templateTemplate.split('$');
        templateElement.forEach(el => {

            //Suppression d'un éventuel espace
            while(el.substring(el.length-1,el.length)==' ') {

                el = el.substring(0,el.length-1);
            }

            //Traitement des sauts de ligne
            let carriageReturn:boolean=false;
            while(el.substring(el.length-1,el.length)=='\n') {

                el = el.substring(0,el.length-1);
                carriageReturn = true;
            }

            if(el.split(']').length>1){

                //Détermination liste des élément avec priorité
                let tmp:string[] = [];

                //Traitement des éléments composés (avec ())
                if(el.split('[')[0].split(')').length>1) {   

                    this.templateDetailsToDisplay.elementOrder.push(el.split('(')[0]);
                    tmp.push(el.split('(')[0]);
                } 
                else {  

                    this.templateDetailsToDisplay.elementOrder.push(el.split('[')[0]);
                    tmp.push(el.split('[')[0]);
                }               
                
                tmp.push(el.split('[')[1].substring(0,el.split('[')[1].length-1));
                elToDisplay.push(tmp);
            }
            else {
                if(el!="") {
                
                    //Traitement des éléments composés (avec ())
                    this.templateDetailsToDisplay.elementOrder.push(el);
                    this.templateDetailsToDisplay.elementWithoutPriority.push(el);
                }
            }

            if(carriageReturn==true) {

                this.templateDetailsToDisplay.elementOrder.push("\n");
            }
        });
                
        for(let i=0;i<=elToDisplay.length+1;i++) {

            elToDisplay.forEach(el => {   
                             
                if(parseInt(el[1])==i) {

                    this.templateDetailsToDisplay.elementWithPriority.push(el);
                }
            });
        }
  }  
}

/*******************************************/
/* Objets pour l'affichage                 */
/*******************************************/
export class TemplateDetailsToDisplay {
    elementOrder:string[];
    elementWithPriority:string[][];
    elementWithoutPriority:string[];
}

/*******************************************/
/* Objets à envoyer                        */
/*******************************************/
export class TemplateToSend {    
    access: number;
    display: number;
    templateTemplate: string; 
    format: Format;
    codePhase: string;
    templateContent: TemplateContent;
    templateName: string;
    templateType: TemplateType;
}

/*******************************************/
/* Objets en réception du service          */
/*******************************************/
export class Template {    
    access: number;
    display: number;
    templateSize: number;
    idTemplate: string;
    templateTemplate: string; 
    format: Format;
    codePhase: string;
    templateContent: TemplateContent;
    templateName: string;
    templateType: TemplateType;
}

export class TemplateContent {
    templateContentId: string;
    templateContentName: string;
    templateContentTxt: string;
    templateContentShortTxt: string;
    contractCustomerId: number;
    contractCustomerLabel: string;
    channelId: number;
    channelLib: string;

}

export class TemplateType {
    templateTypeId: number;
    templateTypeDescription: string;

}

export class Format {
    formatDescription: string;
    formatId: string;
}

export class RefTemplateItem {
    id:number;
    keyword:string;
    infoadd_id:string;
    description:string;
    content_sent:number;
    priorisable:number;
    enabled:number;
    example:string;
}

export class ContractCustomer {
    contractCustomerId: number;
    label: string;

}

