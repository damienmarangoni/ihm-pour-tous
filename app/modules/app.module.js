"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_routing_1 = require("../routing/app.routing");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var ng2_popup_1 = require("ng2-popup");
/*import {GoogleChart} from 'angular2-google-chart/directives/angular2-google-chart.directive';*/
var mydatepicker_1 = require("mydatepicker");
var ng2_dragula_1 = require("ng2-dragula");
var app_component_1 = require("../components/app.component");
var customerRoute_component_1 = require("../components/customerRoute/customerRoute.component");
var audioPlayer_component_1 = require("../components/audio/audioPlayer.component");
var administration_component_1 = require("../components/administration/administration.component");
var dialogSimulator_component_1 = require("../components/mediaSimulators/dialogSimulator.component");
var customerRoute_form_1 = require("../forms/customerRoute/customerRoute.form");
var customerProg_form_1 = require("../forms/customerRoute/customerProg.form");
var maskNumber_component_1 = require("../components/maskNumber/maskNumber.component");
var maskNumber_form_1 = require("../forms/maskNumber/maskNumber.form");
var writtenMessageModel_component_1 = require("../components/writtenMessage/writtenMessageModel.component");
var writtenMessageText_component_1 = require("../components/writtenMessage/writtenMessageText.component");
var writtenMessageModel_form_1 = require("../forms/writtenMessage/writtenMessageModel.form");
var writtenMessageText_form_1 = require("../forms/writtenMessage/writtenMessageText.form");
var customAudioMessage_component_1 = require("../components/audioMessage/customAudioMessage.component");
var defaultAudioMessage_component_1 = require("../components/audioMessage/defaultAudioMessage.component");
var defaultAudioMessage_form_1 = require("../forms/audioMessage/defaultAudioMessage.form");
var customAudioMessage_form_1 = require("../forms/audioMessage/customAudioMessage.form");
var smsSimulator_component_1 = require("../components/mediaSimulators/smsSimulator.component");
var environment_1 = require("../conf/environment");
/*export function loadEnv(env: Environment) {
    
    env.load();
    alert("|" )
}*/
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, router_1.RouterModule.forRoot(app_routing_1.appRoutes, { useHash: false }), http_1.HttpModule,
                ng2_bs3_modal_1.Ng2Bs3ModalModule, ng2_popup_1.Ng2PopupModule, mydatepicker_1.MyDatePickerModule, ng2_dragula_1.DragulaModule],
            declarations: [app_component_1.AppComponent, app_component_1.ProgrammingComponent, app_component_1.HomeComponent, administration_component_1.AdministrationComponent,
                dialogSimulator_component_1.DialogSimulatorComponent, smsSimulator_component_1.SmsSimulatorComponent, audioPlayer_component_1.AudioPlayerComponent, customerRoute_component_1.CustomerRouteComponent, customerRoute_form_1.modifyRouteForm, customerRoute_form_1.deleteRouteForm, customerRoute_form_1.addRouteForm, customerRoute_form_1.consultRouteForm, customerProg_form_1.consultRouteProgForm,
                maskNumber_component_1.MaskNumberComponent, maskNumber_form_1.MaskNumberForm, maskNumber_form_1.DeleteMaskNumberForm,
                writtenMessageModel_component_1.WrittenMessageModelComponent, writtenMessageModel_form_1.WrittenMessageModelForm, writtenMessageText_component_1.WrittenMessageTextComponent, writtenMessageText_form_1.WrittenMessageTextForm, writtenMessageModel_form_1.DeleteMessageModelForm, writtenMessageText_form_1.DeleteWrittMessTextForm,
                writtenMessageModel_form_1.DeleteMessageModelForm, customAudioMessage_component_1.CustomAudioMessageComponent, defaultAudioMessage_component_1.DefaultAudioMessageComponent, defaultAudioMessage_form_1.DefaultAudioMessageForm, customAudioMessage_form_1.CustomAudioMessageForm, customAudioMessage_form_1.DeleteCustomAudioMessageForm],
            providers: [environment_1.Environment //, 
                //{provide: APP_BASE_HREF, useValue : "/" }
            ],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [audioPlayer_component_1.AudioPlayerComponent, customerRoute_form_1.modifyRouteForm, customerRoute_form_1.deleteRouteForm, customerRoute_form_1.addRouteForm, customerRoute_form_1.consultRouteForm, customerProg_form_1.consultRouteProgForm]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map