import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Inject, Injectable } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { appRoutes } from '../routing/app.routing';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Ng2PopupModule  } from 'ng2-popup';
/*import {GoogleChart} from 'angular2-google-chart/directives/angular2-google-chart.directive';*/
import { MyDatePickerModule } from 'mydatepicker';
import { DragulaModule  } from 'ng2-dragula';

import { AppComponent, ProgrammingComponent, HomeComponent} from '../components/app.component';
import { CustomerRouteComponent } from '../components/customerRoute/customerRoute.component';
import { AudioPlayerComponent } from '../components/audio/audioPlayer.component';
import { AdministrationComponent } from '../components/administration/administration.component';
import { DialogSimulatorComponent } from '../components/mediaSimulators/dialogSimulator.component';
import { modifyRouteForm, deleteRouteForm, addRouteForm, consultRouteForm} from '../forms/customerRoute/customerRoute.form';
import { consultRouteProgForm} from '../forms/customerRoute/customerProg.form';
import { MaskNumberComponent } from '../components/maskNumber/maskNumber.component';
import { MaskNumberForm, DeleteMaskNumberForm } from '../forms/maskNumber/maskNumber.form';
import { WrittenMessageModelComponent } from '../components/writtenMessage/writtenMessageModel.component';
import { WrittenMessageTextComponent } from '../components/writtenMessage/writtenMessageText.component';
import { WrittenMessageModelForm, DeleteMessageModelForm } from '../forms/writtenMessage/writtenMessageModel.form';
import { WrittenMessageTextForm, DeleteWrittMessTextForm } from '../forms/writtenMessage/writtenMessageText.form';
import { CustomAudioMessageComponent } from '../components/audioMessage/customAudioMessage.component';
import { DefaultAudioMessageComponent } from '../components/audioMessage/defaultAudioMessage.component';
import { DefaultAudioMessageForm } from '../forms/audioMessage/defaultAudioMessage.form';
import { CustomAudioMessageForm, DeleteCustomAudioMessageForm } from '../forms/audioMessage/customAudioMessage.form';
import { SmsSimulatorComponent } from '../components/mediaSimulators/smsSimulator.component';
import { Environment } from '../conf/environment';


// pour les Urls avec un #
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import {APP_BASE_HREF} from '@angular/common';

/*export function loadEnv(env: Environment) {
    
    env.load();
    alert("|" )
}*/

@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule.forRoot(appRoutes, {useHash: false}), HttpModule, 
        Ng2Bs3ModalModule, Ng2PopupModule, MyDatePickerModule, DragulaModule ],
    declarations: [AppComponent, ProgrammingComponent, HomeComponent, AdministrationComponent, 
    DialogSimulatorComponent, SmsSimulatorComponent, AudioPlayerComponent, CustomerRouteComponent, modifyRouteForm, deleteRouteForm, addRouteForm, consultRouteForm, consultRouteProgForm,
    MaskNumberComponent, MaskNumberForm, DeleteMaskNumberForm,
    WrittenMessageModelComponent, WrittenMessageModelForm, WrittenMessageTextComponent, WrittenMessageTextForm, DeleteMessageModelForm, DeleteWrittMessTextForm, 
    DeleteMessageModelForm, CustomAudioMessageComponent, DefaultAudioMessageComponent, DefaultAudioMessageForm, CustomAudioMessageForm, DeleteCustomAudioMessageForm], 
    providers: [Environment//, 
    //{provide: APP_BASE_HREF, useValue : "/" }
    ],
    bootstrap:    [ AppComponent ],
    entryComponents: [AudioPlayerComponent, modifyRouteForm, deleteRouteForm, addRouteForm, consultRouteForm, consultRouteProgForm]
})
export class AppModule { 


/*    constructor(@Inject(Environment) private _env: Environment) {
         
         this._env.load();
    }*/
}
