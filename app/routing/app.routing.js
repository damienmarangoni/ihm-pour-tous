"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_component_1 = require("../components/app.component");
var customerRoute_component_1 = require("../components/customerRoute/customerRoute.component");
var dialogSimulator_component_1 = require("../components/mediaSimulators/dialogSimulator.component");
var smsSimulator_component_1 = require("../components/mediaSimulators/smsSimulator.component");
var writtenMessageModel_component_1 = require("../components/writtenMessage/writtenMessageModel.component");
var writtenMessageText_component_1 = require("../components/writtenMessage/writtenMessageText.component");
var maskNumber_component_1 = require("../components/maskNumber/maskNumber.component");
var customAudioMessage_component_1 = require("../components/audioMessage/customAudioMessage.component");
var defaultAudioMessage_component_1 = require("../components/audioMessage/defaultAudioMessage.component");
exports.appRoutes = [
    /*    { path: 'customerRoute', component: CustomerRouteComponent },*/
    { path: '', component: app_component_1.HomeComponent },
    { path: 'customer', component: customerRoute_component_1.CustomerRouteComponent },
    { path: 'programming', component: app_component_1.ProgrammingComponent },
    { path: 'dialogSimulator', component: dialogSimulator_component_1.DialogSimulatorComponent },
    { path: 'writtenMessageModel', component: writtenMessageModel_component_1.WrittenMessageModelComponent },
    { path: 'writtenMessageText', component: writtenMessageText_component_1.WrittenMessageTextComponent },
    { path: 'masks', component: maskNumber_component_1.MaskNumberComponent },
    { path: 'customAudioMessage', component: customAudioMessage_component_1.CustomAudioMessageComponent },
    { path: 'defaultAudioMessage', component: defaultAudioMessage_component_1.DefaultAudioMessageComponent },
    { path: 'smsSimulator', component: smsSimulator_component_1.SmsSimulatorComponent },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', component: app_component_1.HomeComponent }
];
//# sourceMappingURL=app.routing.js.map