import { Routes } from '@angular/router';
import { RoutesRecognized } from '@angular/router';

import { AppComponent,  ProgrammingComponent, HomeComponent } from '../components/app.component';
import { CustomerRouteComponent } from '../components/customerRoute/customerRoute.component';
import { DialogSimulatorComponent } from '../components/mediaSimulators/dialogSimulator.component';
import { SmsSimulatorComponent } from '../components/mediaSimulators/smsSimulator.component';
import { WrittenMessageModelComponent } from '../components/writtenMessage/writtenMessageModel.component';
import { WrittenMessageTextComponent } from '../components/writtenMessage/writtenMessageText.component';
import { MaskNumberComponent } from '../components/maskNumber/maskNumber.component';
import { CustomAudioMessageComponent } from '../components/audioMessage/customAudioMessage.component';
import { DefaultAudioMessageComponent } from '../components/audioMessage/defaultAudioMessage.component';

import { AudioPlayerComponent } from '../components/audio/audioPlayer.component'

export const appRoutes: Routes = [
/*    { path: 'customerRoute', component: CustomerRouteComponent },*/
    { path: '', component: HomeComponent }, // 'home', left blank so it's default page
    { path: 'customer', component: CustomerRouteComponent },
    { path: 'programming', component: ProgrammingComponent },
    { path: 'dialogSimulator', component: DialogSimulatorComponent },
    { path: 'writtenMessageModel', component: WrittenMessageModelComponent },
    { path: 'writtenMessageText', component: WrittenMessageTextComponent },
    { path: 'masks', component: MaskNumberComponent },
    { path: 'customAudioMessage', component: CustomAudioMessageComponent },
    { path: 'defaultAudioMessage', component: DefaultAudioMessageComponent },
    { path: 'smsSimulator', component: SmsSimulatorComponent },
    { path: '', redirectTo: '', pathMatch: 'full' },
    { path: '**', component: HomeComponent }
];


