"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var environment_1 = require("../../conf/environment");
var AudioInfosService = /** @class */ (function () {
    function AudioInfosService(_proxy, _env) {
        this._proxy = _proxy;
        this._env = _env;
    }
    //private _env:Environment;
    AudioInfosService.prototype.getAudioFile = function (file) {
        var url = this._env.baseUrl + "audioFile";
        var params = new http_1.URLSearchParams();
        params.set('fileName', file);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
    };
    AudioInfosService.prototype.getDefAudioFile = function (file) {
        var url = this._env.baseUrl + "audioDefaultFile";
        var params = new http_1.URLSearchParams();
        params.set('fileName', file);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
    };
    AudioInfosService.prototype.updateAudioFile = function (file, body) {
        var url = this._env.baseUrl + "updateAudioFile/" + file;
        return this._proxy.post(url, body);
    };
    AudioInfosService.prototype.updateDefaultAudioFile = function (file, body) {
        var url = this._env.baseUrl + "updateDefaultAudioFile/" + file;
        return this._proxy.post(url, body);
    };
    AudioInfosService.prototype.isAudioFileA8k = function (body) {
        var url = this._env.baseUrl + "isAudioFileA8k";
        return this._proxy.post(url, body);
    };
    AudioInfosService = __decorate([
        __param(0, core_1.Inject(http_1.Http)), __param(1, core_1.Inject(environment_1.Environment)),
        __metadata("design:paramtypes", [http_1.Http, environment_1.Environment])
    ], AudioInfosService);
    return AudioInfosService;
}());
exports.AudioInfosService = AudioInfosService;
//# sourceMappingURL=audio.service.js.map