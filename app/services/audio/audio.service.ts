﻿import { AdministrationComponent } from '../../components/administration/administration.component';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {Environment} from '../../conf/environment'

export class AudioInfosService {
    //private _env:Environment;

    public getAudioFile(file:string): Observable<Response>  {
        let url = this._env.baseUrl + "audioFile";
        let params = new URLSearchParams();
        params.set('fileName', file);  
        let options = new RequestOptions({search: params});
        return this._proxy.get(url, options);
    }

    public getDefAudioFile(file:string): Observable<Response>  {
        let url = this._env.baseUrl + "audioDefaultFile";
        let params = new URLSearchParams();
        params.set('fileName', file);  
        let options = new RequestOptions({search: params});
        return this._proxy.get(url, options);
    }

    public updateAudioFile(file: string, body: any): Observable<Response> {
        let url = this._env.baseUrl + "updateAudioFile/" + file;
        return this._proxy.post(url, body);
    } 

    public updateDefaultAudioFile(file: string, body: any): Observable<Response> {
        let url = this._env.baseUrl + "updateDefaultAudioFile/" + file;
        return this._proxy.post(url, body);
    } 

    public isAudioFileA8k(body: any): Observable<Response> {
        let url = this._env.baseUrl + "isAudioFileA8k";
        return this._proxy.post(url, body);
    } 

    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }

}