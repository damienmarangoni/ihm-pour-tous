"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var environment_1 = require("../../conf/environment");
var AudioMessageService = /** @class */ (function () {
    function AudioMessageService(_proxy, _env) {
        this._proxy = _proxy;
        this._env = _env;
    }
    //private _env:Environment;
    // Liste des messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des messages audios par défaut
    AudioMessageService.prototype.getRefDefAudioTrackList = function () {
        var url = this._env.baseUrl + "refDefAudioTracks";
        return this._proxy.get(url);
    };
    // Liste des phases liées aux messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des phases liées aux messages audios par défaut
    AudioMessageService.prototype.getRefDefAudioTrackPhaseList = function () {
        var url = this._env.baseUrl + "refAudioTrackPhases";
        return this._proxy.get(url);
    };
    // Liste des services liées aux messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des services liées aux messages audios par défaut
    AudioMessageService.prototype.getRefDefAudioTrackServiceList = function () {
        var url = this._env.baseUrl + "refAudioTrackServices";
        return this._proxy.get(url);
    };
    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    AudioMessageService.prototype.getMessageModelList = function (codePhase) {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        var url = this._env.baseUrl + "templates";
        var params = new http_1.URLSearchParams();
        params.set('search', "codePhase:" + codePhase);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    };
    // Mise à jour du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    AudioMessageService.prototype.updateAudioMessageModel = function (body, id) {
        var url = this._env.baseUrl + "modifyAudioTemplate/" + id;
        return this._proxy.put(url, body);
    };
    // Mise à jour du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    AudioMessageService.prototype.createAudioMessageModel = function (body) {
        var url = this._env.baseUrl + "createTemplate/1";
        return this._proxy.post(url, body);
    };
    // Suppression du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    // Retours :
    //     Statut du transfert	
    AudioMessageService.prototype.deleteAudioMessageModel = function (id) {
        var url = this._env.baseUrl + "deleteTemplate/" + id;
        var options = new http_1.RequestOptions({});
        return this._proxy.delete(url, options);
    };
    // Modèle de message sélectionné
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    AudioMessageService.prototype.getAudioMessageModel = function (id) {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        var url = this._env.baseUrl + "templateOnIdTemplate";
        var params = new http_1.URLSearchParams();
        params.set('idTemplate', id);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    };
    // Présence du fichier audio sur le serveur
    // Paramètres :
    //     - FILE: Nom du fichier audio
    // Retours :
    //     Un booléen signifiant la présence d'un fichier audio sur le serveur
    AudioMessageService.prototype.getIsAudioFilePresent = function (file) {
        var url = this._env.baseUrl + "isAudioFilePresent";
        var params = new http_1.URLSearchParams();
        params.set('fileName', file);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
    };
    AudioMessageService = __decorate([
        __param(0, core_1.Inject(http_1.Http)), __param(1, core_1.Inject(environment_1.Environment)),
        __metadata("design:paramtypes", [http_1.Http, environment_1.Environment])
    ], AudioMessageService);
    return AudioMessageService;
}());
exports.AudioMessageService = AudioMessageService;
//# sourceMappingURL=audioMessage.service.js.map