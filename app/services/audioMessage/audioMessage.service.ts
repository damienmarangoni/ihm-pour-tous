import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {DatePipe} from '@angular/common'
import {Environment} from '../../conf/environment'

export class AudioMessageService {

    //private _env:Environment;
    
    // Liste des messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des messages audios par défaut
    public getRefDefAudioTrackList(): Observable<Response> {

        let url = this._env.baseUrl + "refDefAudioTracks";
        return this._proxy.get(url);
    } 
    
    // Liste des phases liées aux messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des phases liées aux messages audios par défaut
    public getRefDefAudioTrackPhaseList(): Observable<Response> {

        let url = this._env.baseUrl + "refAudioTrackPhases";
        return this._proxy.get(url);
    } 
    
    // Liste des services liées aux messages audios par défaut
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste des services liées aux messages audios par défaut
    public getRefDefAudioTrackServiceList(): Observable<Response> {

        let url = this._env.baseUrl + "refAudioTrackServices";
        return this._proxy.get(url);
    } 
        // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getMessageModelList(codePhase: string): Observable<Response> {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        let url = this._env.baseUrl + "templates";
        let params = new URLSearchParams();
        params.set('search', "codePhase:" + codePhase);          
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    } 

    // Mise à jour du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    public updateAudioMessageModel(body: any, id:string): Observable<Response> {
        let url = this._env.baseUrl + "modifyAudioTemplate/" + id;
        return this._proxy.put(url, body);
    }    

    // Mise à jour du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    public createAudioMessageModel(body: any): Observable<Response> {
        let url = this._env.baseUrl + "createTemplate/1";
        return this._proxy.post(url, body);
    } 

    // Suppression du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    // Retours :
    //     Statut du transfert	
	public deleteAudioMessageModel(id:string): Observable<Response> {
        let url = this._env.baseUrl + "deleteTemplate/" + id;
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    // Modèle de message sélectionné
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getAudioMessageModel(id: string): Observable<Response> {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        let url = this._env.baseUrl + "templateOnIdTemplate";
        let params = new URLSearchParams();
        params.set('idTemplate', id);          
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    } 
    
    // Présence du fichier audio sur le serveur
    // Paramètres :
    //     - FILE: Nom du fichier audio
    // Retours :
    //     Un booléen signifiant la présence d'un fichier audio sur le serveur
    public getIsAudioFilePresent(file:string): Observable<Response> {

        let url = this._env.baseUrl + "isAudioFilePresent";
        let params = new URLSearchParams();
        params.set('fileName', file);          
        let options = new RequestOptions({search: params});
        return this._proxy.get(url, options);
    } 


    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }
}