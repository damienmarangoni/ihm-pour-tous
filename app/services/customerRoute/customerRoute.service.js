"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var environment_1 = require("../../conf/environment");
var CustomerRouteService = /** @class */ (function () {
    function CustomerRouteService(_proxy, _env) {
        this._proxy = _proxy;
        this._env = _env;
    }
    /*private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";*/
    //private _env:Environment;
    // Liste des Parcours client et de leurs Programmations    
    // Paramètres :
    //     - STARTDATE: Date de début de période de validité
    //     - ENDDATE: Date de fin de période de validité
    // Retours :
    //     Une liste de Parcours client avec le nom de leurs
    //     programmations associées
    CustomerRouteService.prototype.getCustomerRouteList = function (startDate, endDate) {
        //let url = this._baseUrl + "views/customerRoute/customerRouteList.json";customerRoutes
        var url = this._env.baseUrl + "customerRoutes";
        /*let url = this._baseUrl + "customerRoutesOnDates";
        let params = new URLSearchParams();
        let datePipe = new DatePipe("fr-FR");
        params.set('startDate', datePipe.transform(startDate,"yyyy-MM-dd"));
        params.set('endDate', datePipe.transform(endDate,"yyyy-MM-dd"));
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);*/
        return this._proxy.get(url);
    };
    // Liste des Parcours client et de leurs Programmations    
    // Paramètres :
    //     - STARTDATE: Date de début de période de validité
    //     - ENDDATE: Date de fin de période de validité
    // Retours :
    //     Une liste de Parcours client avec le nom de leurs
    //     programmations associées
    CustomerRouteService.prototype.getCustomerRouteListOnDates = function (startDate, endDate) {
        var url = this._env.baseUrl + "customerRoutesOnDates";
        var params = new http_1.URLSearchParams();
        var datePipe = new common_1.DatePipe("fr-FR");
        params.set('startDate', datePipe.transform(startDate, "yyyy-MM-dd"));
        params.set('endDate', datePipe.transform(endDate, "yyyy-MM-dd"));
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
    };
    // Détails du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Les détails d'un Parcours (DU, communautés, segments) 
    //     sans ses programmations associées
    CustomerRouteService.prototype.getCustomerRouteDetails = function (id) {
        //let url = this._baseUrl + "views/customerRoute/customerRouteDetails.json";
        var url = this._env.baseUrl + "customerRoutes";
        var params = new http_1.URLSearchParams();
        params.set('search', "customerQualificationId:" + id);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    };
    // Mise à jour détails du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - DETAILS: Détails du Parcours client
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.updateCustomerRouteDetails = function (body, id) {
        var url = this._env.baseUrl + "createQualExpr/" + id;
        return this._proxy.post(url, body);
    };
    // Suppression du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.deleteCustomerRouteDetails = function (id) {
        var url = this._env.baseUrl + "deleteCustomerRoute/" + id;
        var options = new http_1.RequestOptions({});
        return this._proxy.delete(url, options);
    };
    // Création du Parcours client sans ses programmations
    // Paramètres :
    //     - DETAILS: Détails du Parcours client
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.createCustomerRouteDetails = function (body) {
        var url = this._env.baseUrl + "customerRoute";
        return this._proxy.post(url, body);
    };
    // Détails du Parcours client et de ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Les détails d'un Parcours client (DU, communautés, segments) 
    //     et de ses programmations associées
    CustomerRouteService.prototype.getCustomerRouteFullDetail = function (id) {
        var url = this._env.baseUrl + "programParentsOnCustQualifId";
        var params = new http_1.URLSearchParams();
        params.set('customerQualificationId', id.toString());
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
    };
    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - PHASENAME : Programmation de la phase correspondante
    // Retours :
    //     Les détails de la programmation d'une phase d'un   
    //     Parcours client 
    CustomerRouteService.prototype.getCustomerRouteProg = function (id, codePhase) {
        //let url = this._baseUrl + "views/customerRoute/customerRouteProg.json";
        var url = this._env.baseUrl + "programParentsOnCodePhase";
        var params = new http_1.URLSearchParams();
        params.set('customerQualificationId', id.toString());
        params.set('codePhase', codePhase);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    };
    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.updateCustomerRouteProg = function () {
        var url = this._env.baseUrl + "updateRouteProg";
        return this._proxy.post(url, new String());
    };
    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.createCustomerRouteProg = function (id, body) {
        var url = this._env.baseUrl + "createProgParent/" + id;
        return this._proxy.post(url, body);
    };
    // Suppression du Parcours client sans ses programmations<
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.deleteCustomerRouteProg = function (id) {
        var url = this._env.baseUrl + "deleteProgParent/" + id;
        var options = new http_1.RequestOptions({});
        return this._proxy.delete(url, options);
    };
    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    CustomerRouteService.prototype.modifyCustomerRouteProg = function (id, body) {
        var url = this._env.baseUrl + "modifyProgParent/" + id;
        return this._proxy.post(url, body);
    };
    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    CustomerRouteService.prototype.getPhaseMessageModelList = function (codePhase) {
        //let url = this._baseUrl + "views/customerRoute/phaseMessagesModelList.json";
        var url = this._env.baseUrl + "templates";
        var params = new http_1.URLSearchParams();
        params.set('search', "codePhase:" + codePhase);
        var options = new http_1.RequestOptions({ search: params });
        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    };
    // Priorités des programmations
    // Retours :
    //     Une liste de priorités
    CustomerRouteService.prototype.getPrioritiesList = function () {
        var url = this._env.baseUrl + "refPriority";
        return this._proxy.get(url);
    };
    // Contenu model des programmations
    // Retours :
    //     Une liste de modèle de message
    CustomerRouteService.prototype.getTemplateContentsList = function () {
        var url = this._env.baseUrl + "templateContent";
        return this._proxy.get(url);
    };
    CustomerRouteService = __decorate([
        __param(0, core_1.Inject(http_1.Http)), __param(1, core_1.Inject(environment_1.Environment)),
        __metadata("design:paramtypes", [http_1.Http, environment_1.Environment])
    ], CustomerRouteService);
    return CustomerRouteService;
}());
exports.CustomerRouteService = CustomerRouteService;
//# sourceMappingURL=customerRoute.service.js.map