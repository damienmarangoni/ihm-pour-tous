import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {DatePipe} from '@angular/common'
import {Environment} from '../../conf/environment'

export class CustomerRouteService {
    /*private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";*/

    //private _env:Environment;

    // Liste des Parcours client et de leurs Programmations    
    // Paramètres :
    //     - STARTDATE: Date de début de période de validité
    //     - ENDDATE: Date de fin de période de validité
    // Retours :
    //     Une liste de Parcours client avec le nom de leurs
    //     programmations associées
    public getCustomerRouteList(startDate: Date, endDate: Date): Observable<Response> {
        //let url = this._baseUrl + "views/customerRoute/customerRouteList.json";customerRoutes
        let url = this._env.baseUrl + "customerRoutes";
        /*let url = this._baseUrl + "customerRoutesOnDates";
        let params = new URLSearchParams();
        let datePipe = new DatePipe("fr-FR");
        params.set('startDate', datePipe.transform(startDate,"yyyy-MM-dd"));        
        params.set('endDate', datePipe.transform(endDate,"yyyy-MM-dd"));
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);*/
        return this._proxy.get(url);
    }    
    
    // Liste des Parcours client et de leurs Programmations    
    // Paramètres :
    //     - STARTDATE: Date de début de période de validité
    //     - ENDDATE: Date de fin de période de validité
    // Retours :
    //     Une liste de Parcours client avec le nom de leurs
    //     programmations associées
    public getCustomerRouteListOnDates(startDate: Date, endDate: Date): Observable<Response> {
        
        let url = this._env.baseUrl + "customerRoutesOnDates";
        let params = new URLSearchParams();
        let datePipe = new DatePipe("fr-FR");
        params.set('startDate', datePipe.transform(startDate,"yyyy-MM-dd"));        
        params.set('endDate', datePipe.transform(endDate,"yyyy-MM-dd"));
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
    }

    // Détails du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Les détails d'un Parcours (DU, communautés, segments) 
    //     sans ses programmations associées
    public getCustomerRouteDetails(id: number): Observable<Response> {
        //let url = this._baseUrl + "views/customerRoute/customerRouteDetails.json";
        let url = this._env.baseUrl + "customerRoutes";
        let params = new URLSearchParams();
        params.set('search', "customerQualificationId:" + id);          
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    }

    // Mise à jour détails du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - DETAILS: Détails du Parcours client
    // Retours :
    //     Statut du transfert
    public updateCustomerRouteDetails(body: any, id:string): Observable<Response> {
        let url = this._env.baseUrl + "createQualExpr/" + id;
        return this._proxy.post(url, body);
    }

    // Suppression du Parcours client sans ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Statut du transfert
    public deleteCustomerRouteDetails(id:number): Observable<Response> {
        let url = this._env.baseUrl + "deleteCustomerRoute/" + id;  
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    // Création du Parcours client sans ses programmations
    // Paramètres :
    //     - DETAILS: Détails du Parcours client
    // Retours :
    //     Statut du transfert
    public createCustomerRouteDetails(body: any): Observable<Response> {
        let url = this._env.baseUrl + "customerRoute";
        return this._proxy.post(url, body);
    }

    // Détails du Parcours client et de ses programmations
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Les détails d'un Parcours client (DU, communautés, segments) 
    //     et de ses programmations associées
    public getCustomerRouteFullDetail(id:number): Observable<Response> {
        let url = this._env.baseUrl + "programParentsOnCustQualifId";
        let params = new URLSearchParams();
        params.set('customerQualificationId', id.toString());  
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
    }

    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - PHASENAME : Programmation de la phase correspondante
    // Retours :
    //     Les détails de la programmation d'une phase d'un   
    //     Parcours client 
    public getCustomerRouteProg(id: number, codePhase: string): Observable<Response> {
        //let url = this._baseUrl + "views/customerRoute/customerRouteProg.json";
        let url = this._env.baseUrl + "programParentsOnCodePhase";
        let params = new URLSearchParams();
        params.set('customerQualificationId', id.toString());        
        params.set('codePhase', codePhase);
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    }   

    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    public updateCustomerRouteProg(): Observable<Response> {
        let url = this._env.baseUrl + "updateRouteProg";
        return this._proxy.post(url, new String());
    }     

    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    public createCustomerRouteProg(id: string, body: any): Observable<Response> {
        let url = this._env.baseUrl + "createProgParent/" + id;
        return this._proxy.post(url, body);
    }      

    // Suppression du Parcours client sans ses programmations<
    // Paramètres :
    //     - ID : ID du Parcours client
    // Retours :
    //     Statut du transfert
    public deleteCustomerRouteProg(id:number): Observable<Response> {
        let url = this._env.baseUrl + "deleteProgParent/" + id;  
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    // Détails de la programmation
    // Paramètres :
    //     - ID : ID du Parcours client
    //     - STEP : Programmation de la phase correspondante
    //     - DETAILS: Détails de la programmation
    // Retours :
    //     Statut du transfert
    public modifyCustomerRouteProg(id: string, body: any): Observable<Response> {
        let url = this._env.baseUrl + "modifyProgParent/" + id;
        return this._proxy.post(url, body);
    } 
    
    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getPhaseMessageModelList(codePhase: string): Observable<Response> {
        //let url = this._baseUrl + "views/customerRoute/phaseMessagesModelList.json";
        let url = this._env.baseUrl + "templates";
        let params = new URLSearchParams();
        params.set('search', "codePhase:" + codePhase);          
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
        //return this._proxy.get(url);
    } 
    
    // Priorités des programmations
    // Retours :
    //     Une liste de priorités
    public getPrioritiesList(): Observable<Response> {
        let url = this._env.baseUrl + "refPriority";
        return this._proxy.get(url);
    }
    
    // Contenu model des programmations
    // Retours :
    //     Une liste de modèle de message
    public getTemplateContentsList(): Observable<Response> {
        let url = this._env.baseUrl + "templateContent";
        return this._proxy.get(url);
    }

    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }
}