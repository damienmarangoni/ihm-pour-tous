"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var environment_1 = require("../../conf/environment");
var MaskNumberService = /** @class */ (function () {
    function MaskNumberService(_proxy, _env) {
        this._proxy = _proxy;
        this._env = _env;
    }
    /*private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";*/
    //private _env:Environment;   
    // Contenu numéros masqués
    // Retours :
    //     Une liste de numéros masqué (numéros noirs)
    MaskNumberService.prototype.getMaskNumberList = function () {
        var url = this._env.baseUrl + "masks";
        return this._proxy.get(url);
    };
    // Mise à jour détails du numéro masqué
    // Paramètres :
    //     - NumGIS :
    //     - BODY: Détails du numéro
    // Retours :
    //     Statut du transfert
    MaskNumberService.prototype.updateMaskNumber = function (body, NumGIS) {
        var url = this._env.baseUrl + "modifyMask/" + NumGIS;
        return this._proxy.post(url, body);
    };
    // Création du numero masqué
    // Paramètres :
    //     - NumGIS : 
    //     - BODY: Détails numéro
    // Retours :
    //     Statut du transfert
    MaskNumberService.prototype.createMaskNumber = function (body) {
        var url = this._env.baseUrl + "createMask";
        return this._proxy.post(url, body);
    };
    // Suppression du numero masqué
    // Paramètres :
    //     - NumGIS : 
    //     - BODY: Détails numéro
    // Retours :
    //     Statut du transfert
    MaskNumberService.prototype.deleteMaskNumber = function (numGIS) {
        var url = this._env.baseUrl + "deleteMask/" + numGIS;
        var options = new http_1.RequestOptions({});
        return this._proxy.delete(url, options);
    };
    MaskNumberService = __decorate([
        __param(0, core_1.Inject(http_1.Http)), __param(1, core_1.Inject(environment_1.Environment)),
        __metadata("design:paramtypes", [http_1.Http, environment_1.Environment])
    ], MaskNumberService);
    return MaskNumberService;
}());
exports.MaskNumberService = MaskNumberService;
//# sourceMappingURL=maskNumber.service.js.map