import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {DatePipe} from '@angular/common'
import {Environment} from '../../conf/environment'

export class MaskNumberService {
    /*private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";*/
    //private _env:Environment;   

    // Contenu numéros masqués
    // Retours :
    //     Une liste de numéros masqué (numéros noirs)
    public getMaskNumberList(): Observable<Response> {
        let url = this._env.baseUrl + "masks";
        return this._proxy.get(url);
    }


    // Mise à jour détails du numéro masqué
    // Paramètres :
    //     - NumGIS :
    //     - BODY: Détails du numéro
    // Retours :
    //     Statut du transfert
    public updateMaskNumber(body: any, NumGIS:string): Observable<Response> {
        let url = this._env.baseUrl + "modifyMask/" + NumGIS;
        return this._proxy.post(url, body);
    }



    // Création du numero masqué
    // Paramètres :
    //     - NumGIS : 
    //     - BODY: Détails numéro
    // Retours :
    //     Statut du transfert
    public createMaskNumber(body: any): Observable<Response> {
        let url = this._env.baseUrl + "createMask";
        return this._proxy.post(url, body);
    }

    // Suppression du numero masqué
    // Paramètres :
    //     - NumGIS : 
    //     - BODY: Détails numéro
    // Retours :
    //     Statut du transfert
    public deleteMaskNumber(numGIS:string): Observable<Response> {
        let url = this._env.baseUrl + "deleteMask/" + numGIS;
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }
}