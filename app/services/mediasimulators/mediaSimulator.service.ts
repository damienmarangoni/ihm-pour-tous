import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {Environment} from '../../conf/environment'

export class SimulatorDataService {
    //private _baseUrl = "http://localhost:3000/views/common/";
    //private _env:Environment;

    public getDUList(): Observable<Response> {
        let url = this._env.baseUrl + "/services";
        return this._proxy.get(url);
    }

    public getCommunitiesList(): Observable<Response> {
        let url = this._env.baseUrl + "/refCommunity";
        return this._proxy.get(url);
    }

    public getSegmentsList(): Observable<Response> {
        let url = this._env.baseUrl + "/refCategory";
        return this._proxy.get(url);
    }

    public getPhasesList(): Observable<Response> {
        let url = this._env.baseUrl + "/refPhase";
        return this._proxy.get(url);
    }

    public getProgramParentList(message: string[], codePhase: string, dateProgram: string): Observable<Response> {
        let url = this._env.baseUrl + "customerRoutesForSimulator";
        let params = new URLSearchParams();
        params.set('codePhase', codePhase);  
        params.set('date', dateProgram); 
        params.append('message', message[0]); 
        params.append('message', message[1]); 
        params.append('message', message[2]); 
        let options = new RequestOptions({search: params});

        return this._proxy.get(url, options);
    }    


    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }

}