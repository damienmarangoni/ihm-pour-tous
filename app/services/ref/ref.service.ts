import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {DatePipe} from '@angular/common'
import {Environment} from '../../conf/environment'

export class RefService {

    //private _env:Environment;
    
    // Modèles de pied de page
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste de modèle de pied de page
    public getWrittenMessageModelList(): Observable<Response> {

        let url = this._env.baseUrl + "refTemplateItem";
        return this._proxy.get(url);
    } 
    
    // Phases de référence
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste de phases de référence
    public getRefPhase(): Observable<Response> {

        let url = this._env.baseUrl + "refPhase";
        return this._proxy.get(url);
    } 
    
    // Phases de référence
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste de phases de référence
    public getRefDefAudioTrackPhases(): Observable<Response> {

        let url = this._env.baseUrl + "refDefaultAudioTrackPhases";
        return this._proxy.get(url);
    } 
    
    // Phases de référence
    // Paramètres :
    //     - None
    // Retours :
    //     Une liste de phases de référence
    public updateRefDefaultAudioTrack(body: any, file: string): Observable<Response> {
        let url = this._env.baseUrl + "updateRefDefAudioTrack/" + file;
        return this._proxy.put(url, body);
    } 


    public constructor(@Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }
}