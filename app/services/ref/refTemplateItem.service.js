"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var core_1 = require("@angular/core");
var RefTemplateItemService = /** @class */ (function () {
    function RefTemplateItemService(_proxy) {
        this._proxy = _proxy;
        this._baseUrlStub = "http://localhost:3000/";
        this._baseUrl = "http://localhost:8080/";
        this._baseUrl2 = "http://192.168.101.22:8080/";
    }
    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    RefTemplateItemService.prototype.getWrittenMessageModelList = function () {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        var url = this._baseUrl + "refTemplateItem";
        /*let params = new URLSearchParams();
        params.set('search', "codePhase:" + codePhase);
        let options = new RequestOptions({search: params});*/
        return this._proxy.get(url);
        /*return this._proxy.get(url);*/
    };
    RefTemplateItemService = __decorate([
        __param(0, core_1.Inject(http_1.Http)),
        __metadata("design:paramtypes", [http_1.Http])
    ], RefTemplateItemService);
    return RefTemplateItemService;
}());
exports.RefTemplateItemService = RefTemplateItemService;
//# sourceMappingURL=refTemplateItem.service.js.map