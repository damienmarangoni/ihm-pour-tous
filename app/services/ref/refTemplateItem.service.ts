import { Http, Response, Headers, RequestOptions, URLSearchParams  } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import {DatePipe} from '@angular/common'

export class RefTemplateItemService {
    private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";
    
    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getWrittenMessageModelList(): Observable<Response> {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        let url = this._baseUrl + "refTemplateItem";
        /*let params = new URLSearchParams();
        params.set('search', "codePhase:" + codePhase);          
        let options = new RequestOptions({search: params});*/

        return this._proxy.get(url);
        /*return this._proxy.get(url);*/
    } 


    public constructor(@Inject(Http) private _proxy: Http) {
    }
}