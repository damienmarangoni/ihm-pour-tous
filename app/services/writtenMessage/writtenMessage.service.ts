import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http'
import { Observable } from 'rxjs/observable'
import { Inject } from '@angular/core'
import { DatePipe } from '@angular/common'
import { Environment } from '../../conf/environment'

export class WrittenMessageService {
    /*private _baseUrlStub = "http://localhost:3000/";
    private _baseUrl = "http://localhost:8080/";
    private _baseUrl2 = "http://192.168.101.22:8080/";*/
    //private _env: Environment;

    // Modèle des messages de la phase
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getWrittenMessageModelList(codePhase: string): Observable<Response> {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        let url = this._env.baseUrl + "templates";
        let params = new URLSearchParams();
        params.set('search', "codePhase:" + codePhase);
        let options = new RequestOptions({ search: params });

        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    }

    // Modèle de message sélectionné
    // Paramètres :
    //     - PHASENAME : Nom de la phase
    // Retours :
    //     Une liste de modèle de message liés à la phase
    public getWrittenMessageModel(id: string): Observable<Response> {
        /*let url = this._baseUrlStub + "views/writtenMessage/writtenMessageModelList.json";*/
        let url = this._env.baseUrl + "templateOnIdTemplate";
        let params = new URLSearchParams();
        params.set('idTemplate', id);
        let options = new RequestOptions({ search: params });

        return this._proxy.get(url, options);
        /*return this._proxy.get(url);*/
    }

    public getContractCustomerLabelList(): Observable<Response> {
        let url = this._env.baseUrl + "contractCustomerLabels";
        return this._proxy.get(url);
    }


    public getWrittenMessageTextList(): Observable<Response> {
        let url = this._env.baseUrl + "getWritTempContentText";
        // let url = this._baseUrlStub + "views/writtenMessage/writtenMessageTextList.json";

        return this._proxy.get(url);
    }

    // Mise à jour du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    public updateWrittenMessageModel(body: any, id: string): Observable<Response> {
        let url = this._env.baseUrl + "modifyWrittenTemplate/" + id;
        return this._proxy.put(url, body);
    }

    // Création du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    //     - BODY: Détails Modèle
    // Retours :
    //     Statut du transfert
    public createWrittenMessageModel(body: any): Observable<Response> {
        let url = this._env.baseUrl + "createTemplate/1";
        return this._proxy.post(url, body);
    }

    public createWrittenMessageText(body: any): Observable<Response> {
        let url = this._env.baseUrl + "createWritTempContentText";
        return this._proxy.post(url, body);
    }

    // Suppression du message écrit "texte" aka TemplateContent côté serveur
    // Paramètres :
    //     - ID : ID du texte
    // Retours :
    //     Statut du transfert
    public deleteWrittenMessageText(id: string): Observable<Response> {
        let url = this._env.baseUrl + "deleteTemplateContent/" + id;
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    // Mise à jour du message écrit "texte" aka TemplateContent côté serveur
    // Paramètres :
    //     - TemplateContent : l'objet modifié 
    //     - ID : ID du texte
    // Retours :
    //     Statut du transfert
    public saveWrittenMessageText(body: any, id: string): Observable<Response> {

        let url = this._env.baseUrl + "updateTemplateContent/" + id;
        return this._proxy.put(url, body);
    }


    // Suppression du modèle de message
    // Paramètres :
    //     - ID : ID du Modèle
    // Retours :
    //     Statut du transfert	
    public deleteWrittenMessageModel(id: string): Observable<Response> {
        let url = this._env.baseUrl + "deleteTemplate/" + id;
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }

    public deleteWrittenMessage(id: string): Observable<Response> {
        let url = this._env.baseUrl + "deleteWrittenMessage/" + id;
        let options = new RequestOptions({});
        return this._proxy.delete(url, options);
    }


    public constructor( @Inject(Http) private _proxy: Http, @Inject(Environment) private _env: Environment) {
    }
}